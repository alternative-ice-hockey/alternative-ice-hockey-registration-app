/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MembersReportPanel.java
 *
 * Created on 05-Oct-2011, 16:41:18
 */
package altsdatabase.gui;

import altsdatabase.data.DataStore;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author alemer
 */
public class MembersReportPanel extends javax.swing.JPanel
{
    DataStore store;

    /** Creates new form MembersReportPanel */
    public MembersReportPanel( DataStore store )
    {
		this.store = store;
        initComponents();

		GregorianCalendar calendar = new GregorianCalendar();
		if (calendar.get( Calendar.MONTH ) >= Calendar.SEPTEMBER)
		{
			calendar = new GregorianCalendar( calendar.get( Calendar.YEAR ), Calendar.SEPTEMBER, 01 );
		}
		else
		{
			calendar = new GregorianCalendar( calendar.get( Calendar.YEAR ) - 1, Calendar.SEPTEMBER, 01 );
		}
		startCalButton.setTargetDate( calendar.getTime() );
		calendar.add( Calendar.YEAR, 1 );
		endCalButton.setTargetDate( calendar.getTime() );
		calculate();
    }

    public void addBackActionListener( ActionListener l )
    {
        backButton.addActionListener( l );
    }

    public void removeBackActionListener( ActionListener l )
    {
        backButton.removeActionListener( l );
    }

	private void calculate()
	{
		int result = store.memberCountByAttendance( startCalButton.getTargetDate(), endCalButton.getTargetDate(), (Integer)attendance.getValue() );
		resultLabel.setText( Integer.toString( result ) );
	}

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        startCalButton = new net.sourceforge.jcalendarbutton.JCalendarButton();
        endCalButton = new net.sourceforge.jcalendarbutton.JCalendarButton();
        attendance = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        resultLabel = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();

        startCalButton.setText("Choose...");
        startCalButton.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                startCalButtonPropertyChange(evt);
            }
        });

        endCalButton.setText("Choose...");
        endCalButton.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                endCalButtonPropertyChange(evt);
            }
        });

        attendance.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                attendanceStateChanged(evt);
            }
        });

        jLabel3.setText("Minimum attendance:");

        resultLabel.setText("0");

        jLabel4.setText("Members:");

        jLabel2.setText("End date:");

        jLabel1.setText("Start date:");

        backButton.setText("Back");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(resultLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(attendance, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(endCalButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(startCalButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addComponent(backButton))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startCalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(endCalButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(attendance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(resultLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 156, Short.MAX_VALUE)
                .addComponent(backButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

private void startCalButtonPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_startCalButtonPropertyChange
		if ( evt.getNewValue() instanceof Date )
        {
            calculate();
        }
}//GEN-LAST:event_startCalButtonPropertyChange

private void endCalButtonPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_endCalButtonPropertyChange
		if ( evt.getNewValue() instanceof Date )
        {
            calculate();
        }
}//GEN-LAST:event_endCalButtonPropertyChange

private void attendanceStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_attendanceStateChanged
		calculate();
}//GEN-LAST:event_attendanceStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner attendance;
    private javax.swing.JButton backButton;
    private net.sourceforge.jcalendarbutton.JCalendarButton endCalButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel resultLabel;
    private net.sourceforge.jcalendarbutton.JCalendarButton startCalButton;
    // End of variables declaration//GEN-END:variables
}
