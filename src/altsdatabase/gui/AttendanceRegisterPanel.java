/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * AttendanceRegisterPanel.java
 *
 * Created on 05-Oct-2011, 15:15:22
 */
package altsdatabase.gui;

import altsdatabase.data.AttendanceRegister;
import altsdatabase.data.DataStore;
import altsdatabase.data.DuplicateEntryException;
import altsdatabase.data.Member;
import altsdatabase.data.ProhibitedChangeException;
import altsdatabase.models.MemberTableFormat;
import ca.odell.glazedlists.swing.EventTableModel;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 *
 * @author alemer
 */
public class AttendanceRegisterPanel extends javax.swing.JPanel
{
    AttendanceRegister register;
    DataStore dataStore;
    EventTableModel<Member> model;

    java.awt.Frame getWindow() {
        return (java.awt.Frame)getTopLevelAncestor();
    }

    /** Creates new form AttendanceRegisterPanel */
    public AttendanceRegisterPanel( AttendanceRegister register )
    {
        this.register = register;
        this.dataStore = register.getDataStore();

        initComponents();

        JComboBox cb = new JComboBox ();
        for ( Member.Status status : Member.Status.values () )
        {
            cb.addItem (status);
        }
        registerTable.setDefaultEditor (Member.Status.class, new DefaultCellEditor (cb));
        model = new EventTableModel<Member>( register, new MemberTableFormat(true) );
        registerTable.setModel( model );
        model.addTableModelListener( new TableModelListener() {
            public void tableChanged( TableModelEvent e )
            {
                registerSizeLabel.setText( "Total: " + model.getRowCount() );
            }
        } );
        registerSizeLabel.setText( "Total: " + model.getRowCount() );
        this.addFocusListener( new FocusListener() {
            public void focusGained( FocusEvent e )
            {
                codeField.requestFocusInWindow();
            }
            public void focusLost( FocusEvent e )
            {
            }
        });
    }

    public void addBackActionListener( ActionListener l )
    {
        backButton.addActionListener( l );
    }

    public void removeBackActionListener( ActionListener l )
    {
        backButton.removeActionListener( l );
    }
	
	private void updateContactDetails( Member entry )
	{
		if ( entry.getContactDetails().length() == 0 &&
                entry.getCollege().length() == 0 )
		{
			StringBuilder message = new StringBuilder();
			message.append( entry.getName() );
			message.append( " does not have any contact details.\n" );
			if ( entry.getStatus() == Member.Status.OUStudent )
			{
				message.append( "Please enter their college or an email address for them." );
			}
			else 
			{
				message.append( "Please enter an email address for them." );
			}
			String value = JOptionPane.showInputDialog( message );
			if ( value != null && value.length() > 0 )
			{
				try
				{
                    if ( entry.getStatus() == Member.Status.OUStudent &&
                            !value.contains( "@" ) ) {
                        entry.setCollege( value );
                    } else {
                        entry.setContactDetails( value );
                    }
				}
				catch ( ProhibitedChangeException ex )
				{
					System.err.println( "Update contact details failed: " + ex.getMessage() );
				}
			}
		}
	}

    private void addCurrentlyEnteredMember()
    {
        codeField.setText( dataStore.cleanScannedCode( codeField.getText() ) );
        Member entry = null;
		for ( Member m : dataStore.members() )
		{
			if (codeField.getText().equals(m.getMembershipCode()))
			{
				entry = m;
				break;
			}
		}
        try
        {
            if ( entry != null )
            {
				updateContactDetails( entry );
                register.add( entry );
            }
            else
            {
                EditMemberDialog dialog = new EditMemberDialog(
                        getWindow(),
                        dataStore,
                        codeField.getText(),
                        true );
                dialog.setVisible( true );
                entry = dialog.getMember();
                if ( entry != null )
                {
					updateContactDetails( entry );
                    register.add( entry );
                }
            }
            registerTable.scrollRectToVisible(
                    registerTable.getCellRect( registerTable.getRowCount() - 1, 0, true ) );
        }
        catch ( DuplicateEntryException ex )
        {
            // we don't care
        }
        codeField.setText( "" );
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        registerTable = new javax.swing.JTable();
        registerSizeLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lookupButton = new javax.swing.JButton();
        okbutton = new javax.swing.JButton();
        codeField = new javax.swing.JTextField();
        backButton = new javax.swing.JButton();

        registerTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        registerTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                registerTableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(registerTable);

        registerSizeLabel.setText("Total: 0");

        jLabel1.setText("Membership code:");

        lookupButton.setText("Lookup member");
        lookupButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lookupButtonActionPerformed(evt);
            }
        });

        okbutton.setText("OK");
        okbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okbuttonActionPerformed(evt);
            }
        });

        codeField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codeFieldActionPerformed(evt);
            }
        });

        backButton.setText("Back");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 533, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 509, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(codeField, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(okbutton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lookupButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 399, Short.MAX_VALUE)
                        .addComponent(registerSizeLabel)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 313, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lookupButton)
                    .addComponent(okbutton)
                    .addComponent(codeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(registerSizeLabel)
                    .addComponent(backButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

private void registerTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_registerTableKeyReleased
        if ( evt.isAltDown() )
            return;
        if ( evt.isAltGraphDown() )
            return;
        if ( evt.isControlDown() )
            return;
        if ( evt.isMetaDown() )
            return;
        if ( evt.isShiftDown() )
            return;
        if ( evt.getKeyCode() == KeyEvent.VK_DELETE )
        {
            for ( int row : registerTable.getSelectedRows() )
            {
                int modelIndex = registerTable.convertRowIndexToModel( row );
				Member member = model.getElementAt( modelIndex );
                register.remove( member );
            }
        }
}//GEN-LAST:event_registerTableKeyReleased

private void lookupButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lookupButtonActionPerformed
        try
        {
            Member entry = LookupMemberDialog.lookupMember( getWindow(), dataStore );
            if ( entry != null )
            {
				updateContactDetails( entry );
                register.add( entry );
                registerTable.scrollRectToVisible(
                        registerTable.getCellRect( registerTable.getRowCount() - 1, 0, true ) );
            }
        }
        catch ( DuplicateEntryException ex )
        {
            // we don't care
        }
        codeField.requestFocusInWindow();
}//GEN-LAST:event_lookupButtonActionPerformed

private void okbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okbuttonActionPerformed
        addCurrentlyEnteredMember();
        codeField.requestFocusInWindow();
}//GEN-LAST:event_okbuttonActionPerformed

private void codeFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codeFieldActionPerformed
        addCurrentlyEnteredMember();
}//GEN-LAST:event_codeFieldActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JTextField codeField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton lookupButton;
    private javax.swing.JButton okbutton;
    private javax.swing.JLabel registerSizeLabel;
    private javax.swing.JTable registerTable;
    // End of variables declaration//GEN-END:variables
}
