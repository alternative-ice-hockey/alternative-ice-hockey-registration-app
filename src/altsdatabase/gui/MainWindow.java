/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MainWindow.java
 *
 * Created on 20-Dec-2009, 15:11:05
 */

package altsdatabase.gui;

import altsdatabase.data.*;
import altsdatabase.models.SessionFilterUtils;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author alex
 */
public class MainWindow extends javax.swing.JFrame {
    private DataStore dataStore = null;
    private IdentityHashMap<Session, AttendanceRegisterPanel> registerPanels = new IdentityHashMap<Session, AttendanceRegisterPanel>();
    private IdentityHashMap<Session, AttendanceViewPanel> registerViewPanels = new IdentityHashMap<Session, AttendanceViewPanel>();
    private Map<String, JPanel> storePanels = new HashMap<String, JPanel>();
    private LinkedList<JPanel> menuPath = new LinkedList<JPanel>();
    private javax.swing.JFileChooser jFileChooser;

    private void goToPanel( JPanel panel ) {
        menuPath.add( panel );
        getContentPane().removeAll();
        getContentPane().add( panel );
        pack();
        panel.requestFocusInWindow();
    }

    private void loadTransientPanel( JPanel panel )
    {
        getContentPane().removeAll();
        getContentPane().add( panel );
        pack();
        panel.requestFocusInWindow();
    }

    private void goBack() {
        menuPath.removeLast();
        JPanel panel = menuPath.getLast();
        getContentPane().removeAll();
        getContentPane().add( panel );
        pack();
        panel.requestFocusInWindow();
    }

    private ActionListener goBackActionListener = new ActionListener() {
        public void actionPerformed( ActionEvent e )
        {
            goBack();
        }
    };

    private NewOrOpenChoicePanel initPanel = null;
    private JPanel getInitPanel() {
        if (initPanel == null) {
            initPanel = new NewOrOpenChoicePanel();
            initPanel.addNewButtonActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    newStore();
                }
            });
            initPanel.addOpenButtonActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    open();
                }
            });
        }
        return initPanel;
    }

    private MainChoicePanel mcPanel = null;
    private MainChoicePanel getMainPanel() {
        if (mcPanel == null) {
            mcPanel = new MainChoicePanel();
            mcPanel.addCloseActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    close( OnClose.Ask );
                }
            });
            mcPanel.addSaveCloseActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    close( OnClose.Save );
                }
            });
            mcPanel.addNextSessionActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( getRegisterPanel( mcPanel.getNextSession() ) );
                }
            });
            mcPanel.addSignupActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( getSignupPanel() );
                }
            });
            mcPanel.addManageActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( getManagementPanel() );
                }
            });
            mcPanel.addReportsActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( getMembersReportPanel() );
                }
            });
        }

        return mcPanel;
    }

    private JPanel getMembersPanel() {
        MembersPanel panel = (MembersPanel)storePanels.get( "members" );
        if (panel == null) {
            panel = new MembersPanel( dataStore );
            storePanels.put( "members", panel );
            panel.addBackActionListener( goBackActionListener );
        }
        return panel;
    }

    private JPanel getSessionsPanel() {
        EditSessionsPanel panel = (EditSessionsPanel)storePanels.get( "sessions" );
        if (panel == null) {
            panel = new EditSessionsPanel( dataStore, EditSessionsPanel.FinishAction.Back );
            storePanels.put( "sessions", panel );
            panel.addFinishActionListener( goBackActionListener );
            panel.addOpenRegisterListener( new EditSessionsPanel.SessionEditorListener() {
                public void viewRegister( Session session )
                {
                    goToPanel( getRegisterViewPanel( session ) );
                }
                public void openRegister( Session session )
                {
                    goToPanel( getRegisterPanel( session ) );
                }
                public void editSession(Session session)
                {
                    goToPanel( getEditSessionDetailsPanel( session ) );
                }
                
            });
        }
        return panel;
    }

    private JPanel getMembersReportPanel() {
        MembersReportPanel panel = (MembersReportPanel)storePanels.get( "report" );
        if (panel == null) {
            panel = new MembersReportPanel( dataStore );
            storePanels.put( "report", panel );
            panel.addBackActionListener( goBackActionListener );
        }
        return panel;
    }

    private JPanel doMerge()
    {
        DataStore source;
        try
        {
            if ( jFileChooser.showOpenDialog( this ) == JFileChooser.APPROVE_OPTION )
            {
                File file = jFileChooser.getSelectedFile();
                source = DataStore.open( file );
            }
            else
            {
                return null;
            }
        }
        catch ( IOException ex )
        {
            JOptionPane.showMessageDialog(
                    this,
                    ex.getMessage(),
                    "Could not open the file",
                    JOptionPane.ERROR_MESSAGE );
            return null;
        }
        try
        {
            MergeJob mergeJob = dataStore.createMergeJob(source);
            MergePanel p;
            if (!mergeJob.run())
            {
                p = new MergePanel(false, mergeJob.getError(), mergeJob.getWarnings());
            }
            else
            {
                p = new MergePanel(true, null, mergeJob.getWarnings());
            }
            p.addBackActionListener( goBackActionListener );
            return p;
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(
                    this,
                    ex.getMessage(),
                    "Unexpected error when merging",
                    JOptionPane.ERROR_MESSAGE );
            return null;
        }
        finally
        {
            source.discardAndClose();
        }
    }

    private ManagementPanel mgmtPanel;
    private JPanel getManagementPanel() {
        if (mgmtPanel == null) {
            mgmtPanel = new ManagementPanel();
            mgmtPanel.addSessionsActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( getSessionsPanel() );
                }
            });
            mgmtPanel.addMembersActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( getMembersPanel() );
                }
            });
            mgmtPanel.addMergeActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    JPanel p = doMerge();
                    if (p != null)
                    {
                        goToPanel( p );
                    }
                }
            });
            mgmtPanel.addBackActionListener( goBackActionListener );
        }
        return mgmtPanel;
    }

    private JPanel getSignupPanel() {
        SignupPanel signupPanel = (SignupPanel)storePanels.get( "signup" );
        if (signupPanel == null) {
            signupPanel = new SignupPanel( dataStore );
            storePanels.put( "signup", signupPanel );
            signupPanel.addBackActionListener( goBackActionListener );
        }
        return signupPanel;
    }

    private JPanel getRegisterPanel( Session session ) {
        AttendanceRegisterPanel panel = registerPanels.get( session );
        if (panel == null) {
            panel = new AttendanceRegisterPanel( dataStore.getRegister( session ) );
            registerPanels.put( session, panel );
            panel.addBackActionListener( goBackActionListener );
        }
        return panel;
    }

    private JPanel getRegisterViewPanel( Session session ) {
        AttendanceViewPanel panel = registerViewPanels.get( session );
        if (panel == null) {
            panel = new AttendanceViewPanel( dataStore.getRegister( session ) );
            registerViewPanels.put( session, panel );
            panel.addBackActionListener( goBackActionListener );
        }
        return panel;
    }

    private JPanel getEditSessionDetailsPanel( Session session ) {
        EditSessionDetailsPanel panel = (EditSessionDetailsPanel)
                storePanels.get("edit session");
        if (panel == null) {
            panel = new EditSessionDetailsPanel();
            storePanels.put("edit session", panel);
            panel.addFinishActionListener( goBackActionListener );
        }
        panel.setSession(session);
        return panel;
    }

    /** Creates new form MainWindow */
    public MainWindow( File openAt ) {
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Alternative Ice Hockey Registration");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                quit();
            }
        });
        jFileChooser = new javax.swing.JFileChooser();
        jFileChooser.setFileFilter(new FileNameExtensionFilter( "Alts database files (*.alts)", "alts" ));

        if ( openAt != null )
        {
            try
            {
                dataStore = DataStore.open( openAt );
            }
            catch ( IOException ex )
            {
                JOptionPane.showMessageDialog( this, "Cannot open file " + openAt, "Error", JOptionPane.ERROR_MESSAGE );
            }
        }
        if ( dataStore == null )
            dataStore = StaleSessionDialog.findStaleSession( this );
        if ( dataStore != null ) {
            setupForOpenStore();
        } else {
            getContentPane().add( getInitPanel() );
            pack();
        }
    }

    private void quit()
    {
        if ( close() )
            System.exit( 0 );
    }

    private enum OnClose
    {
        Save,
        Discard,
        Ask
    }

    private boolean close()
    {
        return close( OnClose.Ask );
    }

    private boolean close(OnClose onClose)
    {
        if ( dataStore != null )
        {
            try
            {
                if ( dataStore.hasChanges() && onClose == OnClose.Save )
                {
                    if ( ! save( true ) )
                        return false;
                }
                else if ( dataStore.hasChanges() && onClose == OnClose.Ask )
                {
                    String message;
                    if ( dataStore.getSaveFileName() == null )
                    {
                        message = "Do you want to save your changes before closing?";
                    }
                    else
                    {
                        message = "Do you want to save your changes to " +
                                dataStore.getSaveFileName().getName() +
                                " before closing?";
                    }
                    int result = JOptionPane.showConfirmDialog(
                            this,
                            message,
                            "Save changes",
                            JOptionPane.YES_NO_CANCEL_OPTION );
                    if ( result == JOptionPane.YES_OPTION )
                    {
                        if ( ! save( true ) )
                            return false;
                    }
                    else if ( result == JOptionPane.NO_OPTION )
                    {
                        dataStore.discardAndClose();
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    dataStore.discardAndClose();
                }
            }
            catch ( IllegalStateException ex )
            {
                JOptionPane.showMessageDialog(
                        this,
                        "The file appears to have been closed already: this is a bug",
                        "File already closed",
                        JOptionPane.WARNING_MESSAGE );
            }
            upcomingSessions.removeListEventListener(nextSessionUpdater);
            upcomingSessions.dispose();
            upcomingSessions = null;
            nextSession = null;
            dataStore = null;
            registerPanels.clear();
            registerViewPanels.clear();
            storePanels.clear();

            menuPath.clear();
            loadTransientPanel( getInitPanel() );
        }
        return true;
    }

    private boolean save( boolean close )
    {
        if ( dataStore.getSaveFileName() == null )
        {
            saveAs( close );
        }
        else
        {
            try
            {
                if ( close )
                    dataStore.saveAndClose();
                else
                    dataStore.save();
            }
            catch ( IOException ex )
            {
                JOptionPane.showMessageDialog(
                        this,
                        ex.getMessage(),
                        "Could not save the file",
                        JOptionPane.ERROR_MESSAGE );
                return false;
            }
            catch ( NoFileNameException ex )
            {
                saveAs( close );
            }
        }
        return true;
    }

    private boolean saveAs( boolean close )
    {
        try
        {
            if ( jFileChooser.showSaveDialog( this ) == JFileChooser.APPROVE_OPTION )
            {
                File file = jFileChooser.getSelectedFile();
                if ( close )
                    dataStore.saveAndClose( file );
                else
                    dataStore.save( file );
                return true;
            }
            else
            {
                return false;
            }
        }
        catch ( IOException ex )
        {
            JOptionPane.showMessageDialog(
                    this,
                    ex.getMessage(),
                    "Could not save the file",
                    JOptionPane.ERROR_MESSAGE );
            return false;
        }
    }

    private void open()
    {
        try
        {
            if ( jFileChooser.showOpenDialog( this ) == JFileChooser.APPROVE_OPTION )
            {
                File file = jFileChooser.getSelectedFile();
                if ( close() )
                {
                    dataStore = DataStore.open( file );
                    setupForOpenStore();
                }
            }
        }
        catch ( IOException ex )
        {
            JOptionPane.showMessageDialog(
                    this,
                    ex.getMessage(),
                    "Could not open the file",
                    JOptionPane.ERROR_MESSAGE );
        }
    }

    private void newStore()
    {
        try
        {
            if ( close() )
            {
                dataStore = new DataStore();
                setupForOpenStore();
            }
        }
        catch ( Error ex )
        {
            JOptionPane.showMessageDialog(
                    this,
                    ex.getMessage(),
                    "Could not create the required temporary files",
                    JOptionPane.ERROR_MESSAGE );
        }
    }

    private EventList<Session> upcomingSessions = null;
    private Session nextSession = null;
    private ListEventListener<Session> nextSessionUpdater =
            new ListEventListener<Session>()
            {
                public void listChanged(ListEvent<Session> le)
                {
                    if (upcomingSessions == null)
                    {
                        nextSession = null;
                        return;  // we shouldn't be here
                    }

                    boolean needsReset = false;
                    while ( le.next() )
                    {
                        if ( le.getType() == ListEvent.DELETE )
                        {
                            Session oldMember = le.getOldValue();
                            if (oldMember == ListEvent.UNKNOWN_VALUE
                                    || oldMember == nextSession)
                            {
                                needsReset = true;
                            }
                        }
                        else if ( le.getType() == ListEvent.INSERT )
                        {
                            if (le.getIndex() == 0)
                                needsReset = true;
                        }
                    }
                    if (needsReset)
                    {
                        if (upcomingSessions.size() > 0)
                            nextSession = upcomingSessions.get(0);
                        else
                            nextSession = null;

                        if (mcPanel != null)
                            mcPanel.setNextSession(nextSession);
                    }
                }
            };

    private void setupForOpenStore()
    {
        final MainChoicePanel panel = getMainPanel();

        upcomingSessions = new FilterList<Session>( dataStore.sessions(), new SessionFilterUtils.UpcomingMatcher() );
        upcomingSessions.addListEventListener(nextSessionUpdater);

        if (!upcomingSessions.isEmpty()) {
            nextSession = upcomingSessions.get( 0 );
            panel.setNextSession( nextSession );
            goToPanel( panel );
        } else {
            panel.setNextSession( null );
            NoSessionsQuestionPanel qPanel = new NoSessionsQuestionPanel();
            qPanel.addNoActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    goToPanel( panel );
                }
            });
            qPanel.addYesActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    EditSessionsPanel sPanel = new EditSessionsPanel( dataStore, EditSessionsPanel.FinishAction.Continue );
                    sPanel.addFinishActionListener( new ActionListener() {
                        public void actionPerformed( ActionEvent e )
                        {
                            goToPanel( panel );
                        }
                    });
                    loadTransientPanel( sPanel );
                }
            });
            loadTransientPanel( qPanel );
        }
    }
}
