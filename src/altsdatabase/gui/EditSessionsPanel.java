/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * EditSessionsPanel.java
 *
 * Created on 05-Oct-2011, 15:55:44
 */
package altsdatabase.gui;

import altsdatabase.data.DataStore;
import altsdatabase.data.OverlappingSessionsException;
import altsdatabase.data.Session;
import ca.odell.glazedlists.swing.EventListModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author alemer
 */
public class EditSessionsPanel extends javax.swing.JPanel
{
    public static DateFormat dateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
    private Date firstStartDate;
    private DataStore store;
    private FinishAction finishAction;
    private JPopupMenu sessionPopup;

    public interface SessionEditorListener extends EventListener
    {
        public void openRegister( Session session );
        public void viewRegister( Session session );
        public void editSession( Session session );
    }

    public enum FinishAction {
        Back,
        Continue
    }

    /** Creates new form EditSessionsPanel */
    public EditSessionsPanel( DataStore store, FinishAction finishAction )
    {
        this.store = store;
        this.finishAction = finishAction;
        initComponents();

        Calendar cal = Calendar.getInstance();
        cal.set( Calendar.HOUR_OF_DAY, 23 );
        cal.set( Calendar.MINUTE, 30 );
        cal.set( Calendar.SECOND, 0 );
        cal.set( Calendar.MILLISECOND, 0 );
        setStartDateTime( cal.getTime() );

        sessionsList.setModel( new EventListModel<Session>( store.sessions() ) );

        backButton.setVisible( finishAction == FinishAction.Back );
        viewButton.setVisible( finishAction == FinishAction.Back );
        if ( finishAction == FinishAction.Back ) {
            continueButton.setText( "Take Attendance" );
            sessionsList.addListSelectionListener( new ListSelectionListener() {
                public void valueChanged( ListSelectionEvent e )
                {
                    if ( e.getValueIsAdjusting() )
                        return;
                    continueButton.setEnabled( sessionsList.getSelectedIndices().length == 1 );
                }
            });
            continueButton.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    Session s = (Session)sessionsList.getSelectedValue();
                    if (s != null)
                        fireOpenRegister( s );
                }
            });
            viewButton.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e )
                {
                    Session s = (Session)sessionsList.getSelectedValue();
                    if (s != null)
                        fireViewRegister( s );
                }
            });
        }

        sessionPopup = new JPopupMenu();
        JMenuItem menuItem;
        menuItem = new JMenuItem("View register", KeyEvent.VK_V);
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                Session s = (Session)sessionsList.getSelectedValue();
                if (s != null)
                    fireViewRegister( s );
            }
        });
        sessionPopup.add(menuItem);
        menuItem = new JMenuItem("Take attendance", KeyEvent.VK_A);
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                Session s = (Session)sessionsList.getSelectedValue();
                if (s != null)
                    fireOpenRegister( s );
            }
        });
        sessionPopup.add(menuItem);
        sessionPopup.addSeparator();
        menuItem = new JMenuItem("Edit", KeyEvent.VK_A);
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                Session s = (Session)sessionsList.getSelectedValue();
                if (s != null)
                    fireEditSession( s );
            }
        });
        sessionPopup.add(menuItem);
        menuItem = new JMenuItem("Delete", KeyEvent.VK_D);
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                deleteSelectedSessions();
            }
        });
        sessionPopup.add(menuItem);
        
    }

    public void addFinishActionListener( ActionListener l )
    {
        if ( finishAction == FinishAction.Back )
            backButton.addActionListener( l );
        else
            continueButton.addActionListener( l );
    }

    public void removeFinishActionListener( ActionListener l )
    {
        if ( finishAction == FinishAction.Back )
            backButton.removeActionListener( l );
        else
            continueButton.removeActionListener( l );
    }

    private void fireOpenRegister( Session session )
    {
        Object[] list = listenerList.getListenerList();
        for (int i = 0; i < list.length; i+=2) {
            if (list[i] == SessionEditorListener.class) {
                ((SessionEditorListener)list[i+1]).openRegister( session );
            }
        }
    }

    private void fireViewRegister( Session session )
    {
        Object[] list = listenerList.getListenerList();
        for (int i = 0; i < list.length; i+=2) {
            if (list[i] == SessionEditorListener.class) {
                ((SessionEditorListener)list[i+1]).viewRegister( session );
            }
        }
    }

    private void fireEditSession( Session session )
    {
        Object[] list = listenerList.getListenerList();
        for (int i = 0; i < list.length; i+=2) {
            if (list[i] == SessionEditorListener.class) {
                ((SessionEditorListener)list[i+1]).editSession( session );
            }
        }
    }

    public void addOpenRegisterListener( SessionEditorListener l )
    {
        listenerList.add( SessionEditorListener.class, l );
    }

    public void removeOpenRegisterListener( SessionEditorListener l )
    {
        listenerList.remove( SessionEditorListener.class, l );
    }

    private void setStartDateTime( Date date )
    {
        if ( date == null )
        {
            date = firstStartDate;
        }
        else
        {
            firstStartDate = date;
        }
        startTime.setText( dateTimeFormat.format( date ) );
        jCalendarButton1.setTargetDate( date );
        jTimeButton1.setTargetDate( date );
    }

    private void setStartDateTime( String timeString )
    {
        Date time = null;
        try
        {
            if ( ( timeString != null ) && ( timeString.length() > 0 ) )
            {
                time = dateTimeFormat.parse( timeString );
            }
        }
        catch ( Exception e )
        {
            time = null;
        }
        this.setStartDateTime( time );
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        startTime = new javax.swing.JTextField();
        continueButton = new javax.swing.JButton();
        repeatOffset = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        sessionsList = new javax.swing.JList();
        jCalendarButton1 = new net.sourceforge.jcalendarbutton.JCalendarButton();
        jTimeButton1 = new net.sourceforge.jcalendarbutton.JTimeButton();
        jLabel5 = new javax.swing.JLabel();
        duration = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        addButton = new javax.swing.JButton();
        sessionCount = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();
        viewButton = new javax.swing.JButton();

        startTime.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                startTimeFocusLost(evt);
            }
        });

        continueButton.setText("Continue");

        repeatOffset.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "day", "week", "month" }));
        repeatOffset.setSelectedIndex(1);

        sessionsList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sessionsListMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sessionsListMouseReleased(evt);
            }
        });
        sessionsList.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sessionsListKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(sessionsList);

        jCalendarButton1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jCalendarButton1PropertyChange(evt);
            }
        });

        jTimeButton1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jTimeButton1PropertyChange(evt);
            }
        });

        jLabel5.setText("repeating each");

        duration.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(90), Integer.valueOf(1), null, Integer.valueOf(15)));

        jLabel2.setText("starting at");

        jLabel6.setText("lasting");

        jLabel3.setText("sessions");

        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        sessionCount.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(10), Integer.valueOf(1), null, Integer.valueOf(1)));

        jLabel7.setText("minutes");

        jLabel1.setText("Add");

        backButton.setText("Back");

        viewButton.setText("View Attendance");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(startTime)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(sessionCount)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel3)
                                        .addGap(6, 6, 6))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(duration, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCalendarButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTimeButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(repeatOffset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addButton))
                        .addGap(106, 106, 106))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(backButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(viewButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(continueButton)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(sessionCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(startTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)))
                    .addComponent(jTimeButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCalendarButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(duration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(repeatOffset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(continueButton)
                    .addComponent(backButton)
                    .addComponent(viewButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void startTimeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_startTimeFocusLost
        setStartDateTime( startTime.getText() );
}//GEN-LAST:event_startTimeFocusLost

    private void deleteSelectedSessions() {
            List<Session> sessionsToRemove = new LinkedList<Session>();
            for ( Object obj : sessionsList.getSelectedValues() )
            {
                Session session = ( Session ) obj;
                sessionsToRemove.add( session );
            }
            List<String> errors = new LinkedList<String>();
            for ( Session session : sessionsToRemove )
            {
                if ( !store.sessions().remove( session ) )
                {
                    if ( !store.getRegister( session ).isEmpty() )
                    {
                        errors.add( "Could not delete session '" + session
                                + "': the attendance register is not empty" );
                    }
                    else
                    {
                        errors.add( "Could not delete session '" + session
                                + "': an unknown problem occurred" );
                    }
                }
            }

            if ( errors.size() == 1 )
            {
                JOptionPane.showMessageDialog( this,
                                               errors.get( 0 ),
                                               "Cannot delete session",
                                               JOptionPane.ERROR_MESSAGE );
            }
            else if ( errors.size() >= 1 )
            {
                StringBuilder sb = new StringBuilder( "Errors occurred:" );
                for ( String error : errors )
                {
                    sb.append( '\n' );
                    sb.append( error );
                }
                JOptionPane.showMessageDialog( this,
                                               sb,
                                               "Cannot delete sessions",
                                               JOptionPane.ERROR_MESSAGE );
            }
    }

private void sessionsListKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sessionsListKeyPressed
        if ( evt.getKeyCode() == KeyEvent.VK_DELETE )
        {
            deleteSelectedSessions();
        }
}//GEN-LAST:event_sessionsListKeyPressed

private void jCalendarButton1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jCalendarButton1PropertyChange
        if ( evt.getNewValue() instanceof Date )
        {
            setStartDateTime( (Date) evt.getNewValue() );
        }
}//GEN-LAST:event_jCalendarButton1PropertyChange

private void jTimeButton1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jTimeButton1PropertyChange
        if ( evt.getNewValue() instanceof Date )
        {
            setStartDateTime( (Date) evt.getNewValue() );
        }
}//GEN-LAST:event_jTimeButton1PropertyChange

private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        Calendar startCal = Calendar.getInstance();
        startCal.setTime( firstStartDate );
        Calendar endCal = (Calendar) startCal.clone();
        endCal.add( Calendar.MINUTE, (Integer)duration.getValue() );

        int count = (Integer)sessionCount.getValue();
        int overlapCount = 0;
        LinkedList<String> errors = null;

        for (int i = 0; i < count; ++i) {
            try
            {
				Session session = new Session( startCal.getTime(), endCal.getTime() );
                store.sessions().add( session );
            }
            catch ( IllegalArgumentException ex )
            {
                if (errors == null)
                    errors = new LinkedList<String>();
                errors.addLast( ex.getMessage() );
            }
            catch ( OverlappingSessionsException ex )
            {
                ++overlapCount;
            }
            int field = Calendar.DAY_OF_YEAR;
            if ("week".equals( repeatOffset.getSelectedItem() )) {
                field = Calendar.WEEK_OF_YEAR;
            } else if ("month".equals( repeatOffset.getSelectedItem() )) {
                field = Calendar.MONTH;
            }
            startCal.add( field, 1 );
            endCal.add( field, 1 );
        }
        if (overlapCount > 0) {
                JOptionPane.showMessageDialog( this,
                                               "Some sessions would overlap with existing sessions, and so were not created",
                                               "Could not add all sessions",
                                               JOptionPane.ERROR_MESSAGE );
        }
        if (errors != null) {
                JOptionPane.showMessageDialog( this,
                                               "Errors occurred when creating some sessions",
                                               "Could not add all sessions",
                                               JOptionPane.ERROR_MESSAGE );
        }
}//GEN-LAST:event_addButtonActionPerformed
    
    private void maybeShowPopup(java.awt.event.MouseEvent evt)
    {
        if (evt.isPopupTrigger()) {
            int index = sessionsList.locationToIndex(evt.getPoint());
            int[] selected = sessionsList.getSelectedIndices();
            if (selected.length == 0 || Arrays.binarySearch(selected, index) < 0) {
                sessionsList.setSelectedIndex(index);
            }
            sessionPopup.show(evt.getComponent(), evt.getX(), evt.getY());
        }
    }

    private void sessionsListMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_sessionsListMouseReleased
    {//GEN-HEADEREND:event_sessionsListMouseReleased
        maybeShowPopup(evt);
    }//GEN-LAST:event_sessionsListMouseReleased

    private void sessionsListMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_sessionsListMousePressed
    {//GEN-HEADEREND:event_sessionsListMousePressed
        maybeShowPopup(evt);
    }//GEN-LAST:event_sessionsListMousePressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton backButton;
    private javax.swing.JButton continueButton;
    private javax.swing.JSpinner duration;
    private net.sourceforge.jcalendarbutton.JCalendarButton jCalendarButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private net.sourceforge.jcalendarbutton.JTimeButton jTimeButton1;
    private javax.swing.JComboBox repeatOffset;
    private javax.swing.JSpinner sessionCount;
    private javax.swing.JList sessionsList;
    private javax.swing.JTextField startTime;
    private javax.swing.JButton viewButton;
    // End of variables declaration//GEN-END:variables
}
