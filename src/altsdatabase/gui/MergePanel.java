/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MergePanel.java
 *
 * Created on 07-Oct-2011, 22:06:19
 */
package altsdatabase.gui;

import java.awt.event.ActionListener;
import java.util.Collection;

/**
 *
 * @author alex
 */
public class MergePanel extends javax.swing.JPanel
{
    /** Creates new form MergePanel */
    public MergePanel(boolean success, String errorMessage, Collection<String> warnings)
    {
        initComponents();
        if (!success) {
            resultLabel.setText("Merge failed: " + errorMessage);
        }
        for (String s : warnings)
        {
            warningsTextArea.append(s);
            warningsTextArea.append("\n");
        }
    }

    public void addBackActionListener(ActionListener l)
    {
        backButton.addActionListener(l);
    }

    public void removeBackActionListener(ActionListener l)
    {
        backButton.removeActionListener(l);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        warningsTextArea = new javax.swing.JTextArea();
        backButton = new javax.swing.JButton();
        resultLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        warningsTextArea.setColumns(20);
        warningsTextArea.setEditable(false);
        warningsTextArea.setRows(5);
        jScrollPane1.setViewportView(warningsTextArea);

        backButton.setText("Back");

        resultLabel.setText("<html>The merge was successful</html>");

        jLabel1.setText("Warnings:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                    .addComponent(backButton, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(resultLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(resultLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(backButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel resultLabel;
    private javax.swing.JTextArea warningsTextArea;
    // End of variables declaration//GEN-END:variables
}
