/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.models;

import altsdatabase.data.Member;
import ca.odell.glazedlists.TextFilterator;
import ca.odell.glazedlists.matchers.Matcher;
import java.util.List;

/**
 *
 * @author alex
 */
public final class MemberFilterUtils {
	private static void maybeAdd( List<String> baseList, String item )
	{
		if ( item != null && item.length() > 0 )
		{
			baseList.add( item );
		}
	}

	public static final TextFilterator<Member> MEMBERSHIP_CODE_FILTERATOR =
			new TextFilterator<Member>() {
		public void getFilterStrings( List<String> baseList, Member element )
		{
			maybeAdd( baseList, element.getMembershipCode() );
		}
	};

	public static final TextFilterator<Member> CONTACT_DETAILS_FILTERATOR =
			new TextFilterator<Member>() {
		public void getFilterStrings( List<String> baseList, Member element )
		{
			maybeAdd( baseList, element.getContactDetails() );
		}
	};

	public static final TextFilterator<Member> NAME_FILTERATOR =
			new TextFilterator<Member>() {
		public void getFilterStrings( List<String> baseList, Member element )
		{
			maybeAdd( baseList, element.getName() );
		}
	};

	public static final TextFilterator<Member> ALL_FIELDS_FILTERATOR =
			new TextFilterator<Member>() {
		public void getFilterStrings( List<String> baseList, Member element )
		{
			maybeAdd( baseList, element.getMembershipCode() );
			maybeAdd( baseList, element.getName() );
			maybeAdd( baseList, element.getContactDetails() );
		}
	};

	public static class MembershipCodeMatcher implements Matcher<Member>
	{
		private String code;

		public MembershipCodeMatcher( String code )
		{
			this.code = code;
		}

		public boolean matches( Member item )
		{
			if (code != null)
			{
				if (!item.getMembershipCode().equals( code ))
					return false;
			}
			return true;
		}
	}

    public static class NameMatcher implements Matcher<Member>
    {
        private String name;

        public NameMatcher( String name )
        {
            this.name = name;
        }

        public boolean matches( Member item )
        {
            return name == null || item.getName().equalsIgnoreCase( name );
        }
    }
}
