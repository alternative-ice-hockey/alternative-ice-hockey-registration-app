/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.models;

import altsdatabase.data.Member;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author alex
 */
public class MemberStatusComboBoxModel
	extends AbstractListModel
	implements ComboBoxModel
{
    private Member.Status selectedStatus;

    public int getSize ()
    {
	return Member.Status.values ().length;
    }

    public Object getElementAt (int index)
    {
	return Member.Status.values ()[index];
    }

    public void setSelectedItem (Object anItem)
    {
	selectedStatus = (Member.Status)anItem;
        fireContentsChanged( this, 0, getSize() - 1 );
    }

    public Object getSelectedItem ()
    {
	return selectedStatus;
    }

}
