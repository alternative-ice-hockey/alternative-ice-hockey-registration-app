/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.models;

import altsdatabase.data.DuplicateEntryException;
import altsdatabase.data.Member;
import altsdatabase.data.ProhibitedChangeException;
import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;

/**
 *
 * @author alex
 */
public class MemberTableFormat implements WritableTableFormat<Member>, AdvancedTableFormat<Member>
{
	boolean editable = false;

	public MemberTableFormat()
	{
	}

	public MemberTableFormat(boolean editable)
	{
		this.editable = editable;
	}

	public void setEditable( boolean editable )
	{
		this.editable = editable;
	}

    private final static String[] columnNames = {
        "Membership code",
        "Name",
        "Contact Details",
        "College",
        "Status"
    };
    public final static int MEMBERSHIP_CODE_COLUMN = 0;
    public final static int NAME_COLUMN = 1;
    public final static int CONTACT_DETAILS_COLUMN = 2;
    public final static int COLLEGE_COLUMN = 3;
    public final static int STATUS_COLUMN = 4;

	public boolean isEditable( Member baseObject, int column )
	{
		return editable;
	}

	public Member setColumnValue( Member baseObject, Object editedValue, int column )
	{
		try
        {
            switch ( column )
            {
                case MEMBERSHIP_CODE_COLUMN:
                    baseObject.setMembershipCode( editedValue.toString() );
                    return baseObject;
                case NAME_COLUMN:
                    baseObject.setName( editedValue.toString() );
                    return baseObject;
                case CONTACT_DETAILS_COLUMN:
                    baseObject.setContactDetails( editedValue.toString() );
                    return baseObject;
                case COLLEGE_COLUMN:
                    baseObject.setCollege( editedValue.toString() );
                    return baseObject;
                case STATUS_COLUMN:
                    baseObject.setStatus ( (Member.Status)editedValue );
                    return baseObject;
				default:
					throw new IndexOutOfBoundsException();
            }
        }
        catch ( ProhibitedChangeException ex )
        {
        }
        catch ( DuplicateEntryException ex )
        {
        }
        catch ( IllegalArgumentException ex )
        {
        }
		return null;
	}

	public int getColumnCount()
	{
		return columnNames.length;
	}

	public String getColumnName( int column )
	{
		return columnNames[column];
	}

	public Object getColumnValue( Member member, int column )
	{
        switch ( column )
        {
            case MEMBERSHIP_CODE_COLUMN:
                return member.getMembershipCode();
            case NAME_COLUMN:
                return member.getName();
            case CONTACT_DETAILS_COLUMN:
                return member.getContactDetails();
            case COLLEGE_COLUMN:
                return member.getCollege();
            case STATUS_COLUMN:
                return member.getStatus ();
            default:
                throw new IndexOutOfBoundsException();
        }
	}

	public Class getColumnClass( int column )
	{
		if (column == STATUS_COLUMN)
			return Member.Status.class;
		return String.class;
	}

	private Comparator toStringComparator = new Comparator()
	{
		public int compare( Object o1, Object o2 )
		{
			if (o1 == o2)
				return 0;
			if (o1 == null)
				return -1;
			if (o2 == null)
				return 1;
			return o1.toString().compareTo( o2.toString() );
		}
	};

	public Comparator getColumnComparator( int column )
	{
		if (column == STATUS_COLUMN)
		{
			return toStringComparator;
		}
		return String.CASE_INSENSITIVE_ORDER;
	}

}
