/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.models;

import altsdatabase.data.Session;
import ca.odell.glazedlists.matchers.Matcher;
import java.util.Date;

/**
 *
 * @author alex
 */
public final class SessionFilterUtils {

	public static class UpcomingMatcher implements Matcher<Session>
	{
		private Date createdAt;

		public UpcomingMatcher()
		{
			createdAt = new Date();
		}

		public boolean matches( Session item )
		{
			return item.getEndTime().after( createdAt );
		}
	}
}
