/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.models;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Wraps a ListModel to produce a ComboBoxModel
 */
class ComboBoxModelWrapper extends AbstractListModel
                           implements ComboBoxModel, ListDataListener {
    private ListModel internal;
    private int selectedIndex = -1;

    public ComboBoxModelWrapper( ListModel model )
    {
        internal = model;
        model.addListDataListener( this );
        if ( model.getSize() > 0 )
            selectedIndex = 0;
    }

    public int getSize()
    {
        return internal.getSize();
    }

    public Object getElementAt( int index )
    {
        return internal.getElementAt( index );
    }

    public void setSelectedItem( Object anItem )
    {
        if ( anItem == null )
        {
            selectedIndex = -1;
        }
        else
        {
            int index = 0;
            for ( ; index < getSize(); ++index )
            {
                if ( getElementAt( index ).equals( anItem ) )
                    break;
            }
            if ( index == getSize() )
                throw new IllegalArgumentException(
                        "The item was not in this model" );
            selectedIndex = index;
        }
        fireContentsChanged( this, 0, getSize() - 1 );
    }

    public Object getSelectedItem()
    {
        if ( selectedIndex == -1 )
            return null;
        else
            return getElementAt( selectedIndex );
    }

    public void intervalAdded( ListDataEvent e )
    {
        fireIntervalAdded( this, e.getIndex0(), e.getIndex1() );
    }

    public void intervalRemoved( ListDataEvent e )
    {
        if ( (selectedIndex >= e.getIndex0() &&
              selectedIndex <= e.getIndex1()) ||
                (selectedIndex <= e.getIndex0() &&
                 selectedIndex >= e.getIndex1()))
        {
            selectedIndex = -1;
        }
        fireIntervalRemoved( this, e.getIndex0(), e.getIndex1() );
    }

    public void contentsChanged( ListDataEvent e )
    {
        fireContentsChanged( this, e.getIndex0(), e.getIndex1() );
    }
}
