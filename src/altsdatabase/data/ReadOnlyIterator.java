/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.util.Iterator;

/**
 * A simple wrapper around Iterator that throws an
 * UnsupportedOperationException when remove() is called
 */
public class ReadOnlyIterator<T> implements Iterator<T>
{
	Iterator<T> internalIterator;

	public ReadOnlyIterator( Iterator<T> internalIterator )
	{
		if ( internalIterator == null )
		{
			throw new NullPointerException();
		}
		this.internalIterator = internalIterator;
	}

	public boolean hasNext()
	{
		return internalIterator.hasNext();
	}

	public T next()
	{
		return internalIterator.next();
	}

	public void remove()
	{
		throw new UnsupportedOperationException( "Not supported yet." );
	}
}
