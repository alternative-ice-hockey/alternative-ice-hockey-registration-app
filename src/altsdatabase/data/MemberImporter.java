/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import altsdatabase.data.Member.Status;
import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
public class MemberImporter
{
	private DataStore target;
    private int membershipCodeIndex = -1;
    private int fullNameIndex = -1;
    private int firstNameIndex = -1;
    private int lastNameIndex = -1;
    private int contactIndex = -1;
    private int collegeIndex = -1;
    private int statusIndex = -1;
    private Member.Status defaultStatus = Member.Status.OUStudent;

	public MemberImporter( DataStore target )
	{
		this.target = target;
	}

    public int getCollegeIndex()
    {
        return collegeIndex;
    }

    public void setCollegeIndex( int collegeIndex )
    {
        this.collegeIndex = collegeIndex;
    }

    public int getContactIndex()
    {
        return contactIndex;
    }

    public void setContactIndex( int contactIndex )
    {
        this.contactIndex = contactIndex;
    }

    public Status getDefaultStatus()
    {
        return defaultStatus;
    }

    public void setDefaultStatus( Status defaultStatus )
    {
        this.defaultStatus = defaultStatus;
    }

    public int getFirstNameIndex()
    {
        return firstNameIndex;
    }

    public void setFirstNameIndex( int firstNameIndex )
    {
        this.firstNameIndex = firstNameIndex;
    }

    public int getLastNameIndex()
    {
        return lastNameIndex;
    }

    public void setLastNameIndex( int lastNameIndex )
    {
        this.lastNameIndex = lastNameIndex;
    }

    public void setFullNameIndex( int fullNameIndex )
    {
        this.fullNameIndex = fullNameIndex;
    }

    public int getFullNameIndex()
    {
        return fullNameIndex;
    }

    public int getMembershipCodeIndex()
    {
        return membershipCodeIndex;
    }

    public void setMembershipCodeIndex( int membershipCodeIndex )
    {
        this.membershipCodeIndex = membershipCodeIndex;
    }

    public int getStatusIndex()
    {
        return statusIndex;
    }

    public void setStatusIndex( int statusIndex )
    {
        this.statusIndex = statusIndex;
    }

	public int importCSVFile( File csvFile )
	{
		return importCSVFile( csvFile, Logger.getLogger( "altsdatabase" ) );
	}

    private String getName( String[] line )
    {
        if (fullNameIndex >= 0)
            return line[fullNameIndex];
        else
            return line[firstNameIndex] + " " + line[lastNameIndex];
    }

    private String getOptItem( String[] line, int index )
    {
        if (index > 0 && line.length > index && line[index] != null)
            return line[index];
        return "";
    }

    private boolean hasOptItem( String[] line, int index )
    {
        return index > 0 &&
               line.length > index &&
               line[index] != null &&
               line[index].length() > 0;
    }

	public int importCSVFile( File csvFile, Logger errorLog )
	{
        if (fullNameIndex < 0 && (firstNameIndex < 0 || lastNameIndex < 0)) {
            throw new IllegalStateException(
                    "An index must be provided for either "
                    + "full name or first name and last name" );
        }
        if (membershipCodeIndex < 0) {
            throw new IllegalStateException(
                    "An index must be provided for membership code" );
        }
        int maxIndex = membershipCodeIndex;
        maxIndex = (maxIndex < fullNameIndex) ? fullNameIndex : maxIndex;
        maxIndex = (maxIndex < firstNameIndex) ? firstNameIndex : maxIndex;
        maxIndex = (maxIndex < lastNameIndex) ? lastNameIndex : maxIndex;

		int imports = 0;
		try
		{
			FileReader fileReader = new FileReader( csvFile );
			CSVReader reader = new CSVReader( fileReader );
			String[] nextLine;
			int line = 0;
			while ( (nextLine = reader.readNext()) != null )
			{
				++line;
				if ( nextLine.length > 0 )
				{
					try
					{
						if ( nextLine.length < maxIndex - 1 )
						{
							errorLog.log( Level.WARNING, "Bad format at line {0}: not enough fields", line );
                            continue;
						}

                        String code = nextLine[membershipCodeIndex];
                        if ( code.isEmpty() ) {
                            // ignore lines with no membership code
                            continue;
                        }

                        String name = getName( nextLine );
						if ( name.isEmpty() )
						{
							errorLog.log( Level.WARNING, "Bad format at line {0}: no name", line );
                            continue;
						}

                        if ( target.getQueryManager().hasMemberWithCode( code ) )
                        {
                            Member match = null;
                            for ( Member m : target.members() ) {
                                if ( m.getMembershipCode().equals( code ) ) {
                                    match = m;
                                    break;
                                }
                            }
                            if ( match.getName().equalsIgnoreCase( name ) )
                            {
                                errorLog.log( Level.INFO,
                                              "The entry at line {0} " +
                                              "seems to already be in " +
                                              "the membership roster; " +
                                              "updating details", line );
                                if ( hasOptItem( nextLine, contactIndex )
                                        && match.getContactDetails().length() == 0 )
                                {
                                    match.setContactDetails( nextLine[contactIndex] );
                                }
                                if ( hasOptItem( nextLine, collegeIndex )
                                        && match.getCollege().length() == 0 )
                                {
                                    match.setCollege( nextLine[collegeIndex] );
                                }
                            }
                            else
                            {
                                errorLog.log( Level.INFO,
                                              "The entry at line {0}" +
                                              "has the same " +
                                              "membership code as " +
                                              "{1}; ignoring",
                                              new Object[] { line, match } );
                            }
                            continue;
                        }

                        Member.Status status = defaultStatus;
                        if (hasOptItem( nextLine, statusIndex ) )
                        {
                            try {
                                status = Member.Status.valueOf( nextLine[statusIndex] );
                            } catch (IllegalArgumentException ex) {
                                errorLog.log( Level.WARNING,
                                              "Invalid status {0} given at line {1}",
                                              new Object[] { nextLine[statusIndex], line } );
                            }
                        }
                        Member member = new Member( code,
                                                    name,
                                                    getOptItem( nextLine, contactIndex ),
                                                    getOptItem( nextLine, collegeIndex ),
                                                    status );
                        target.members().add( member );
                        ++imports;
					}
					catch ( DuplicateMembershipCodeException ex )
					{
						errorLog.log( Level.INFO, "There is already a member with membership code {0}", nextLine[membershipCodeIndex] );
					}
					catch ( IllegalArgumentException ex )
					{
						errorLog.log( Level.WARNING, "Bad data: {0}: {1}", new Object[]
								{
									nextLine, ex.getMessage()
								} );
					}
					catch ( ProhibitedChangeException ex )
					{
						errorLog.log( Level.WARNING, "Bad data: {0}: {1}", new Object[]
								{
									nextLine, ex.getMessage()
								} );
					}
					catch ( SQLException ex )
					{
						errorLog.log( Level.WARNING, "Internal error: {0}: {1}", new Object[]
								{
									nextLine, ex.getMessage()
								} );
					}
				}
			}
			reader.close();
			fileReader.close();
		}
		catch ( FileNotFoundException ex )
		{
			errorLog.log( Level.SEVERE, "Could not open {0}", csvFile.getName() );
		}
		catch ( IOException ex )
		{
			errorLog.log( Level.SEVERE, "There was an error importing the data: {0}", ex.getMessage() );
		}
		return imports;
	}
}
