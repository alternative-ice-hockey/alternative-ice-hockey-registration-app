/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import au.com.bytecode.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author alex
 */
public class DataExporter
{
	private DataStore store;

	public DataExporter( DataStore store )
	{
		this.store = store;
	}

	public void exportBodCardCheckDigits( File csvFile )
			throws IOException
	{
		if ( csvFile.exists() && !csvFile.isFile() )
		{
			throw new IOException( csvFile + " is not a file" );
		}

		CSVWriter writer = new CSVWriter( new FileWriter( csvFile ) );

		try
		{
			Statement st = store.getConnection().createStatement();
			ResultSet results = st.executeQuery( "SELECT * FROM BodCardLog ORDER BY checksum, checkdigit" );
			writer.writeAll( results, true );
			results.close();
			st.close();
		}
		catch ( SQLException sqe )
		{
			throw new Error( sqe.getMessage(), sqe );
		}
		finally
		{
			writer.close();
		}
	}
}
