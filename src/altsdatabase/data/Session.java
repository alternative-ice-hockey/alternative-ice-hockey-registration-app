/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import altsdatabase.data.event.SessionEvent;
import altsdatabase.data.event.SessionListener;
import java.util.Date;
import java.util.UUID;
import javax.swing.event.EventListenerList;

/**
 *
 * @author alex
 */
public class Session implements Comparable<Session>
{
	private UUID sessionid;
	private Date startTime;
	private Date endTime;
	private EventListenerList listenerList = new EventListenerList();

	Session( UUID sessionid,
			 Date startTime,
			 Date endTime )
	{
		this.sessionid = sessionid;
		this.startTime = (Date) startTime.clone();
		this.endTime = (Date) endTime.clone();
	}

	public Session( Date startTime,
					Date endTime )
	{
		this( null, startTime, endTime );
	}

	public void addSessionListener( SessionListener listener )
	{
		listenerList.add( SessionListener.class, listener );
	}

	public void removeSessionListener( SessionListener listener )
	{
		listenerList.remove( SessionListener.class, listener );
	}

	@Override
	public String toString()
	{
		long timeDiff = endTime.getTime() - startTime.getTime(); // ms
		timeDiff /= (1000 * 60); // mins
		return String.format( "%1$ta %1$te %1$tb %1$tY at %1$tR (%2$d mins)",
							  startTime,
							  timeDiff );
	}

	/** Checks whether two Sessions are equal.
	 *
	 * Two sessions are considered equal if they start and end at the
	 * same times.
	 *
	 * Note that if both terms are from the same datastore, prevention of
	 * overlapping sessions means that this is equivalent to ==.
	 *
	 * @param obj  The object to compare to.
	 * @return  @c true if the obj is a Session and happens at exactly the
	 *          same time as this session, @c false otherwise.
	 */
	@Override
	public boolean equals( Object obj )
	{
		if ( !(obj instanceof Session) )
		{
			return false;
		}
		Session other = (Session) obj;
		boolean equals = true;
		if ( startTime != null )
		{
			equals &= startTime.equals( other.startTime );
		}
		else
		{
			equals &= other.startTime == null;
		}
		if ( endTime != null )
		{
			equals &= endTime.equals( other.endTime );
		}
		else
		{
			equals &= other.endTime == null;
		}
		return equals;
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 97 * hash + (this.startTime != null ? this.startTime.hashCode() : 0);
		hash = 97 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
		return hash;
	}

	/** Orders Sessions by date
	 *
	 * Ordering is based on the starting time of the session, and if two
	 * sessions start at the same time (and are not the same session), the
	 * shorter session is considered the lesser one.
	 *
	 * Note that if both terms are from the same datastore, prevention of
	 * overlapping sessions means that only the starting time of the session
	 * is relevant.
	 *
	 * @param o
	 * @return
	 */
	public int compareTo( Session o )
	{
		int startComparison = 0;

		if ( this.startTime != null )
		{
			startComparison = this.startTime.compareTo( o.startTime );
		}
		else if ( o.startTime != null )
		{
			startComparison = -o.startTime.compareTo( this.startTime );
		}

		if ( startComparison == 0 )
		{
			if ( this.endTime != null )
			{
				return this.endTime.compareTo( o.endTime );
			}
			else if ( o.endTime != null )
			{
				return -o.endTime.compareTo( this.endTime );
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return startComparison;
		}
	}

	UUID getId()
	{
		return sessionid;
	}

	void setId( UUID id )
	{
		this.sessionid = id;
	}

	protected void fireSessionChanged( Date oldStart, Date oldEnd )
	{
		SessionEvent event = null;
		Object[] listeners = listenerList.getListenerList();
		for ( int i = listeners.length - 2; i >= 0; i -= 2 )
		{
			if ( listeners[i] == SessionListener.class )
			{
				// Lazily create the event:
				if ( event == null )
				{
					event = new SessionEvent( this, oldStart, oldEnd );
				}
				((SessionListener) listeners[i + 1]).sessionChanged( event );
			}
		}
	}

	protected boolean isSessionChangeAllowed( Date newStart, Date newEnd )
	{
		Object[] listeners = listenerList.getListenerList();
		for ( int i = listeners.length - 2; i >= 0; i -= 2 )
		{
			if ( listeners[i] == SessionListener.class )
			{
				boolean allow = ((SessionListener) listeners[i + 1]).aboutToChangeSession( this, newStart, newEnd );
				if ( !allow )
				{
					return false;
				}
			}
		}
		return true;
	}

	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime( Date newStartTime )
			throws ProhibitedChangeException
	{
		if ( !newStartTime.before( endTime ) )
		{
			throw new IllegalArgumentException(
					"Start time must be before end time" );
		}
		if ( newStartTime.equals( startTime ) )
		{
			return;
		}
		if ( !isSessionChangeAllowed( newStartTime, endTime ) )
		{
			throw new ProhibitedChangeException();
		}

		Date oldStartTime = new Date( this.startTime.getTime() );
		this.startTime.setTime( newStartTime.getTime() );
		fireSessionChanged( oldStartTime, endTime );
	}

	public Date getEndTime()
	{
		return endTime;
	}

	public void setEndTime( Date newEndTime )
			throws ProhibitedChangeException
	{
		if ( !newEndTime.after( startTime ) )
		{
			throw new IllegalArgumentException(
					"End time must be after start time" );
		}
		if ( newEndTime.equals( endTime ) )
		{
			return;
		}
		if ( !isSessionChangeAllowed( startTime, newEndTime ) )
		{
			throw new ProhibitedChangeException();
		}

		Date oldEndTime = new Date( this.endTime.getTime() );
		this.endTime.setTime( newEndTime.getTime() );
		fireSessionChanged( startTime, oldEndTime );
	}

	public void setStartAndEndTime( Date startTime, Date endTime )
			throws ProhibitedChangeException
	{
		if ( !startTime.before( endTime ) )
		{
			throw new IllegalArgumentException(
					"Start time cannot be before end time" );
		}
		if ( startTime.equals( this.startTime ) && endTime.equals( this.endTime ) )
		{
			return;
		}
		if ( !isSessionChangeAllowed( startTime, endTime ) )
		{
			throw new ProhibitedChangeException();
		}

		Date oldStartTime = new Date( this.startTime.getTime() );
		Date oldEndTime = new Date( this.endTime.getTime() );
		this.startTime.setTime( startTime.getTime() );
		this.endTime.setTime( endTime.getTime() );
		fireSessionChanged( oldStartTime, oldEndTime );
	}
}
