/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.io.IOException;

/**
 *
 * @author alex
 */
public class TooRecentFileException extends IOException
{
	public TooRecentFileException()
	{
		super( "The file format is too recent" );
	}

	public TooRecentFileException( String message )
	{
		super( message );
	}
}
