/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import altsdatabase.data.event.SessionEvent;
import altsdatabase.data.event.SessionListener;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alex
 */
public class SessionManager
{
	private static Logger logger = LoggerFactory.getLogger( SessionManager.class );
	private DataStore datastore;
	private SessionList sessions = new SessionList();
	private SortedList<Session> sortedSessions;
	private SessionListener sessionListener = new SessionListener()
	{
		public void sessionChanged( SessionEvent event )
		{
			updateSessionInDatabase( event.getSession() );
		}

		public boolean aboutToChangeSession( Session session, Date newStart, Date newEnd )
		{
			for ( Session s : sessions )
			{
				if ( session != s )
				{
					if ( !newEnd.before( s.getStartTime() )
							&& !newStart.after( s.getEndTime() ) )
					{
						return false;
					}
				}
			}
			return true;
		}
	};

	private class SessionList extends GuardedEventList<Session>
	{
		@Override
		protected void checkElementAddition( Session element )
		{
			if ( element.getId() != null )
			{
				throw new IllegalArgumentException( "Session already in a database" );
			}
			for ( Session s : this )
			{
				if ( element.getEndTime().after( s.getStartTime() )
						&& element.getStartTime().before( s.getEndTime() ) )
				{
					throw new OverlappingSessionsException();
				}
			}
		}
	}

	private ListEventListener<Session> sessionListListener = new ListEventListener<Session>()
	{
        private boolean ignore = false;
		public void listChanged( ListEvent<Session> le )
		{
            if (ignore)
                return;

            LinkedList<Session> toReAdd = new LinkedList<Session>();
            LinkedList<Integer> toRemove = new LinkedList<Integer>();
			while ( le.next() )
			{
				if ( le.getType() == ListEvent.DELETE )
				{
					Session oldSession = le.getOldValue();
					if ( oldSession == ListEvent.UNKNOWN_VALUE )
					{
						logger.error( "Failed to get old session value!" );
					}
					else
					{
						oldSession.removeSessionListener( sessionListener );
						if (!deleteSessionFromDatabase( oldSession )) {
                            //re-add the session
                            toReAdd.add(oldSession);
                        }
					}
				}
				else if ( le.getType() == ListEvent.INSERT )
				{
					Session session = le.getSourceList().get( le.getIndex() );
					if (insertSessionIntoDatabase( session ))
                        session.addSessionListener( sessionListener );
                    else
                        toRemove.add(le.getIndex());
				}
			}
            ignore = true;
            try {
                for (int index: toRemove) {
                    le.getSourceList().remove(index);
                }
                for (Session s: toReAdd) {
                    le.getSourceList().add(s);
                }
            } finally {
                ignore = false;
            }
		}
	};

	SessionManager( DataStore datastore )
	{
		this.datastore = datastore;
        loadAll();
		sessions.addListEventListener( sessionListListener );
	}

    void reload()
    {
        try {
            sessions.removeListEventListener(sessionListListener);
        } catch (Exception ex) {
            logger.warn("Failed to remove listener from sessions list", ex);
        }
        for (Session session : sessions)
        {
            try {
                session.removeSessionListener(sessionListener);
            } catch (Exception ex) {
                logger.warn("Failed to remove listener from session "
                        + session.toString(), ex);
            }
        }
        sessions.clear();
        loadAll();
		sessions.addListEventListener(sessionListListener);
    }

    private void loadAll()
    {
		try
		{
			ResultSet results = datastore.getQueryManager().lookupAllSessions();
			while ( results.next() )
			{
				Session session = new Session(
						QueryManager.sessionIdFromResult( results, "sessionid" ),
						results.getTimestamp( "starttime" ),
						results.getTimestamp( "endtime" ) );
				sessions.uncheckedAdd( session );
				session.addSessionListener( sessionListener );
			}
			results.close();
		}
		catch ( SQLException sqe )
		{
			logger.error( "Failed to load sessions", sqe );
			throw new Error( sqe.getMessage(), sqe );
		}
    }

	public EventList<Session> allSessions()
	{
		if (sortedSessions == null)
		{
			sortedSessions = new SortedList<Session>( sessions );
		}
		return sortedSessions;
	}

	private boolean insertSessionIntoDatabase( Session session )
	{
		try
		{
			UUID id = datastore.getQueryManager().insertSession( session.getStartTime(), session.getEndTime() );
			session.setId( id );
            return true;
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to add session {} to the database", session );
			logger.error( ex.getMessage(), ex );
		}
        return false;
	}

	private void updateSessionInDatabase( Session session )
	{
		if ( session.getId() != null )
		{
			try
			{
				datastore.getQueryManager().updateSession(
						session.getId(),
						session.getStartTime(),
						session.getEndTime() );
			}
			catch ( SQLException ex )
			{
				logger.error( "Failed to update session {} in the database", session );
				logger.error( ex.getMessage(), ex );
			}
		}
	}

	private boolean deleteSessionFromDatabase( Session session )
	{
		try
		{
			datastore.getQueryManager().deleteSession( session.getId() );
			session.setId( null );
            return true;
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to remove session {} from the database", session );
			logger.error( ex.getMessage(), ex );
		}
        return false;
	}
}

// vim:ts=4:sw=4:sts=4