CREATE TABLE BodCardLog (
    checksum int NOT NULL,
    checkdigit char(1) NOT NULL,
    seencount int DEFAULT 1,
    CONSTRAINT PK_BodCardLog
        PRIMARY KEY ( checksum, checkdigit )
);

UPDATE Admin SET int_value = 2 WHERE key = 'db_version';