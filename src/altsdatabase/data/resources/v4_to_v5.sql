DROP VIEW PrePaySessionMembers;
DROP TABLE PrePaySessionGroupsPayments;
DROP TABLE PrePaySessionGroupsSessions;
DROP TABLE PrePaySessionGroups;

DROP VIEW MembersView;

CREATE FUNCTION gen_uuid()
    RETURNS char(36)
    NO SQL
    LANGUAGE JAVA
    SPECIFIC gen_uuid_1
    EXTERNAL NAME 'CLASSPATH:altsdatabase.data.DataStore.randomUUID';

ALTER TABLE Members RENAME TO Members_old;
ALTER TABLE Sessions RENAME TO Sessions_old;
ALTER TABLE Attendance RENAME TO Attendance_old;

CREATE TABLE Members (
    memberid char(36) NOT NULL,
    oldmemberid int,
    membershipcode varchar(20),
    name varchar(255),
    contact varchar(255),
    statusid integer NOT NULL,
    CONSTRAINT PK_Members
        PRIMARY KEY ( memberid ),
    CONSTRAINT FK_Members_statusid
        FOREIGN KEY ( statusid )
        REFERENCES MemberStatuses ( statusid ),
    CONSTRAINT UQ_Members_membershipcode
        UNIQUE ( membershipcode )
);

CREATE TABLE Sessions (
    sessionid char(36) NOT NULL,
    oldsessionid int,
    starttime timestamp NOT NULL,
    endtime timestamp NOT NULL,
    CONSTRAINT PK_Sessions
        PRIMARY KEY ( sessionid )
);

CREATE TABLE Attendance (
    memberid char(36) NOT NULL,
    sessionid char(36) NOT NULL,
    CONSTRAINT PK_Attendance
        PRIMARY KEY ( memberid, sessionid ),
    CONSTRAINT FK_Attendance_memberid
        FOREIGN KEY ( memberid )
        REFERENCES Members ( memberid ),
    CONSTRAINT FK_Attendance_sessionid
        FOREIGN KEY ( sessionid )
        REFERENCES Sessions ( sessionid )
);

INSERT INTO Members (
    SELECT gen_uuid(), memberid, membershipcode,
           (firstname + ' ' + lastname), contact, statusid
    FROM Members_old);

INSERT INTO Sessions (
    SELECT gen_uuid(), sessionid, starttime, endtime
    FROM Sessions_old);

INSERT INTO Attendance (
    SELECT Members.memberid, Sessions.sessionid
    FROM Attendance_old
    JOIN Members ON Members.oldmemberid = Attendance_old.memberid
    JOIN Sessions ON Sessions.oldsessionid = Attendance_old.sessionid);

DROP TABLE Attendance_old;
DROP TABLE Sessions_old;
DROP TABLE Members_old;

CREATE VIEW MembersView AS
    SELECT m.*, s.status
    FROM Members AS m,
    MemberStatuses as s
    WHERE m.statusid = s.statusid;

UPDATE Admin SET int_value = 5 WHERE key = 'db_version';