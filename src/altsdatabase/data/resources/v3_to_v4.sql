UPDATE Members
    SET statusid = (SELECT s.statusid
                    FROM MemberStatuses as s
                    WHERE s.status = 'Other')
    WHERE statusid = (SELECT s2.statusid
                      FROM MemberStatuses as s2
                      WHERE s2.status = 'OtherStudent');

DELETE FROM MemberStatuses WHERE status = 'OtherStudent';

UPDATE Admin SET int_value = 4 WHERE key = 'db_version';
