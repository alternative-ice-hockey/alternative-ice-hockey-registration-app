CREATE FUNCTION gen_uuid()
    RETURNS char(36)
    NO SQL
    LANGUAGE JAVA
    SPECIFIC gen_uuid_1
    EXTERNAL NAME 'CLASSPATH:altsdatabase.data.DataStore.randomUUID';


CREATE TABLE MemberStatuses (
    statusid integer GENERATED ALWAYS AS IDENTITY NOT NULL,
    status varchar(20),
    CONSTRAINT PK_MemberStatuses
        PRIMARY KEY ( statusid ),
    CONSTRAINT UQ_MemberStatuses_status
        UNIQUE ( status )
);


CREATE TABLE Members (
    memberid char(36) NOT NULL,
    membershipcode varchar(20),
    name varchar(255),
    contact varchar(255),
    college varchar(255),
    statusid integer NOT NULL,
    created timestamp DEFAULT LOCALTIMESTAMP NOT NULL,
    lastmodified timestamp DEFAULT LOCALTIMESTAMP NOT NULL,
    CONSTRAINT PK_Members
        PRIMARY KEY ( memberid ),
    CONSTRAINT FK_Members_statusid
        FOREIGN KEY ( statusid )
        REFERENCES MemberStatuses ( statusid ),
    CONSTRAINT UQ_Members_membershipcode
        UNIQUE ( membershipcode )
);


CREATE TABLE Sessions (
    sessionid char(36) NOT NULL,
    starttime timestamp NOT NULL,
    endtime timestamp NOT NULL,
    created timestamp DEFAULT LOCALTIMESTAMP NOT NULL,
    lastmodified timestamp DEFAULT LOCALTIMESTAMP NOT NULL,
    CONSTRAINT PK_Sessions
        PRIMARY KEY ( sessionid )
);


CREATE TABLE Attendance (
    memberid char(36) NOT NULL,
    sessionid char(36) NOT NULL,
    added timestamp DEFAULT LOCALTIMESTAMP NOT NULL,
    CONSTRAINT PK_Attendance
        PRIMARY KEY ( memberid, sessionid ),
    CONSTRAINT FK_Attendance_memberid
        FOREIGN KEY ( memberid )
        REFERENCES Members ( memberid ),
    CONSTRAINT FK_Attendance_sessionid
        FOREIGN KEY ( sessionid )
        REFERENCES Sessions ( sessionid )
);


CREATE TABLE Checkpoints (
    cpid char(36) NOT NULL,
    cptime timestamp NOT NULL,
    CONSTRAINT PK_Checkpoints
        PRIMARY KEY ( cpid ),
);


CREATE PROCEDURE insert_checkpoint()
    MODIFIES SQL DATA
    INSERT INTO Checkpoints VALUES (gen_uuid(), CURRENT_TIMESTAMP);


CREATE TABLE Admin (
    key varchar(30) NOT NULL,
    string_value varchar(50),
    int_value integer,
    CONSTRAINT PK_Admin
        PRIMARY KEY ( key )
);


CREATE TABLE BodCardLog (
    checksum int NOT NULL,
    checkdigit char(1) NOT NULL,
    seencount int DEFAULT 1,
    CONSTRAINT PK_BodCardLog
        PRIMARY KEY ( checksum, checkdigit )
);


CREATE VIEW MembersView AS
    SELECT m.*, s.status
    FROM Members AS m,
    MemberStatuses as s
    WHERE m.statusid = s.statusid;


INSERT INTO Admin (key,int_value) VALUES ('db_version',6);