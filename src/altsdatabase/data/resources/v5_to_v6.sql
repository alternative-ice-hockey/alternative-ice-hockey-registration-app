DROP VIEW MembersView;
ALTER TABLE Members ADD COLUMN college varchar(255) BEFORE statusid;
ALTER TABLE Members ADD COLUMN created timestamp DEFAULT LOCALTIMESTAMP NOT NULL;
ALTER TABLE Members ADD COLUMN lastmodified timestamp DEFAULT LOCALTIMESTAMP NOT NULL;
ALTER TABLE Sessions ADD COLUMN created timestamp DEFAULT LOCALTIMESTAMP NOT NULL;
ALTER TABLE Sessions ADD COLUMN lastmodified timestamp DEFAULT LOCALTIMESTAMP NOT NULL;
ALTER TABLE Attendance ADD COLUMN added timestamp DEFAULT LOCALTIMESTAMP NOT NULL;

UPDATE Members
    SET college = contact, contact = ''
    WHERE contact != '' AND contact NOT LIKE '%@%';

CREATE VIEW MembersView AS
    SELECT m.*, s.status
    FROM Members AS m,
    MemberStatuses as s
    WHERE m.statusid = s.statusid;

CREATE TABLE Checkpoints (
    cpid char(36) NOT NULL,
    cptime timestamp NOT NULL,
    CONSTRAINT PK_Checkpoints
        PRIMARY KEY ( cpid ),
);

CREATE PROCEDURE insert_checkpoint()
    MODIFIES SQL DATA
    INSERT INTO Checkpoints VALUES (gen_uuid(), CURRENT_TIMESTAMP);

UPDATE Admin SET int_value = 6 WHERE key = 'db_version';