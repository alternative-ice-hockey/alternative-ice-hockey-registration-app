SELECT * INTO BodCardLogTemp FROM BodCardLog;
DROP TABLE BodCardLog;

CREATE TABLE BodCardLog (
    checksum int NOT NULL,
    checkdigit char(1) NOT NULL,
    seencount int DEFAULT 1,
    CONSTRAINT PK_BodCardLog
        PRIMARY KEY ( checksum, checkdigit )
);

INSERT INTO BodCardLog SELECT *, 1 AS seencount FROM BodCardLogTemp;
DROP TABLE BodCardLogTemp;

UPDATE Admin SET int_value = 2 WHERE key = 'db_version';
