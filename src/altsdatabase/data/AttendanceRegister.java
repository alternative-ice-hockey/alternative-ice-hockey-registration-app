/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import ca.odell.glazedlists.AbstractEventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alex
 */
public final class AttendanceRegister extends AbstractEventList<Member>
{
	private static Logger logger = LoggerFactory.getLogger( AttendanceRegister.class );
	private Session session;
	private DataStore store;
	private ArrayList<Integer> sourceIndices = new ArrayList<Integer>();
	private ListEventListener<Member> sourceListener = new ListEventListener<Member>()
	{
		public void listChanged( ListEvent<Member> listChanges )
		{
			// handle reordering events
			if ( listChanges.isReordering() )
			{
				int[] sourceReorder = listChanges.getReorderMap();

				// remember what the mapping was before
				ArrayList<Integer> oldMap = sourceIndices;
				ArrayList<Integer> newMap = new ArrayList<Integer>( sourceIndices.size() );

				for ( int i = 0; i < oldMap.size(); ++i )
				{
					int oldSourceIndex = oldMap.get( i );
					int newSourceIndex = sourceReorder[oldSourceIndex];
					newMap.add( i, newSourceIndex );
				}

				// note that we don't change our order

				return;
			}

			// all of these changes to this list happen "atomically"
			updates.beginEvent();

			while ( listChanges.next() )
			{
				int sourceIndex = listChanges.getIndex();
				int changeType = listChanges.getType();

				if ( changeType == ListEvent.UPDATE )
				{
					int localIndex = sourceIndices.indexOf( sourceIndex );
					if ( localIndex >= 0 )
					{
						updates.elementUpdated( localIndex, listChanges.getOldValue(), listChanges.getNewValue() );
					}
				}
				else if ( changeType == ListEvent.DELETE )
				{
					int localIndex = sourceIndices.indexOf( sourceIndex );
					if ( localIndex >= 0 )
					{
						sourceIndices.remove( localIndex );
						updates.elementDeleted( localIndex, listChanges.getOldValue() );
					}
				}
			}

			// commit the changes and notify listeners
			updates.commitEvent();

		}
	};

	public enum PrepaidStatus
	{
		NotPaid,
		PaidWithSkateHire,
		PaidAdmissionOnly
	}

	AttendanceRegister( DataStore store, Session session )
	{
		super( store.members().getPublisher() );
		this.session = session;
		this.store = store;
		readWriteLock = store.members().getReadWriteLock();
		load();
		store.members().addListEventListener( sourceListener );
	}

	@Override
	public String toString()
	{
		return "Register for " + session.toString();
	}

	public Session getSession()
	{
		return session;
	}

	public DataStore getDataStore()
	{
		return store;
	}

	private void load()
	{
		try
		{
			Set<UUID> attendees = new HashSet<UUID>();
			ResultSet results = store.getQueryManager().lookupAttendeesForSession( session.getId() );
			while ( results.next() )
			{
				attendees.add( QueryManager.memberIdFromResult( results, "memberid" ) );
			}
			sourceIndices = new ArrayList<Integer>();
			for ( int i = 0; i < store.members().size(); ++i )
			{
				if ( attendees.contains( store.members().get( i ).getId() ) )
				{
					sourceIndices.add( i );
				}
			}
		}
		catch ( SQLException sqe )
		{
			logger.error( "Failed to load attendance register", sqe );
			throw new Error( sqe.getMessage(), sqe );
		}
	}

	@Override
	public int size()
	{
		return sourceIndices.size();
	}

	@Override
	public Member get( int i )
	{
		return store.members().get( sourceIndices.get( i ) );
	}

	public void dispose()
	{
		store.members().removeListEventListener( sourceListener );
	}

	@Override
	public void add( int index, Member value )
	{
		if ( index < 0 || index > size() )
		{
			throw new IndexOutOfBoundsException();
		}
		if ( value == null )
		{
			throw new NullPointerException();
		}
		int sourceIndex = store.members().indexOf( value );
		if ( sourceIndex < 0 )
		{
			throw new IllegalArgumentException( "member is not in store" );
		}

		// already in the register
		if ( sourceIndices.contains( sourceIndex ) )
		{
			return;
		}

		try
		{
			store.getQueryManager().addAttendeeForSession( session.getId(), value.getId() );
			updates.beginEvent();
			sourceIndices.add( index, sourceIndex );
			updates.elementInserted( index, value );
			updates.commitEvent();
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to add member {} to the register for session {} in the database", value, session );
			logger.error( ex.getMessage(), ex );
			throw new Error ( ex.getMessage(), ex );
		}
	}

	@Override
	public Member set( int index, Member value )
	{
		if ( index < 0 || index > size() )
		{
			throw new IndexOutOfBoundsException();
		}
		if ( value == null )
		{
			throw new NullPointerException();
		}
		int sourceIndex = store.members().indexOf( value );
		if ( sourceIndex < 0 )
		{
			throw new IllegalArgumentException( "member is not in store" );
		}

		int oldIndex = sourceIndices.indexOf( sourceIndex );
		if ( oldIndex == index )
		{
			return value; // no change
		}
		// already in the register
		if ( oldIndex >= 0 )
		{
			throw new DuplicateEntryException();
		}

		try
		{
			Member old = store.members().get( sourceIndices.get( index ) );

			store.getQueryManager().removeAttendeeForSession( session.getId(), old.getId() );
			store.getQueryManager().addAttendeeForSession( session.getId(), value.getId() );

			updates.beginEvent();
			sourceIndices.set( index, sourceIndex );
			updates.elementUpdated( index, old, value );
			updates.commitEvent();

			return old;
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to insert member {} into the register for session {} at position {} in the database",
				      new Object[] {value, session, index} );
			logger.error( ex.getMessage(), ex );
			throw new Error ( ex.getMessage(), ex );
		}
	}

	@Override
	public Member remove( int index )
	{
		if ( index < 0 || index >= size() )
		{
			throw new IndexOutOfBoundsException();
		}

		Member removed = store.members().get( sourceIndices.get( index ) );
		try
		{
			store.getQueryManager().removeAttendeeForSession( session.getId(), removed.getId() );

			updates.beginEvent();
			sourceIndices.remove( index );
			updates.elementDeleted( index, removed );
			updates.commitEvent();

			return removed;
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to remove member {} from the register for session {} at position {} in the database",
				      new Object[] {removed, session, index} );
			logger.error( ex.getMessage(), ex );
			throw new Error ( ex.getMessage(), ex );
		}
	}

	@Override
	public boolean addAll( int index, Collection<? extends Member> values )
	{
		updates.beginEvent( true );
		try
		{
			return super.addAll( index, values );
		}
		finally
		{
			updates.commitEvent();
		}
	}

	@Override
	public void clear()
	{
		updates.beginEvent( true );
		try
		{
			super.clear();
		}
		finally
		{
			updates.commitEvent();
		}
	}

	@Override
	public boolean removeAll( Collection<?> values )
	{
		updates.beginEvent( true );
		try
		{
			return super.removeAll( values );
		}
		finally
		{
			updates.commitEvent();
		}
	}

	@Override
	public boolean retainAll( Collection<?> values )
	{
		updates.beginEvent( true );
		try
		{
			return super.retainAll( values );
		}
		finally
		{
			updates.commitEvent();
		}
	}
}
