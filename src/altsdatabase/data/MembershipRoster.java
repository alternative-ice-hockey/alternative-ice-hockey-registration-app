/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import altsdatabase.data.Member.Status;
import altsdatabase.data.event.MemberEvent;
import altsdatabase.data.event.MemberListener;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.sql.*;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alex Merry
 */
public class MembershipRoster
{
	private static Logger logger = LoggerFactory.getLogger( MembershipRoster.class );
	private DataStore datastore;
	private MemberList members = new MemberList();
	private MemberListener memberListener = new MemberListener()
	{
		public void memberChanged( MemberEvent event )
		{
			updateMemberInDatabase( event.getMember() );
		}

		public boolean aboutToChangeMember( Member member,
											String newMembershipCode,
											String newName,
											String newContact,
                                            String newCollege,
											Status newStatus )
		{
			if ( newMembershipCode.length() > 0 && !member.getMembershipCode().equals( newMembershipCode ) )
			{
				for ( Member m : members )
				{
					if ( m.getMembershipCode().length() > 0 )
					{
						if ( m.getMembershipCode().equals( newMembershipCode ) )
						{
							return false;
						}
					}
				}
			}
			return true;
		}
	};

	private class MemberList extends GuardedEventList<Member>
	{
		@Override
		protected void checkElementAddition( Member element )
		{
			if ( element.getId() != null )
			{
				if ( this.contains( element ) )
					throw new DuplicateEntryException();
				else
					throw new IllegalArgumentException( "Member already in a database" );
			}
			if ( element.getMembershipCode().length() > 0 )
			{
				for ( Member m : this )
				{
					if ( m.getMembershipCode().length() > 0 )
					{
						if ( m.getMembershipCode().equals( element.getMembershipCode() ) )
						{
							throw new DuplicateMembershipCodeException();
						}
					}
				}
			}
		}

		@Override
		protected void checkElementRemoval( Object element )
		{
			if ( element == null || !(element instanceof Member) )
				return;

			Member m = (Member)element;
			try
			{
				int attCount = datastore.getQueryManager()
						.getMemberAttendanceCount( m.getId() );
				if ( attCount > 0 )
				{
					throw new IntegrityViolationException( String.format(
							"Member %1$s is on %2$d attendance registers",
							m.getName(), attCount ) );
				}
			}
			catch ( SQLException ex )
			{
				logger.warn( "Could not check attendance count for member " + m.toString(), ex );
			}
		}
	}

	private ListEventListener<Member> memberListListener = new ListEventListener<Member>()
	{
		public void listChanged( ListEvent<Member> le )
		{
			while ( le.next() )
			{
				if ( le.getType() == ListEvent.DELETE )
				{
					Member oldMember = le.getOldValue();
					if ( oldMember == ListEvent.UNKNOWN_VALUE )
					{
						logger.error( "Failed to get old member value!" );
					}
					else
					{
						oldMember.removeMemberListener( memberListener );
						deleteMemberFromDatabase( oldMember );
					}
				}
				else if ( le.getType() == ListEvent.INSERT )
				{
					Member member = le.getSourceList().get( le.getIndex() );
					insertMemberIntoDatabase( member );
					member.addMemberListener( memberListener );
				}
			}
		}
	};

	/**
	 * Creates a new membership roster for the given datastore
	 *
	 * Should only be called from DataStore
	 *
	 * @param conn The database connection
	 */
	MembershipRoster( DataStore datastore )
	{
		this.datastore = datastore;
        loadAll();
		members.addListEventListener(memberListListener);
	}

    void reload()
    {
        try {
            members.removeListEventListener(memberListListener);
        } catch (Exception ex) {
            logger.warn("Failed to remove listener from members list", ex);
        }
        for (Member member : members)
        {
            try {
                member.removeMemberListener(memberListener);
            } catch (Exception ex) {
                logger.warn("Failed to remove listener from member "
                        + member.getName(), ex);
            }
        }
        members.clear();
        loadAll();
		members.addListEventListener(memberListListener);
	}

    private void loadAll()
    {
		try
		{
			ResultSet results = datastore.getQueryManager().lookupAllMembers();
			while ( results.next() )
			{
				Member entry = new Member(
						QueryManager.memberIdFromResult( results, "memberid" ),
						results.getString( "membershipcode" ),
						results.getString( "name" ),
						results.getString( "contact" ),
						results.getString( "college" ),
						Member.Status.valueOf( results.getString( "status" ) ) );
				entry.addMemberListener( memberListener );
				members.uncheckedAdd( entry );
			}
			results.close();
		}
		catch ( SQLException sqe )
		{
			logger.error( "Failed to load membership roster", sqe );
			throw new Error( sqe.getMessage(), sqe );
		}
    }

	public EventList<Member> members()
	{
		return members;
	}

	@Override
	public String toString()
	{
		return "Roster for " + datastore.toString();
	}

	private void insertMemberIntoDatabase( Member member )
	{
		try
		{
			String mcode = member.getMembershipCode();
			if ( mcode.length() == 0 )
				mcode = null;
			UUID id = datastore.getQueryManager().insertMember(
					mcode,
					member.getName(),
					member.getContactDetails(),
					member.getCollege(),
                    member.getStatus() );
			member.setId( id );
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to add member {} to the database", member );
			logger.error( ex.getMessage(), ex );
		}
	}

	private void updateMemberInDatabase( Member member )
	{
		if ( member.getId() != null )
		{
			try
			{
				String mcode = member.getMembershipCode();
				if ( mcode.length() == 0 )
					mcode = null;
				datastore.getQueryManager().updateMember(
						member.getId(),
						mcode,
						member.getName(),
						member.getContactDetails(),
						member.getCollege(),
						member.getStatus() );
			}
			catch ( SQLException ex )
			{
				logger.error( "Failed to update member {} in the database", member );
				logger.error( ex.getMessage(), ex );
			}
		}
	}

	private void deleteMemberFromDatabase( Member member )
	{
		try
		{
			datastore.getQueryManager().deleteMember( member.getId() );
			member.setId( null );
		}
		catch ( SQLException ex )
		{
			logger.error( "Failed to delete member {} from the database", member );
			logger.error( ex.getMessage(), ex );
		}
	}
}

// vim:ts=4:sw=4:sts=4