/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEventPublisher;
import ca.odell.glazedlists.util.concurrent.ReadWriteLock;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author alex
 */
public abstract class GuardedEventList<E> implements EventList<E>
{
	private BasicEventList<E> internalList = new BasicEventList<E>();

	/**
	 * Check an element that is about to be inserted, and throw an
	 * exception if it should not be allowed
	 * @param E
	 */
	protected void checkElementAddition( E element ) {}

	/**
	 * Check an element that is about to be removed, and throw an
	 * exception if it should not be allowed
	 * @param E
	 */
	protected void checkElementRemoval( Object element ) {}

	private void checkCollectionAddition( Collection<? extends E> c )
	{
		for ( E s : c )
		{
			checkElementAddition( s );
		}
	}

	private void checkCollectionRemoval( Collection<?> c )
	{
		for ( Object s : c )
		{
			if ( this.contains( s ) )
				checkElementRemoval( s );
		}
	}

	public void addListEventListener( ListEventListener<? super E> ll )
	{
		internalList.addListEventListener( ll );
	}

	public void removeListEventListener( ListEventListener<? super E> ll )
	{
		internalList.removeListEventListener( ll );
	}

	public ReadWriteLock getReadWriteLock()
	{
		return internalList.getReadWriteLock();
	}

	public ListEventPublisher getPublisher()
	{
		return internalList.getPublisher();
	}

	public void dispose()
	{
		internalList.dispose();
	}

	public int size()
	{
		return internalList.size();
	}

	public boolean isEmpty()
	{
		return internalList.isEmpty();
	}

	public boolean contains( Object o )
	{
		return internalList.contains( o );
	}

	public Iterator<E> iterator()
	{
		return internalList.iterator();
	}

	public Object[] toArray()
	{
		return internalList.toArray();
	}

	public <T> T[] toArray( T[] a )
	{
		return internalList.toArray( a );
	}

	public boolean containsAll( Collection<?> c )
	{
		return internalList.containsAll( c );
	}

	public void clear()
	{
		internalList.clear();
	}

	public E get( int index )
	{
		return internalList.get( index );
	}

	public int indexOf( Object o )
	{
		return internalList.indexOf( o );
	}

	public int lastIndexOf( Object o )
	{
		return internalList.lastIndexOf( o );
	}

	public ListIterator<E> listIterator()
	{
		return internalList.listIterator();
	}

	public ListIterator<E> listIterator( int index )
	{
		return internalList.listIterator( index );
	}

	public List<E> subList( int fromIndex, int toIndex )
	{
		return internalList.subList( fromIndex, toIndex );
	}

	public boolean add( E e )
	{
		checkElementAddition( e );
		return internalList.add( e );
	}

	public E set( int index, E element )
	{
		if ( get(index) != element )
			checkElementAddition( element );
		return internalList.set( index, element );
	}

	public void add( int index, E element )
	{
		checkElementAddition( element );
		internalList.add( index, element );
	}

	public boolean addAll( Collection<? extends E> c )
	{
		checkCollectionAddition( c );
		return internalList.addAll( c );
	}

	public boolean addAll( int index, Collection<? extends E> c )
	{
		checkCollectionAddition( c );
		return internalList.addAll( index, c );
	}

	public boolean remove( Object o )
	{
		if ( this.contains( o ) )
			checkElementRemoval( o );
		return internalList.remove( o );
	}

	public boolean removeAll( Collection<?> c )
	{
		checkCollectionRemoval( c );
		return internalList.removeAll( c );
	}

	public E remove( int index )
	{
		checkElementRemoval( get(index) );
		return internalList.remove( index );
	}

	public boolean retainAll( Collection<?> c )
	{
		// FIXME: retainAll should check
		return internalList.retainAll( c );
	}

	protected boolean uncheckedAdd( E e )
	{
		return internalList.add( e );
	}

	protected E uncheckedSet( int index, E element )
	{
		return internalList.set( index, element );
	}

	protected void uncheckedAdd( int index, E element )
	{
		internalList.add( index, element );
	}

	protected boolean uncheckedAddAll( Collection<? extends E> c )
	{
		return internalList.addAll( c );
	}

	protected boolean uncheckedAddAll( int index, Collection<? extends E> c )
	{
		return internalList.addAll( index, c );
	}

	protected boolean uncheckedRemove( Object o )
	{
		return internalList.remove( o );
	}

	protected E uncheckedRemove( int index )
	{
		return internalList.remove( index );
	}

	protected boolean uncheckedRemoveAll( Collection<?> c )
	{
		return internalList.removeAll( c );
	}
}
