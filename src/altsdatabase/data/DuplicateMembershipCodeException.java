/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/**
 *
 * @author alex
 */
public class DuplicateMembershipCodeException extends IllegalArgumentException
{
	public DuplicateMembershipCodeException()
	{
		this( (Throwable) null );
	}

	public DuplicateMembershipCodeException( String message )
	{
		super( message );
	}

	public DuplicateMembershipCodeException( Throwable cause )
	{
		this( "A member with the same membership code already exists", cause );
	}

	public DuplicateMembershipCodeException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
