/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/**
 *
 * @author alex
 */
public class ProhibitedChangeException extends Exception
{
	public ProhibitedChangeException()
	{
	}

	public ProhibitedChangeException( String message )
	{
		super( message );
	}

	public ProhibitedChangeException( Throwable cause )
	{
		super( cause );
	}

	public ProhibitedChangeException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
