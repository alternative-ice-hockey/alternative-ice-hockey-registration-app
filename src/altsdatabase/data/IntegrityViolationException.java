/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/**
 * Indicates that continuing with the action would violate the integrity of
 * the data store
 *
 * @author alemer
 */
public class IntegrityViolationException extends RuntimeException
{
	public IntegrityViolationException()
	{
	}

	public IntegrityViolationException( String message )
	{
		super( message );
	}

	public IntegrityViolationException( Throwable cause )
	{
		super( cause );
	}

	public IntegrityViolationException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
