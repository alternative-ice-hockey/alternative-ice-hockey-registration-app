/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.io.IOException;

/**
 *
 * @author alex
 */
public class CorruptedFileException extends IOException
{
	public CorruptedFileException()
	{
		super( "The file is in an invalid state" );
	}

	public CorruptedFileException( Throwable cause )
	{
		super( "The file is in an invalid state", cause );
	}

	public CorruptedFileException( String message )
	{
		super( message );
	}

	public CorruptedFileException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
