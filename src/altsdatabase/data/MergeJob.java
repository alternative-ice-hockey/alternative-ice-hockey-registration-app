/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author alex
 */
public class MergeJob
{
    DataStore target;
    DataStore source;
    Date lastCommonPoint = null;
    boolean sourceHasChanges = true;
    boolean targetHasChanges = true;
    Statement sourceSt;
    Statement targetSt;
    Map<UUID, UUID> memberidMap = new HashMap<UUID, UUID>();
    Map<UUID, UUID> sessionidMap = new HashMap<UUID, UUID>();
    List<String> warnings = new LinkedList<String>();
    String error = null;

    MergeJob(DataStore target, DataStore source)
    {
        this.target = target;
        this.source = source;
    }

    public List<String> getWarnings()
    {
        return Collections.unmodifiableList(warnings);
    }

    public DataStore getMergeTarget()
    {
        return target;
    }

    public DataStore getMergeSource()
    {
        return source;
    }

    public String getError()
    {
        return error;
    }

    public boolean run()
    {
        try
        {
            start();
            return true;
        }
        catch (SQLException ex)
        {
            error = ex.getMessage();
            return false;
        }
    }

    void start() throws SQLException
    {
        sourceSt = source.getConnection().createStatement();
        targetSt = target.getConnection().createStatement();
        target.getConnection().setAutoCommit(false);

        try
        {
            sourceSt.executeUpdate("START TRANSACTION");

            findLastMergePoint();
            loadNewMembers();
            loadNewSessions();
            loadAttendance();

            sourceSt.executeUpdate("COMMIT");
            target.reloadFromDatabase();
        }
        catch (SQLException ex)
        {
            sourceSt.executeUpdate("ROLLBACK");
            throw ex;
        }
        finally
        {
            target.getConnection().setAutoCommit(true);
            sourceSt.close();
            targetSt.close();
        }
    }

    private void updateStringData(String field, ResultSet srcEntry, ResultSet tgtEntry)
            throws SQLException
    {
        String srcData = srcEntry.getString(field);
        String tgtData = tgtEntry.getString(field);
        if ((tgtData == null || tgtData.length() == 0)
                && srcData != null && srcData.length() > 0)
        {
            PreparedStatement updCollegePst = target.getConnection().prepareStatement(
                    "UPDATE Members SET " + field + " = ? WHERE memberid = ?");
            updCollegePst.setString(1, srcData);
            updCollegePst.setString(2, tgtEntry.getString("memberid"));
            updCollegePst.executeUpdate();
        }
    }

    private void logWarning(String msg)
    {
        warnings.add(msg);
    }

    private void findLastMergePoint() throws SQLException
    {
        PreparedStatement pst = target.getConnection().prepareStatement(
                "SELECT cptime FROM Checkpoints WHERE cpid = ?");

        boolean first = true;
        ResultSet results = sourceSt.executeQuery(
                "SELECT cpid, cptime FROM Checkpoints ORDER BY cptime DESC");
        try
        {
            while (results.next())
            {
                pst.setString(1, results.getString(1));
                if (pst.executeQuery().next())
                {
                    sourceHasChanges = !first;
                    lastCommonPoint = new Date(results.getTimestamp(2).getTime());

                    ResultSet r2 = targetSt.executeQuery(
                            "SELECT cpid FROM Checkpoints ORDER BY cptime DESC");
                    r2.next();
                    targetHasChanges = !r2.getString(1).equals(results.getString(1));
                    r2.close();
                    return;
                }
                first = false;
            }
        }
        finally
        {
            results.close();
            pst.close();
        }
    }

    private void loadNewMembers() throws SQLException
    {
        QueryManager tqm = target.getQueryManager();
        PreparedStatement srcPst;
        if (lastCommonPoint != null)
        {
            srcPst = source.getConnection().prepareStatement(
                    "SELECT * FROM MembersView WHERE lastmodified > ?");

            srcPst.setTimestamp(1, new Timestamp(lastCommonPoint.getTime()));
        }
        else
        {
            srcPst = source.getConnection().prepareStatement(
                    "SELECT * FROM MembersView");
        }
        ResultSet results = srcPst.executeQuery();
        while (results.next())
        {
            if (lastCommonPoint == null ||
                    results.getTimestamp("created").getTime() >= lastCommonPoint.getTime())
            {
                // should be completely new; try just inserting it
                // this is the common case
                try
                {
                    tqm.insertMemberFromResultSet(results, QueryManager.StatusColumn.Status);
                    continue;
                }
                catch (SQLException e)
                {
                }
            }

            Date srcLastUpdated = QueryManager.memberLastModified(results);
            UUID memberid = QueryManager.memberIdFromResult(results, "memberid");
            Date tgtLastUpdated = tqm.memberLastModified(memberid);
            try
            {
                if (tgtLastUpdated == null)
                {
                    tqm.insertMemberFromResultSet(
                            results,
                            QueryManager.StatusColumn.Status);
                }
                else if (tgtLastUpdated.before(srcLastUpdated))
                {
                    tqm.updateMemberFromResultSet(
                            results,
                            QueryManager.StatusColumn.Status);
                }
            }
            catch (SQLException e)
            {
                // maybe it was a duplicate membershipcode?
                String srcCode = results.getString("membershipcode");
                if (srcCode == null)
                {
                    throw e;
                }

                PreparedStatement tgtByCodePst = target.getConnection().prepareStatement(
                        "SELECT * FROM MembersView WHERE membershipcode = ?");
                tgtByCodePst.setString(1, srcCode);
                ResultSet dupMember = tgtByCodePst.executeQuery();
                if (!dupMember.next())
                {
                    dupMember.close();
                    throw e;
                }
                memberidMap.put(QueryManager.memberId(results),
                                QueryManager.memberId(dupMember));

                String srcName = results.getString("name");
                String tgtName = dupMember.getString("name");
                if (srcName.equalsIgnoreCase(tgtName))
                {
                    updateStringData("contact", results, dupMember);
                    updateStringData("college", results, dupMember);
                }
                else
                {
                    logWarning(String.format(
                            "\"%1$s\" from the merge source and \"%2$s\" from " +
                            "the merge target have the same membership code " +
                            "(\"%3$s\"), but may not be the same person; " +
                            "copying attendance data across anyway",
                            srcName, tgtName, srcCode));
                }
            }
        }
    }

    private void loadNewSessions() throws SQLException
    {
        QueryManager tqm = target.getQueryManager();
        PreparedStatement srcPst;
        if (lastCommonPoint != null)
        {
            srcPst = source.getConnection().prepareStatement(
                    "SELECT * FROM Sessions WHERE lastmodified > ?");

            srcPst.setTimestamp(1, new Timestamp(lastCommonPoint.getTime()));
        }
        else
        {
            srcPst = source.getConnection().prepareStatement(
                    "SELECT * FROM Sessions");
        }
        ResultSet results = srcPst.executeQuery();
        while (results.next())
        {
            ResultSet overlaps = tqm.lookupOverlappingSessions(
                    QueryManager.sessionStartTime(results),
                    QueryManager.sessionEndTime(results));
            if (!overlaps.next())
            {
                tqm.insertSessionFromResultSet(results);
                continue;
            }
            int overlapCount = 1;
            UUID srcId = QueryManager.sessionId(results);
            UUID tgtId = QueryManager.sessionId(overlaps);

            while (overlaps.next())
            {
                UUID nextTgtId = QueryManager.sessionId(overlaps);
                if (srcId.equals(nextTgtId))
                {
                    tgtId = nextTgtId;
                }
                ++overlapCount;
            }
            if (overlapCount > 1) {
                logWarning(String.format(
                        "The source session starting at %1$tF %1$tR overlaps " +
                        "with %2$d sessions in the target database",
                        QueryManager.sessionStartTime(results), overlaps));
            }
            if (srcId.equals(tgtId))
            {
                // yay, everyone is happy
                continue;
            }
            else
            {
                // don't mess around, just map across
                sessionidMap.put(srcId, tgtId);
            }
        }
    }
    
    private UUID targetMemberId( ResultSet results ) throws SQLException
    {
        return targetMemberId(QueryManager.memberId(results));
    }
    
    private UUID targetMemberId( UUID id )
    {
        if (memberidMap.containsKey(id))
            return memberidMap.get(id);
        else
            return id;
    }
    
    private UUID targetSessionId( ResultSet results ) throws SQLException
    {
        return targetSessionId(QueryManager.sessionId(results));
    }
    
    private UUID targetSessionId( UUID id )
    {
        if (sessionidMap.containsKey(id))
            return sessionidMap.get(id);
        else
            return id;
    }

    private void loadAttendance() throws SQLException
    {
        QueryManager tqm = target.getQueryManager();
        PreparedStatement srcPst;
        if (lastCommonPoint != null)
        {
            srcPst = source.getConnection().prepareStatement(
                    "SELECT * FROM Attendance WHERE added > ?");
            srcPst.setTimestamp(1, new Timestamp(lastCommonPoint.getTime()));
        }
        else
        {
            srcPst = source.getConnection().prepareStatement(
                    "SELECT * FROM Attendance");
        }
        ResultSet results = srcPst.executeQuery();
        while (results.next())
        {
            try
            {
                // common case: not in already
                tqm.insertAttendanceFromResultSet(results, memberidMap, sessionidMap);
            }
            catch (SQLException ex)
            {
                UUID memberId = targetMemberId(results);
                UUID sessionId = targetSessionId(results);
                if (!tqm.memberIsAttending(sessionId, memberId))
                {
                    throw ex;
                }
            }
        }
    }
}
