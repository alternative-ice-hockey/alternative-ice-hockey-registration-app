/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/**
 *
 * @author alex
 */
public class NoFileNameException extends Exception
{
	public NoFileNameException()
	{
		super( "No file name given" );
	}

	public NoFileNameException( String message )
	{
		super( message );
	}
}
