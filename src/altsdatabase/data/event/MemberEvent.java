/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data.event;

import altsdatabase.data.Member;
import java.util.EnumSet;
import java.util.EventObject;

/**
 *
 * @author alex
 */
public class MemberEvent extends EventObject
{
    private EnumSet<Changes> changeMask = EnumSet.noneOf( Changes.class );
    String oldMembershipCode;
    String oldName;
    String oldContact;
    String oldCollege;
    Member.Status oldStatus;

    public enum Changes
    {
        MembershipCode,
        Name,
        Contact,
        College,
        Status
    }

    public MemberEvent (Member member,
			String oldMembershipCode,
			String oldName,
			String oldContact,
			String oldCollege,
			Member.Status oldStatus)
    {
        super (member);

        this.oldMembershipCode = oldMembershipCode;
        this.oldName = oldName;
        this.oldContact = oldContact;
        this.oldCollege = oldCollege;
        this.oldStatus = oldStatus;

        if ( oldStatus != member.getStatus() )
            changeMask.add( Changes.Status );
        if ( !oldMembershipCode.equals (member.getMembershipCode()) )
            changeMask.add( Changes.MembershipCode );
        if ( !oldName.equals (member.getName()) )
            changeMask.add( Changes.Name );
        if ( !oldContact.equals (member.getContactDetails()) )
            changeMask.add( Changes.Contact );
        if ( !oldCollege.equals (member.getCollege()) )
            changeMask.add( Changes.College );
    }

    public Member getMember()
    {
	return (Member)getSource();
    }

    public EnumSet<Changes> getChangeMask()
    {
        return EnumSet.copyOf (changeMask);
    }

    public String getOldContactDetails()
    {
        return oldContact;
    }

    public String getOldCollege()
    {
        return oldCollege;
    }

    public String getOldName()
    {
        return oldName;
    }

    public String getOldMembershipCode()
    {
        return oldMembershipCode;
    }

    public Member.Status getOldStatus ()
    {
	return oldStatus;
    }
}
