/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data.event;

import altsdatabase.data.event.EventListener;
import java.util.EventObject;
import java.util.Iterator;
import java.util.Vector;

/**
 *
 * @author alex
 */
public class EventManager<EventClass extends EventObject>
{
    private Vector<EventListener<EventClass>> observers =
            new Vector<EventListener<EventClass>>();
    private boolean notifying = false;

    public void addListener( EventListener<EventClass> listener )
    {
        if ( notifying )
            throw new IllegalStateException();
        observers.add( listener );
    }

    public void removeListener( Object listener )
    {
        if ( notifying )
            throw new IllegalStateException();
        Iterator it = observers.iterator();
        while ( it.hasNext() )
        {
            if ( it.next() == listener )
                it.remove();
        }
    }

    public void notifyListeners( EventClass event )
    {
        try
        {
            notifying = true;
            for ( EventListener<EventClass> listener : observers )
            {
                try
                {
                    listener.eventOccurred( event );
                }
                catch ( RuntimeException ex )
                {
                    System.err.println( "The listener " + listener.toString() +
                                        " threw an exception " + ex.getMessage() );
                    System.err.println( "Stacktrace:" );
                    for ( StackTraceElement el : ex.getStackTrace() )
                    {
                        System.err.println( "    at " + el );
                    }
                }
            }
        }
        finally
        {
            notifying = false;
        }
    }
}
