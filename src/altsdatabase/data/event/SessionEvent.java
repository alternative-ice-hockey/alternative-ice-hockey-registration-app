/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data.event;

import altsdatabase.data.Session;
import java.util.Date;
import java.util.EnumSet;
import java.util.EventObject;

/**
 *
 * @author alex
 */
public class SessionEvent extends EventObject
{
    private EnumSet<Changes> changeMask = EnumSet.noneOf( Changes.class );
    Date oldStart;
    Date oldEnd;
    Session session;

    public enum Changes
    {
        StartTime,
        EndTime
    }

    public SessionEvent( Session session, Date oldStart, Date oldEnd )
    {
        super( session );

        this.oldStart = oldStart;
        this.oldEnd = oldEnd;

        if ( !oldStart.equals( session.getStartTime() ) )
            changeMask.add( Changes.StartTime );
        if ( !oldEnd.equals( session.getEndTime() ) )
            changeMask.add( Changes.EndTime );
    }

    public Session getSession()
    {
	return (Session)getSource ();
    }

    public EnumSet<Changes> getChangeMask()
    {
        return EnumSet.copyOf( changeMask );
    }

    public Date getOldStartTime()
    {
        return oldStart;
    }

    public Date getOldEndTime()
    {
        return oldEnd;
    }
}
