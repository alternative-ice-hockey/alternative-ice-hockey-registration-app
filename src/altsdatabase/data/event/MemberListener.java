/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data.event;

import altsdatabase.data.Member;
import java.util.EventListener;

/**
 *
 * @author alex
 */
public interface MemberListener extends EventListener {
    void memberChanged (MemberEvent event);
    boolean aboutToChangeMember (Member member,
				 String newMembershipCode,
				 String newName,
				 String newContact,
                 String newCollege,
				 Member.Status newStatus);
}
