/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data.event;

import java.util.EventObject;

/**
 *
 * @author alex
 */
public interface EventListener<EventClass extends EventObject>
        extends java.util.EventListener
{
    public void eventOccurred( EventClass event );
}
