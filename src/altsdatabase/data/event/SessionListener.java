/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data.event;

import altsdatabase.data.Session;
import java.util.Date;
import java.util.EventListener;

/**
 *
 * @author alex
 */
public interface SessionListener extends EventListener {
    void sessionChanged (SessionEvent event);
    boolean aboutToChangeSession (Session session, Date newStart, Date newEnd);
}
