/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/**
 *
 * @author alex
 */
public class NotFoundException extends Exception
{
	public NotFoundException()
	{
		super( "Not found" );
	}

	public NotFoundException( String message )
	{
		super( message );
	}

	public NotFoundException( Throwable cause )
	{
		super( "Not found", cause );
	}

	public NotFoundException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
