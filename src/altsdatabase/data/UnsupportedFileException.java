/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.io.IOException;

/**
 *
 * @author alex
 */
public class UnsupportedFileException extends IOException
{
	public UnsupportedFileException()
	{
		super( "The file format is not recognized" );
	}

	public UnsupportedFileException( Throwable cause )
	{
		super( "The file format is not recognized", cause );
	}

	public UnsupportedFileException( String message )
	{
		super( message );
	}

	public UnsupportedFileException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
