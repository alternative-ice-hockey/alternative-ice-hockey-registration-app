/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import altsdatabase.data.event.MemberEvent;
import altsdatabase.data.event.MemberListener;
import java.util.UUID;
import javax.swing.event.EventListenerList;

/**
 * Represents an entry in a membership roster
 */
public class Member
{
	public enum Status
	{
		OUStudent,
		OUStaff,
		OUAlumnus,
		Other;

		@Override
		public String toString()
		{
			switch ( this )
			{
				case OUStudent:
					return "Student (Oxford University)";
				case OUStaff:
					return "Staff (Oxford University)";
				case OUAlumnus:
					return "Alumnus (Oxford University)";
				case Other:
					return "Other";
			}
			return super.toString();
		}
	}
	private UUID id = null;
	private String membershipCode; // usually a bod card number
	private String name;
	private String contact;
	private String college;
	private Status status;
	private EventListenerList listenerList = new EventListenerList();

	// <editor-fold desc="Constructors">
	public Member( String membershipCode,
				   String name,
				   String contact,
				   String college,
				   Status status )
	{
		this( null, membershipCode, name, contact, college, status );
	}

	Member( UUID memberId,
			String membershipCode,
			String name,
			String contact,
			String college,
			Status status )
	{
		this.id = memberId;
		this.status = status;
		this.membershipCode = membershipCode == null ? "" : membershipCode;
		this.name = name == null ? "" : name;
		this.contact = contact == null ? "" : contact;
		this.college = college == null ? "" : college;
	}
	// </editor-fold>

	@Override
	public String toString()
	{
		return getId() + ": " + getName()
				+ "(" + status.name() + ")"
				+ (getMembershipCode().equals( "" ) ? "" : " (" + getMembershipCode() + ")");
	}

	public void addMemberListener( MemberListener listener )
	{
		listenerList.add( MemberListener.class, listener );
	}

	public void removeMemberListener( MemberListener listener )
	{
		listenerList.remove( MemberListener.class, listener );
	}

	protected void fireMemberChanged( String oldMembershipCode,
									  String oldName,
									  String oldContact,
									  String oldCollege,
									  Member.Status oldStatus )
	{
		MemberEvent event = null;
		Object[] listeners = listenerList.getListenerList();
		for ( int i = listeners.length - 2; i >= 0; i -= 2 )
		{
			if ( listeners[i] == MemberListener.class )
			{
				// Lazily create the event:
				if ( event == null )
				{
					event = new MemberEvent( this,
											 oldMembershipCode,
											 oldName,
											 oldContact,
											 oldCollege,
											 oldStatus );
				}
				((MemberListener) listeners[i + 1]).memberChanged( event );
			}
		}
	}

	protected boolean isMemberChangeAllowed( String newMembershipCode,
											 String newName,
											 String newContact,
											 String newCollege,
											 Member.Status newStatus )
	{
		Object[] listeners = listenerList.getListenerList();
		for ( int i = listeners.length - 2; i >= 0; i -= 2 )
		{
			if ( listeners[i] == MemberListener.class )
			{
				boolean allow = ((MemberListener) listeners[i + 1])
                                    .aboutToChangeMember(
                                        this,
                                        newMembershipCode,
                                        newName,
                                        newContact,
                                        newCollege,
                                        newStatus );
				if ( !allow )
				{
					return false;
				}
			}
		}
		return true;
	}

	UUID getId()
	{
		return id;
	}

	void setId( UUID id )
	{
		this.id = id;
	}

	public Status getStatus()
	{
		return status;
	}

	public String getName()
	{
		return name;
	}

	public String getMembershipCode()
	{
		return membershipCode;
	}

	public String getContactDetails()
	{
		return contact;
	}

	public String getCollege()
	{
		return college;
	}

	// like String.equals, except null is equivalent to ""
	private boolean relaxedEquals( String s1, String s2 )
	{
		if ( s1 == null )
		{
			s1 = "";
		}
		if ( s2 == null )
		{
			s2 = "";
		}
		return s1.equals( s2 );
	}

	public void setFields( String membershipCode,
						   String name,
						   String contact,
						   String college,
						   Status status )
			throws ProhibitedChangeException
	{
		if ( status == this.status
				&& relaxedEquals( membershipCode, this.membershipCode )
				&& relaxedEquals( name, this.name )
				&& relaxedEquals( contact, this.contact )
				&& relaxedEquals( college, this.college ) )
		{
			return;
		}

		if ( status == null )
		{
			throw new NullPointerException();
		}
		if ( name == null || name.isEmpty() )
		{
			throw new IllegalArgumentException();
		}

		if ( membershipCode == null )
		{
			membershipCode = "";
		}
		if ( contact == null )
		{
			contact = "";
		}
		if ( college == null )
		{
			college = "";
		}

		if ( !isMemberChangeAllowed( membershipCode, name, contact, college, status ) )
		{
			throw new ProhibitedChangeException();
		}

		Status oldStatus = this.status;
		String oldMembershipCode = this.membershipCode;
		String oldName = this.name;
		String oldContact = this.contact;
		String oldCollege = this.college;

		this.status = status;
		this.membershipCode = membershipCode;
		this.name = name;
		this.contact = contact;
		this.college = college;

		fireMemberChanged( oldMembershipCode, oldName, oldContact, oldCollege, oldStatus );
	}

	public void setMembershipCode( String membershipCode )
			throws ProhibitedChangeException
	{
		setFields( membershipCode, name, contact, college, status );
	}

	public void setName( String name )
			throws ProhibitedChangeException
	{
		setFields( membershipCode, name, contact, college, status );
	}

	public void setContactDetails( String contact )
			throws ProhibitedChangeException
	{
		setFields( membershipCode, name, contact, college, status );
	}

	public void setCollege( String college )
			throws ProhibitedChangeException
	{
		setFields( membershipCode, name, contact, college, status );
	}

	public void setStatus( Status status )
			throws ProhibitedChangeException
	{
		setFields( membershipCode, name, contact, college, status );
	}
}
