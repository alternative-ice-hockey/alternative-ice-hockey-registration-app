/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/**
 *
 * @author alex
 */
public class NoSuchSessionException extends NotFoundException
{
	int sessionId;

	public NoSuchSessionException( int sessionId )
	{
		super( "There is no session with id " + sessionId );
		this.sessionId = sessionId;
	}

	public NoSuchSessionException()
	{
		super( "The requested session does not exist" );
	}

	public NoSuchSessionException( String message )
	{
		super( message );
	}

	public int getSessionId()
	{
		return sessionId;
	}
}
