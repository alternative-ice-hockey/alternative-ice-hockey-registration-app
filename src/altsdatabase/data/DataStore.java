/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import org.hsqldb.jdbc.JDBCDataSource;

/**
 *
 * @author alex
 */
public class DataStore
{
    private static final Logger logger = Logger.getLogger( "alts.data.store" );

	private static final String DATABASE_NAME = "store";
	private static final String MARKER_FILE_NAME = "alts-data.mrk";
	private static final String DATADIR_PREFIX = "datastore";
	static final int DB_VERSION = 6;
	private Connection conn;
	private QueryManager qm;
	private File dataDir;
	private File lastSavedFileName;
	private File markerFile;
	private MembershipRoster roster;
	private SessionManager sessions;
	private Map<Session, AttendanceRegister> registers =
            new HashMap<Session, AttendanceRegister>();

	public static class StaleSession
	{
		private File dataDir;
		private File lastSavedFileName;

		StaleSession( File dataDir, File markerFile )
				throws IOException
		{
			this.dataDir = dataDir;
			if ( markerFile.exists() )
			{
				RandomAccessFile file = new RandomAccessFile( markerFile, "rw" );
				try
				{
					if ( file.length() > 0 )
					{
						lastSavedFileName = new File( file.readUTF() );
					}
				}
				finally
				{
					file.close();
				}
			}
		}

        public File getLastSavedFileName()
        {
            return lastSavedFileName;
        }

        public File getDataDir()
        {
            return dataDir;
        }

        public Date getLastUsedDate()
        {
            return new Date(dataDir.lastModified());
        }

		@Override
		public String toString()
		{
            return dataDir.getName();
		}

		public DataStore open()
				throws IOException
		{
			return new DataStore( lastSavedFileName, dataDir );
		}
	}

	// <editor-fold defaultstate="collapsed" desc="Constructors">
	/**
	 * Creates a new, blank data store
	 */
	public DataStore()
	{
		try
		{
			this.dataDir = createTempDirectory();

			openConnection();
            runSqlFile("create_db.sql");
			Statement st = conn.createStatement();
			try
			{
				populateMemberStatuses( st );
			}
			finally
			{
				st.close();
			}
		}
		catch ( IOException ex )
		{
			throw new Error( ex.getMessage(), ex );
		}
		catch ( SQLException sqe )
		{
			throw new Error( "Failed to create database: " + sqe.getMessage(), sqe );
		}
	}

	/**
	 * Split an SQL script into separate statements delimited with the provided
	 * delimiter character. Each individual statement will be added to the
	 * provided <code>List</code>.
	 * 
	 * @param script the SQL script
	 * @param delim character delimiting each statement - typically a ';'
	 * character
	 * @param statements the List that will contain the individual statements
	 */
	public static List<String> splitSqlScript(BufferedReader reader) throws IOException {
        List<String> statements = new LinkedList<String>();
        StringBuilder statement = new StringBuilder();
        boolean inLiteral = false;
        String line = reader.readLine();
        while (line != null) {
            char[] content = line.toCharArray();
            for (int i = 0; i < content.length; i++) {
                if (content[i] == '\'') {
                    inLiteral = !inLiteral;
                }
                if (content[i] == ';' && !inLiteral) {
                    String st = statement.toString().trim();
                    if (st.length() > 0) {
                        statements.add(st);
                    }
                    statement.setLength( 0 );
                } else {
                    statement.append(content[i]);
                }
            }
            line = reader.readLine();
        }
        String st = statement.toString().trim();
		if (st.length() > 0) {
			statements.add(st.toString());
		}
        return statements;
	}

    private void runSqlFile(String filename) throws SQLException, IOException
    {
        logger.log( Level.FINE, "Executing SQL script from resources/{0}", filename);

        InputStream stream = getClass().getResourceAsStream( "resources/" + filename );
        BufferedReader reader = new BufferedReader( new InputStreamReader( stream ) );
        long startTime = System.currentTimeMillis();
        List<String> statements = splitSqlScript( reader );

        Statement st = conn.createStatement();
        for ( String statement : statements )
        {
            try
            {
                int rowsAffected = st.executeUpdate( statement );
                if ( logger.isLoggable( Level.FINEST ) )
                {
                    logger.log(Level.FINEST, "{0} rows affected by SQL: {1}", 
                                             new Object[] { rowsAffected, statement });
                }
            }
            catch (SQLException ex)
            {
                logger.log(Level.WARNING, "Statement {0} failed with error {1}",
                                          new Object[] { statement, ex.getMessage() });
                throw ex;
            }
        }
        long elapsedTime = System.currentTimeMillis() - startTime;
        if ( logger.isLoggable( Level.FINER ) )
        {
            logger.log(Level.FINER, "Done executing SQL scriptBuilder from resources/{0} in {1} ms.", new Object[]{filename, elapsedTime});
        }
    }

	/**
	 * Opens an existing datastore
	 */
	private DataStore( File origFile, File dataDir )
			throws IOException
	{
		this.dataDir = dataDir.getCanonicalFile();
		lastSavedFileName = origFile;

		try
		{
			openConnection();
			Statement st = conn.createStatement();
			ResultSet result = st.executeQuery( "SELECT int_value FROM Admin WHERE key = 'db_version'" );
			if ( !result.next() )
			{
				throw new CorruptedFileException( "The file " + lastSavedFileName + " has no db_version entry in the Admin table" );
			}
			int dbversion = result.getInt( 1 );
			if ( dbversion < 0 )
			{
				throw new CorruptedFileException( "The file " + lastSavedFileName + " has an invalid db_version entry in the Admin table" );
			}
			if ( dbversion > DB_VERSION )
			{
				throw new TooRecentFileException( "The file " + lastSavedFileName + " is too recent (version " + dbversion + ") for this version of the application to read" );
			}
			if ( dbversion == 0 )
			{
                runSqlFile("v0_to_v2.sql");
				dbversion = 2;
			}
			if ( dbversion == 1 )
			{
                runSqlFile("v1_to_v2.sql");
				dbversion = 2;
			}
			if ( dbversion == 2 )
			{
				st.executeUpdate( "DROP TABLE Terms" );
				st.executeUpdate( "CREATE TABLE MemberStatuses ( "
                                + "statusid integer GENERATED ALWAYS AS IDENTITY NOT NULL, "
                                + "status varchar(20), "
                                + "CONSTRAINT PK_MemberStatuses PRIMARY KEY ( statusid ), "
                                + "CONSTRAINT UQ_MemberStatuses_status UNIQUE ( status )"
                        );
				populateMemberStatuses( st );
				ResultSet results = st.executeQuery(
						"SELECT statusid FROM MemberStatuses "
						+ "WHERE status = '" + Member.Status.OUStudent.name() + "'" );
				results.next();
				int studentStatusId = results.getInt( 1 );
				results.close();
				st.executeUpdate( "ALTER TABLE Members ALTER COLUMN email RENAME TO contact" );
				st.executeUpdate( "ALTER TABLE Members ADD COLUMN statusid integer DEFAULT " + studentStatusId + " NOT NULL" );
				st.executeUpdate( "ALTER TABLE Members ALTER COLUMN statusid DROP DEFAULT" );
				st.executeUpdate( "ALTER TABLE Members ADD FOREIGN KEY ( statusid ) REFERENCES MemberStatuses ( statusid )" );
				st.executeUpdate( "CREATE VIEW MembersView AS "
                                + "SELECT m.*, s.status "
                                + "FROM Members AS m, "
                                + "MemberStatuses as s "
                                + "WHERE m.statusid = s.statusid"
                        );
				st.executeUpdate( "ALTER TABLE Sessions DROP COLUMN termid" );
				st.executeUpdate( "UPDATE Admin SET int_value = 4 WHERE key = 'db_version'" );
				dbversion = 4;
			}
			if ( dbversion == 3 )
			{
                runSqlFile("v3_to_v4.sql");
                runSqlFile("v4_to_v5.sql");
				dbversion = 5;
			}
			if ( dbversion == 4 )
			{
                runSqlFile("v4_remove_constraints.sql");
                runSqlFile("v4_to_v5.sql");
				dbversion = 5;
			}
			if ( dbversion == 5 )
			{
                runSqlFile("v5_to_v6.sql");
				dbversion = 6;
			}
			assert (DB_VERSION == dbversion);
			st.close();
		}
		catch ( SQLException sqe )
		{
			throw new UnsupportedFileException( "The file " + lastSavedFileName + " is not in the expected format", sqe );
		}
	}

    public static String randomUUID() {
        return java.util.UUID.randomUUID().toString();
    }

	private void populateMemberStatuses( Statement st ) throws SQLException
	{
		for ( Member.Status status : Member.Status.values() )
		{
			st.executeUpdate( "INSERT INTO MemberStatuses (status) "
					+ "VALUES ( '" + status.name() + "' )" );
		}
	}

	private void openConnection()
			throws SQLException, IOException
	{
		if ( !dataDir.isDirectory() )
		{
			throw new IOException( "The data directory is not a directory" );
		}

		markerFile = new File( dataDir, MARKER_FILE_NAME );
		setLastSavedFileName( lastSavedFileName );

		JDBCDataSource dataSource = new JDBCDataSource();

		dataSource.setDatabase( "jdbc:hsqldb:file:" + dataDir + File.separator + DATABASE_NAME );

		conn = dataSource.getConnection();
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="File access">
	// for the unit tests
	File getDataDir()
	{
		return dataDir;
	}

	/**
	 * Opens a saved data store
	 *
	 * @param filename
	 * @return
	 */
	public static DataStore open( File dbFile )
			throws IOException
	{
		File dataDir = createTempDirectory();
		try
		{
			unzipFiles( dbFile, dataDir );
		}
		catch ( ZipException zex )
		{
			throw new UnsupportedFileException( "The file does not appear to be in the correct format", zex );
		}
		return new DataStore( dbFile, dataDir );
	}

	public static StaleSession[] findStaleSessions()
			throws IOException
	{
		ArrayList<StaleSession> sessions = new ArrayList<StaleSession>();
		File tempDir = new File( System.getProperty( "java.io.tmpdir" ) );
		if ( tempDir.isDirectory() )
		{
			FilenameFilter filter = new FilenameFilter()
			{
				public boolean accept( File dir, String name )
				{
					return name.startsWith( DATADIR_PREFIX );
				}
			};
			for ( File dataDir : tempDir.listFiles( filter ) )
			{
				File markerFile = new File( dataDir, MARKER_FILE_NAME );
				if ( markerFile.isFile() )
				{
					sessions.add( new StaleSession( dataDir, markerFile ) );
				}
			}
		}
		return sessions.toArray( new StaleSession[sessions.size()] );
	}

	private void setLastSavedFileName( File name )
	{
		lastSavedFileName = name;
		if ( markerFile != null )
		{
			try
			{
				markerFile.createNewFile();
				RandomAccessFile file = new RandomAccessFile( markerFile, "rw" );
				try
				{
					file.setLength( 0 );
					if ( name != null )
					{
						file.writeUTF( name.getAbsolutePath() );
					}
				}
				finally
				{
					file.close();
				}
			}
			catch ( IOException ex )
			{
				System.err.println( ex.getMessage() );
			}
		}
	}

	public File getSaveFileName()
	{
		return lastSavedFileName;
	}

	public void save( File file )
			throws IOException
	{
		try
		{
			if ( conn == null )
			{
				throw new IllegalStateException( "This DataStore has been closed" );
			}
			Statement st = conn.createStatement();
			st.execute( "CALL insert_checkpoint()" );
			st.execute( "CHECKPOINT DEFRAG" );
			st.close();
			// FIXME: perhaps we should copy these somewhere, re-open them and shutdown that database?
		}
		catch ( SQLException sqe )
		{
			throw new IOException( "Failed to checkpoint the database", sqe );
		}

		File[] filesToZip = dataDir.listFiles( new FilenameFilter()
		{
			public boolean accept( File dir, String name )
			{
				if ( name.startsWith( DATABASE_NAME + '.' ) )
				{
					String tail = name.substring( DATABASE_NAME.length() + 1 );
					return tail.equals( "script" )
							|| tail.equals( "backup" )
							|| tail.equals( "properties" )
							|| tail.equals( "log" );
				}
				return false;
			}
		} );

		zipFiles( filesToZip, file );

		setLastSavedFileName( file );
	}

	public void save()
			throws NoFileNameException, IOException
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		if ( lastSavedFileName == null )
		{
			throw new NoFileNameException();
		}
		save( lastSavedFileName );
	}

	public void saveAndClose( File file )
			throws IOException
	{
		// check we can write to the file before we do anything drastic
		// like shut down the database
		if ( file.exists() )
		{
			if ( !file.canWrite() )
			{
				throw new IOException( "Cannot write to " + file + ": permission denied" );
			}
		}
		else if ( !file.getParentFile().canWrite() )
		{
			if ( !file.canWrite() )
			{
				throw new IOException( "Cannot create " + file + ": permission denied" );
			}
		}

		try
		{
			if ( conn == null )
			{
				throw new IllegalStateException( "This DataStore has been closed" );
			}
            Statement st = conn.createStatement();
			st.execute( "CALL insert_checkpoint()" );
			st.execute( "SHUTDOWN COMPACT" );
            st.close();
			conn.close();
			conn = null;

			File[] filesToZip = dataDir.listFiles( new FilenameFilter()
			{
				public boolean accept( File dir, String name )
				{
					if ( name.equals( DATABASE_NAME + ".tmp" ) )
					{
						return false;
					}
					if ( name.startsWith( DATABASE_NAME ) )
					{
						return true;
					}
					return false;
				}
			} );

			zipFiles( filesToZip, file );

			for ( File dataFile : dataDir.listFiles() )
			{
				dataFile.delete();
			}
			dataDir.delete();
		}
		catch ( SQLException sqe )
		{
			// try to recover from a bad write
			if ( conn == null )
			{
				try
				{
					openConnection();
				}
				catch ( SQLException s )
				{
				}
			}

			throw new IOException( sqe );
		}
		catch ( IOException ex )
		{
			// try to recover from a bad write
			if ( conn == null )
			{
				try
				{
					openConnection();
				}
				catch ( SQLException s )
				{
				}
			}

			throw ex;
		}
	}

	public void saveAndClose()
			throws NoFileNameException, IOException
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		if ( lastSavedFileName == null )
		{
			throw new NoFileNameException();
		}
		saveAndClose( lastSavedFileName );
	}

	public boolean hasChanges()
	{
		return true;
	}

	public void discardAndClose()
	{
		try
		{
			if ( conn == null )
			{
				throw new IllegalStateException( "This DataStore has been closed" );
			}
			conn.close();
			for ( File dataFile : dataDir.listFiles() )
			{
				dataFile.delete();
			}
			dataDir.delete();
			conn = null;
		}
		catch ( SQLException ex )
		{
			// Meh
		}
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="File helpers">
	private static void zipFiles( File[] filesToZip, File destFile )
			throws IOException
	{
		File tempFile = null;
		if ( destFile.exists() )
		{
			if ( !destFile.canWrite() )
			{
				throw new IOException( "Cannot write to " + destFile + ": permission denied" );
			}
			tempFile = new File( destFile.getAbsolutePath() + ".backup." + Long.toString( System.nanoTime() ) );
			if ( tempFile.exists() )
			{
				throw new IOException( "Could not create backup file (target file name exists!)" );
			}
			if ( !destFile.renameTo( tempFile ) )
			{
				throw new IOException( "Could not rename original file" );
			}
		}

		try
		{
			ZipOutputStream out = new ZipOutputStream( new FileOutputStream( destFile ) );

			out.setLevel( Deflater.DEFAULT_COMPRESSION );

			byte[] buffer = new byte[18024];

			for ( int i = 0; i < filesToZip.length; i++ )
			{
				FileInputStream in = new FileInputStream( filesToZip[i] );

				try
				{
					out.putNextEntry( new ZipEntry( filesToZip[i].getName() ) );

					int len;
					while ( (len = in.read( buffer )) > 0 )
					{
						out.write( buffer, 0, len );
					}

					// Close the current entry
					out.closeEntry();
				}
				finally
				{
					in.close();
				}
			}
			out.close();

			if ( tempFile != null )
			{
				tempFile.delete();
			}
		}
		catch ( Throwable ex )
		{
			destFile.delete();
			if ( tempFile != null )
			{
				tempFile.renameTo( destFile );
			}
			// stupid java...
			if ( ex instanceof RuntimeException )
			{
				throw (RuntimeException) ex;
			}
			else if ( ex instanceof Error )
			{
				throw (Error) ex;
			}
			else if ( ex instanceof IOException )
			{
				throw (IOException) ex;
			}
			throw new RuntimeException( ex );
		}
	}

	private static void unzipFiles( File source, File destDir )
			throws IOException
	{
		ZipFile zipFile = new ZipFile( source, ZipFile.OPEN_READ );

		try
		{
			Enumeration<? extends ZipEntry> zipFileEntries = zipFile.entries();

			while ( zipFileEntries.hasMoreElements() )
			{
				ZipEntry entry = zipFileEntries.nextElement();

				String currentEntry = entry.getName();

				File destFile = new File( destDir, currentEntry );

				File destinationParent = destFile.getParentFile();

				destinationParent.mkdirs();

				if ( !entry.isDirectory() )
				{
					BufferedInputStream is =
							new BufferedInputStream( zipFile.getInputStream( entry ) );
					byte data[] = new byte[18024];

					FileOutputStream fos = new FileOutputStream( destFile );
					BufferedOutputStream dest =
							new BufferedOutputStream( fos );

					int bytesRead;
					while ( (bytesRead = is.read( data, 0, data.length )) != -1 )
					{
						dest.write( data, 0, bytesRead );
					}
					dest.flush();
					dest.close();
					is.close();
				}
			}
		}
		finally
		{
			zipFile.close();
		}
	}

	private static File createTempDirectory()
			throws IOException
	{
		final File temp;

		temp = File.createTempFile( DATADIR_PREFIX, Long.toString( System.nanoTime() ) );

		if ( !temp.delete() )
		{
			throw new IOException( "Could not create temporary directory: " + temp.getAbsolutePath() );
		}

		if ( !temp.mkdir() )
		{
			throw new IOException( "Could not create temporary directory: " + temp.getAbsolutePath() );
		}

		return (temp);
	}
	// </editor-fold>

	@Override
	public String toString()
	{
		if ( lastSavedFileName != null )
		{
			return lastSavedFileName.toString();
		}
		else
		{
			return "Unsaved<" + dataDir.toString() + ">";
		}
	}

    public MergeJob createMergeJob(DataStore other)
    {
        return new MergeJob(this, other);
    }

    void reloadFromDatabase()
    {
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
        if (roster != null)
        {
            roster.reload();
        }
        if (sessions != null)
        {
            registers.clear();
            sessions.reload();
        }
    }

	public EventList<Member> members()
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		if ( roster == null )
		{
			roster = new MembershipRoster( this );
		}
		return roster.members();
	}

	public int memberCountByAttendance(Date startDate, Date endDate, int minSessionCount)
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		try
		{
			return getQueryManager().getMemberCountWithAttendance( startDate, endDate, minSessionCount );
		}
		catch ( SQLException ex )
		{
			throw new Error( ex );
		}
	}

	public EventList<Session> sessions()
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		if ( sessions == null )
		{
			sessions = new SessionManager( this );
			sessions.allSessions().addListEventListener( registerCleaner );
		}
		return sessions.allSessions();
	}

	private ListEventListener<Session> registerCleaner = new ListEventListener<Session>()
	{
		public void listChanged( ListEvent<Session> le )
		{
			while ( le.next() )
			{
				if ( le.getType() == ListEvent.DELETE )
				{
					Session oldSession = le.getOldValue();
					if ( oldSession != ListEvent.UNKNOWN_VALUE )
					{
						registers.remove( oldSession );
					}
				}
			}
		}
	};

	public AttendanceRegister getRegister( Session session )
	{
		if ( session.getId() == null )
		{
			throw new IllegalStateException( "Already removed from collection" );
		}
		if ( !registers.containsKey( session ) )
		{
			registers.put( session, new AttendanceRegister( this, session ) );
		}
		return registers.get( session );
	}
	private static final java.util.regex.Pattern bodCardRegex = java.util.regex.Pattern.compile( "[0-9]{7}." );

	public String cleanScannedCode( String rawCode )
	{
		/*
		 * Bod card barcodes have a check digit at the end.  Here we just look for
		 * anything that looks like a scanned barcode (7 digits and one more
		 * character), and remove the final character.
		 *
		 * The check digit is based on the sum of the digits of the bod card number.
		 * For totals between 10 and 35, a lower case letter is used.  Symbols are
		 * used after that:
		 * 36 -
		 * 37 .
		 * 39 $
		 * 40 /
		 * 41 +
		 * 42 %
		 * Note that 38 is unknown.  43-47 are 0-4, so presumably 43-52 are 0-9.
		 * The whole thing is possibly mod 43 (and 0-9 map to 0-9).
		 */
		String code = rawCode;
		if ( bodCardRegex.matcher( code ).matches() )
		{
			try
			{
				final int realCodeLength = 7;
				char checkDigit = code.charAt( realCodeLength );
				code = code.substring( 0, realCodeLength );
				int sum = 0;
				for ( int i = 0; i < realCodeLength; ++i )
				{
					char c = code.charAt( i );
					sum += Integer.parseInt( Character.toString( c ) );
				}
				insertCardCheckDigit( sum, checkDigit );
			}
			catch ( NumberFormatException ex )
			{
				System.err.print( "Unexpected match by regex: " + rawCode );
			}
		}
		return code;
	}

	// <editor-fold desc="Database helpers">
	Connection getConnection()
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		return conn;
	}

	QueryManager getQueryManager()
	{
		if ( conn == null )
		{
			throw new IllegalStateException( "This DataStore has been closed" );
		}
		if ( qm == null )
		{
			qm = new QueryManager( conn );
		}
		return qm;
	}
	// </editor-fold>
	private PreparedStatement pst_insertCardCheckDigit;
	private PreparedStatement pst_incrementCardCheckDigit;
	private PreparedStatement pst_lookupCardCheckDigit;

	private void insertCardCheckDigit( int checksum, char checkdigit )
	{
		try
		{
			String checkStr = Character.toString( checkdigit );
			if ( pst_lookupCardCheckDigit == null )
			{
				pst_lookupCardCheckDigit = getConnection().prepareStatement(
						"SELECT * FROM BodCardLog "
						+ "WHERE checksum = ? AND checkdigit = ?" );
			}
			pst_lookupCardCheckDigit.setInt( 1, checksum );
			pst_lookupCardCheckDigit.setString( 2, checkStr );
			ResultSet results = pst_lookupCardCheckDigit.executeQuery();
			if ( results.next() )
			{
				if ( pst_incrementCardCheckDigit == null )
				{
					pst_incrementCardCheckDigit = getConnection().prepareStatement(
							"UPDATE BodCardLog SET seencount = seencount + 1 "
							+ "WHERE checksum = ? AND checkdigit = ?" );
				}
				pst_incrementCardCheckDigit.setInt( 1, checksum );
				pst_incrementCardCheckDigit.setString( 2, checkStr );
				pst_incrementCardCheckDigit.executeUpdate();
			}
			else
			{
				if ( pst_insertCardCheckDigit == null )
				{
					pst_insertCardCheckDigit = getConnection().prepareStatement(
							"INSERT INTO BodCardLog (checksum, checkdigit) VALUES (?, ?)" );
				}
				pst_insertCardCheckDigit.setInt( 1, checksum );
				pst_insertCardCheckDigit.setString( 2, checkStr );
				pst_insertCardCheckDigit.executeUpdate();
			}
		}
		catch ( SQLException sqe )
		{
			System.err.print( sqe.getMessage() );
		}
	}
}
