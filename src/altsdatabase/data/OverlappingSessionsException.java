/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

/** Indicates that an existing session overlaps the dates given
 *
 * @author alex
 */
public class OverlappingSessionsException extends RuntimeException
{
	public OverlappingSessionsException()
	{
		this( (Throwable) null );
	}

	public OverlappingSessionsException( String message )
	{
		super( message );
	}

	public OverlappingSessionsException( Throwable cause )
	{
		this( "An existing session overlaps this one", cause );
	}

	public OverlappingSessionsException( String message, Throwable cause )
	{
		super( message, cause );
	}
}
