/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase;

import altsdatabase.data.DataExporter;
import altsdatabase.data.DataStore;
import altsdatabase.gui.MainWindow;
import java.io.File;
import javax.swing.SwingUtilities;

/**
 *
 * @author alex
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        File openFile = null;
        if ( args.length > 0 )
        {
            for ( int i = 0; i < args.length; ++i )
            {
                if ( args[i].equals( "--help" ) || args[i].equals( "-h" ) )
                {
                    System.out.println( "Use option --dump-check-digits [csvfile] [altsfile] to dump check digits" );
                    return;
                }
                else if ( args[i].equals( "--dump-check-digits" ) )
                {
                    if ( args.length > i + 2 )
                    {
                        File csvFile = new File( args[i+1] );
                        File altsFile = new File( args[i+2] );
                        DataStore store = DataStore.open( altsFile );
                        try
                        {
                            DataExporter exporter = new DataExporter( store );
                            exporter.exportBodCardCheckDigits( csvFile );
                        }
                        finally
                        {
                            store.discardAndClose();
                        }
                        return;
                    }
                    else
                    {
                        System.err.println( "--dump-check-digits requires two arguments" );
                        System.exit( 1 );
                    }
                }
                else
                {
                    openFile = new File( args[i] );
                    if ( !openFile.isFile() && args[i].startsWith( "-" ) )
                    {
                        System.err.println( "Unrecognised option " + args[i] );
                        System.exit( 1 );
                    }
                }
            }
        }
        final File openAt = openFile;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainWindow window = new MainWindow( openAt );
                window.setVisible(true);
            }
        });
    }

}
