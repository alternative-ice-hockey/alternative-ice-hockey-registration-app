/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.sql.ResultSet;
import ca.odell.glazedlists.EventList;
import java.sql.PreparedStatement;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alex
 */
public class SessionManagerTest
{
	static final int HALF_AN_HOUR = 1000 * 60 * 30;

	public SessionManagerTest()
	{
	}

	@Test
	public void testEmptyStore() throws Exception
	{
		System.out.println( "empty store" );
		DataStore store = new DataStore();
		EventList<Session> sessions = store.sessions();

		assertTrue (sessions.isEmpty());

		store.discardAndClose();
	}

	@Test
	public void testLoad() throws Exception
	{
		System.out.println( "load existing sessions" );
		DataStore store = new DataStore();
        PreparedStatement pst = store.getConnection().prepareStatement(
                "SELECT * FROM Sessions WHERE sessionid = ?");
		DataGenerators.generateSessionsForYear( 2000, store );
		EventList<Session> sessions = store.sessions();

		assertEquals( DataGenerators.SESSION_COUNT, store.sessions().size() );

		Session session0 = sessions.get( 0 );
		UUID session0Id = session0.getId();
		assertNotNull (session0Id);
        pst.setString( 1, session0Id.toString() );
		ResultSet results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( session0.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( session0.getEndTime(), results.getTimestamp( "endtime" ) );
		results.close();

		Session session1 = sessions.get( 1 );
		UUID session1Id = session1.getId();
		assertNotNull (session1Id);
		assertFalse( session0.equals( session1 ) );
		assertFalse( session0.getId().equals( session1.getId() ) );

		pst.close();
		store.discardAndClose();
	}

	@Test
	public void testRemove() throws Exception
	{
		System.out.println( "remove session" );
		DataStore store = new DataStore();
        PreparedStatement pst = store.getConnection().prepareStatement(
                "SELECT * FROM Sessions WHERE sessionid = ?");
		DataGenerators.generateSessionsForYear( 2000, store );
		EventList<Session> sessions = store.sessions();

		Session session0 = sessions.get( 0 );
		Session session1 = sessions.get( 1 );
		UUID session0Id = session0.getId();
        pst.setString( 1, session0Id.toString() );

		Session removed = sessions.remove( 0 );
		assertSame( session0, removed );
		assertNull (removed.getId());
		ResultSet results = pst.executeQuery();
		assertFalse( results.next() );
		results.close();
		assertFalse( sessions.contains( session0 ) );

		// just making sure we don't get an exception here
		session0.setStartAndEndTime( session1.getStartTime(), session1.getEndTime() );

		pst.close();
		store.discardAndClose();
	}

	@Test
	public void testAlter() throws Exception
	{
		System.out.println( "altering sessions updates database" );
		DataStore store = new DataStore();
        PreparedStatement pst = store.getConnection().prepareStatement(
                "SELECT * FROM Sessions WHERE sessionid = ?");
		DataGenerators.generateSessionsForYear( 2000, store );
		EventList<Session> sessions = store.sessions();

		Session session0 = sessions.get( 0 );
		UUID session0Id = session0.getId();
        pst.setString( 1, session0Id.toString() );

        Date before = new Date();
        Date then = new Date();
		Date newStartTime = new Date( session0.getStartTime().getTime() - HALF_AN_HOUR );
		session0.setStartTime( newStartTime );
		ResultSet results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( session0.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( session0.getEndTime(), results.getTimestamp( "endtime" ) );
        Date now = new Date();
        // we construct a new Date object, as the Timestamp object does
        // not behave properly (it does not set its internal fastTime
        // variable correctly, so Date.before() and Date.after() return
        // incorrect results)
        Date lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        Date created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( before.before( created ) );
		results.close();

        then = new Date();
		Date newEndTime = new Date( session0.getEndTime().getTime() - HALF_AN_HOUR );
		session0.setEndTime( newEndTime );
		results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( session0.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( session0.getEndTime(), results.getTimestamp( "endtime" ) );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( before.before( created ) );
		results.close();

        then = new Date();
		newStartTime = new Date( session0.getStartTime().getTime() - 2 * HALF_AN_HOUR );
		newEndTime = new Date( session0.getEndTime().getTime() + 2 * HALF_AN_HOUR );
		session0.setStartAndEndTime( newStartTime, newEndTime );
		results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( session0.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( session0.getEndTime(), results.getTimestamp( "endtime" ) );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( before.before( created ) );
		results.close();

		pst.close();
		store.discardAndClose();
	}

	@Test
	public void testGoodAlter() throws Exception
	{
		System.out.println( "altering sessions is allowed where it should be" );
		DataStore store = new DataStore();
		EventList<Session> sessions = store.sessions();

		Calendar startTime = new GregorianCalendar( 2000, 01, 20, 23, 30 );
		Calendar endTime = new GregorianCalendar( 2000, 01, 21, 1, 0 );
		Session session0 = new Session( startTime.getTime(), endTime.getTime() );
		startTime.add( Calendar.WEEK_OF_YEAR, 1 );
		endTime.add( Calendar.WEEK_OF_YEAR, 1 );
		Session session1 = new Session( startTime.getTime(), endTime.getTime() );

		sessions.add( session0 );
		sessions.add( session1 );

        // set to the same time again
		session0.setStartAndEndTime( session0.getStartTime(), session0.getEndTime() );
		session1.setStartAndEndTime( session1.getStartTime(), session1.getEndTime() );

		startTime = new GregorianCalendar( 2000, 02, 22, 23, 30 );
		endTime = new GregorianCalendar( 2000, 02, 23, 1, 0 );
		session0.setStartAndEndTime( startTime.getTime(), endTime.getTime() );

		startTime = new GregorianCalendar( 2000, 01, 19, 23, 30 );
		endTime = new GregorianCalendar( 2000, 01, 20, 1, 0 );
		session0.setStartAndEndTime( startTime.getTime(), endTime.getTime() );

		store.discardAndClose();
	}

	@Test
	public void testBadAlter() throws Exception
	{
		System.out.println( "altering sessions cannot create overlaps" );
		DataStore store = new DataStore();
		EventList<Session> sessions = store.sessions();

		Calendar startTime = new GregorianCalendar( 2000, 01, 20, 23, 30 );
		Calendar endTime = new GregorianCalendar( 2000, 01, 21, 1, 0 );
		Session session0 = new Session( startTime.getTime(), endTime.getTime() );
		startTime.add( Calendar.WEEK_OF_YEAR, 1 );
		endTime.add( Calendar.WEEK_OF_YEAR, 1 );
		Session session1 = new Session( startTime.getTime(), endTime.getTime() );

		sessions.add( session0 );
		sessions.add( session1 );

		try
		{
			session1.setStartAndEndTime( session0.getStartTime(), session0.getEndTime() );
			fail( "Overlapping session change allowed" );
		}
		catch ( ProhibitedChangeException ex )
		{
		}

		startTime.setTime(session0.getStartTime());
		startTime.add( Calendar.MINUTE, -15 );
		endTime.setTime(session0.getEndTime());
		endTime.add( Calendar.MINUTE, -15 );
		try
		{
			session1.setStartAndEndTime( startTime.getTime(), endTime.getTime() );
			fail( "Overlapping session change allowed" );
		}
		catch ( ProhibitedChangeException ex )
		{
		}

		startTime.add( Calendar.MINUTE, 30 );
		try
		{
			session1.setStartAndEndTime( startTime.getTime(), endTime.getTime() );
			fail( "Overlapping session change allowed" );
		}
		catch ( ProhibitedChangeException ex )
		{
		}

		endTime.add( Calendar.MINUTE, 30 );
		try
		{
			session1.setStartAndEndTime( startTime.getTime(), endTime.getTime() );
			fail( "Overlapping session change allowed" );
		}
		catch ( ProhibitedChangeException ex )
		{
		}

		store.discardAndClose();
	}

	@Test
	public void testCreate() throws Exception
	{
		System.out.println( "create session" );
		DataStore store = new DataStore();
        PreparedStatement countPst = store.getConnection().prepareStatement(
                "SELECT COUNT(*) FROM Sessions");
        PreparedStatement detailsPst = store.getConnection().prepareStatement(
                "SELECT * FROM Sessions WHERE sessionid = ?");
		EventList<Session> sessions = store.sessions();

		Calendar startTime = new GregorianCalendar( 2010, Calendar.JANUARY, 20, 23, 30 );
		Calendar endTime = new GregorianCalendar( 2010, Calendar.JANUARY, 21, 1, 0 );

        Date before = new Date();
        Date then = new Date();
		Session newSession = new Session( startTime.getTime(), endTime.getTime() );
		sessions.add( newSession );
        Date after = new Date();
		UUID newSessionId = newSession.getId();
		assertNotNull (newSessionId);
        detailsPst.setString( 1, newSessionId.toString() );
		ResultSet results = detailsPst.executeQuery();
		assertTrue (results.next());
		assertEquals( newSession.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( newSession.getEndTime(), results.getTimestamp( "endtime" ) );
        Date now = new Date();
        // we construct a new Date object, as the Timestamp object does
        // not behave properly (it does not set its internal fastTime
        // variable correctly, so Date.before() and Date.after() return
        // incorrect results)
        Date lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        Date created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
        assertFalse( before.after( created ) );
		results.close();

        then = new Date();
		startTime.add( Calendar.MINUTE, 30 );
		newSession.setStartTime( startTime.getTime() );
		results = detailsPst.executeQuery();
		assertTrue (results.next());
		assertEquals( newSession.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( newSession.getEndTime(), results.getTimestamp( "endtime" ) );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        Date created2 = new Date(results.getTimestamp( "created" ).getTime());
        assertEquals (created, created2);
		results.close();

        then = new Date();
		endTime.add( Calendar.MINUTE, 30 );
		newSession.setEndTime( endTime.getTime() );
		results = detailsPst.executeQuery();
		assertTrue (results.next());
		assertEquals( newSession.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( newSession.getEndTime(), results.getTimestamp( "endtime" ) );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created2 = new Date(results.getTimestamp( "created" ).getTime());
        assertEquals (created, created2);
		results.close();

        then = new Date();
		startTime.add( Calendar.MINUTE, 30 );
		endTime.add( Calendar.MINUTE, 30 );
		newSession.setStartAndEndTime( startTime.getTime(), endTime.getTime() );
		results = detailsPst.executeQuery();
		assertTrue (results.next());
		assertEquals( newSession.getStartTime(), results.getTimestamp( "starttime" ) );
		assertEquals( newSession.getEndTime(), results.getTimestamp( "endtime" ) );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created2 = new Date(results.getTimestamp( "created" ).getTime());
        assertEquals (created, created2);
		results.close();

		Session newSession2 = new Session( startTime.getTime(), endTime.getTime() );
		try
		{
			sessions.add( newSession2 );
			fail( "Overlapping sessions allowed" );
		}
		catch ( OverlappingSessionsException ex )
		{
			results = countPst.executeQuery();
			assertTrue (results.next());
			assertEquals( 1, results.getInt( 1 ) );
			results.close();
		}

		startTime.add( Calendar.MINUTE, 15 );
		newSession2.setStartTime( startTime.getTime() );
		try
		{
			sessions.add( newSession2 );
			fail( "Overlapping sessions allowed" );
		}
		catch ( OverlappingSessionsException ex )
		{
			results = countPst.executeQuery();
			assertTrue (results.next());
			assertEquals( 1, results.getInt( 1 ) );
			results.close();
		}

		endTime.add( Calendar.MINUTE, -15 );
		newSession2.setEndTime( endTime.getTime() );
		try
		{
			sessions.add( newSession2 );
			fail( "Overlapping sessions allowed" );
		}
		catch ( OverlappingSessionsException ex )
		{
			results = countPst.executeQuery();
			assertTrue (results.next());
			assertEquals( 1, results.getInt( 1 ) );
			results.close();
		}

		startTime.add( Calendar.MINUTE, -30 );
		newSession2.setStartTime( startTime.getTime() );
		try
		{
			sessions.add( newSession2 );
			fail( "Overlapping sessions allowed" );
		}
		catch ( OverlappingSessionsException ex )
		{
			results = countPst.executeQuery();
			assertTrue (results.next());
			assertEquals( 1, results.getInt( 1 ) );
			results.close();
		}


		startTime.add( Calendar.HOUR, 3 );
		endTime.add( Calendar.HOUR, 3 );
		newSession2.setStartAndEndTime( startTime.getTime(), endTime.getTime() );
		sessions.add( newSession2 );

        results = countPst.executeQuery();
		assertTrue (results.next());
		assertEquals( 2, results.getInt( 1 ) );
		results.close();


		startTime.add( Calendar.HOUR, -10 );
		endTime.add( Calendar.HOUR, -10 );
		Session newSession3 = new Session( startTime.getTime(), endTime.getTime() );
		sessions.add( newSession3 );

        results = countPst.executeQuery();
		assertTrue (results.next());
		assertEquals( 3, results.getInt( 1 ) );
		results.close();

		detailsPst.close();
        countPst.close();
		store.discardAndClose();
	}
}
