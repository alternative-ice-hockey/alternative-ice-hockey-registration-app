/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.util.EnumSet;
import altsdatabase.data.event.SessionEvent;
import altsdatabase.data.event.SessionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alex
 */
public class SessionTest
{
    public SessionTest()
    {
    }

    private class TestSessionEventListener implements SessionListener
    {
        Session session;
        SessionEvent lastEvent;
        boolean changeAllowed = true;

        public TestSessionEventListener( Session session )
        {
            this.session = session;
        }

        public boolean isChangeAllowed()
        {
            return changeAllowed;
        }

        public void setChangeAllowed( boolean changeAllowed )
        {
            this.changeAllowed = changeAllowed;
        }

        public void sessionChanged( SessionEvent event )
        {
            lastEvent = event;
        }

        public void checkChange( Date origDate, SessionEvent.Changes change )
        {
            assertSame( session, lastEvent.getSource() );
            assertSame( session, lastEvent.getSession() );
            assertEquals( lastEvent.getChangeMask(), EnumSet.of( change ) );
            if ( change == SessionEvent.Changes.StartTime )
            {
                assertEquals( origDate, lastEvent.getOldStartTime() );
                assertEquals( lastEvent.getSession().getEndTime(), lastEvent.getOldEndTime() );
            }
            else
            {
                assertEquals( lastEvent.getSession().getStartTime(), lastEvent.getOldStartTime() );
                assertEquals( origDate, lastEvent.getOldEndTime() );
            }
            clearEvent();
        }

        public void checkChange( Date origStart, Date origEnd )
        {
            assertSame( session, lastEvent.getSource() );
            assertSame( session, lastEvent.getSession() );
            assertEquals( lastEvent.getChangeMask(), EnumSet.allOf( SessionEvent.Changes.class ) );
            assertEquals( origStart, lastEvent.getOldStartTime() );
            assertEquals( origEnd, lastEvent.getOldEndTime() );
            clearEvent();
        }

        public void checkNoEvent()
        {
            assertNull( lastEvent );
        }

        public void clearEvent()
        {
            lastEvent = null;
        }

        public boolean aboutToChangeSession( Session session, Date newStart, Date newEnd )
        {
            return changeAllowed;
        }
    }

    /**
     * Test of equals method, of class Session.
     */
    @Test
    public void testEquals() throws Exception
    {
        System.out.println( "equals" );

        Calendar startTime = new GregorianCalendar( 2000, 01, 20, 23, 30 );
        Calendar endTime = new GregorianCalendar( 2000, 01, 21, 1, 0 );
        Session session1 = new Session( startTime.getTime(), endTime.getTime() );
        Session session2 = new Session( startTime.getTime(), endTime.getTime() );

        endTime.add( Calendar.HOUR_OF_DAY, 1 );
        Session session3 = new Session( startTime.getTime(), endTime.getTime() );

        startTime.add( Calendar.HOUR_OF_DAY, -1 );
        endTime.add( Calendar.HOUR_OF_DAY, -1 );
        Session session4 = new Session( startTime.getTime(), endTime.getTime() );

        startTime.add( Calendar.YEAR, 1 );
        endTime.add( Calendar.YEAR, 1 );
        Session session5 = new Session( startTime.getTime(), endTime.getTime() );

        assertTrue( session1.equals( session1 ) );
        assertTrue( session1.equals( session2 ) );
        assertFalse( session1.equals( session3 ) );
        assertFalse( session1.equals( session4 ) );
        assertFalse( session1.equals( session5 ) );

        assertFalse( session1.equals( null ) );
    }

    /**
     * Test of compare method, of class Session.
     */
    @Test
    public void testCompare() throws Exception
    {
        System.out.println( "compare" );

        Calendar startTime = new GregorianCalendar( 2000, 01, 20, 23, 30 );
        Calendar endTime = new GregorianCalendar( 2000, 01, 21, 1, 0 );
        Session session1 = new Session( startTime.getTime(), endTime.getTime() );
        Session session2 = new Session( startTime.getTime(), endTime.getTime() );

        endTime.add( Calendar.HOUR_OF_DAY, 1 );
        Session session3 = new Session( startTime.getTime(), endTime.getTime() );

        startTime.add( Calendar.HOUR_OF_DAY, -1 );
        endTime.add( Calendar.HOUR_OF_DAY, -1 );
        Session session4 = new Session( startTime.getTime(), endTime.getTime() );

        startTime.add( Calendar.YEAR, 1 );
        endTime.add( Calendar.YEAR, 1 );
        Session session5 = new Session( startTime.getTime(), endTime.getTime() );

        assert (session1.compareTo( session2 ) == 0);
        assert (session1.compareTo( session3 ) < 0);
        assert (session1.compareTo( session4 ) > 0);
        assert (session1.compareTo( session5 ) < 0);

        try
        {
            session1.compareTo( null );
            fail( "compareTo accepted null" );
        }
        catch ( NullPointerException ex )
        {
        }
    }

    /**
     * Test of getId method, of class Session.
     */
    @Test
    public void testGetId() throws Exception
    {
        System.out.println( "getId" );

        UUID session2Id = UUID.randomUUID();
        Calendar startTime = new GregorianCalendar( 2000, 01, 20, 23, 30 );
        Calendar endTime = new GregorianCalendar( 2000, 01, 21, 1, 0 );
        Session session1 = new Session( startTime.getTime(), endTime.getTime() );
        Session session2 = new Session( session2Id, startTime.getTime(), endTime.getTime() );

        assertNull( session1.getId() );
        assertEquals( session2Id, session2.getId() );

        UUID session1Id = UUID.randomUUID();
        session1.setId( session1Id );
        assertEquals( session1Id, session1.getId() );
    }

    /**
     * Test of getStartTime method, of class Session.
     */
    @Test
    public void testGetStartTime() throws Exception
    {
        System.out.println( "getStartTime" );

        Calendar startTime1 = new GregorianCalendar( 2000, 01, 20, 23, 30 );
        Calendar endTime1 = new GregorianCalendar( 2000, 01, 21, 1, 0 );
        Session session1 = new Session( startTime1.getTime(), endTime1.getTime() );
        assertEquals( startTime1.getTime(), session1.getStartTime() );

        Calendar startTime2 = new GregorianCalendar( 2002, 02, 06, 16, 30 );
        Calendar endTime2 = new GregorianCalendar( 2002, 02, 06, 17, 30 );
        Session session2 = new Session( UUID.randomUUID(), startTime2.getTime(), endTime2.getTime() );
        assertEquals( startTime2.getTime(), session2.getStartTime() );
    }

    /**
     * Test of setStartTime method, of class Session.
     */
    @Test
    public void testSetStartTime() throws Exception
    {
        System.out.println( "setStartTime" );

        Calendar startDate = new GregorianCalendar(
                2000, Calendar.JUNE, 1, 0, 30 );
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add( Calendar.MINUTE, 90 );

        Session session = new Session( startDate.getTime(), endDate.getTime() );
        TestSessionEventListener listener =
                new TestSessionEventListener( session );
        session.addSessionListener( listener );

        Date oldStart = startDate.getTime();

        startDate.add( Calendar.MINUTE, 30 );
        session.setStartTime( startDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        listener.checkChange( oldStart, SessionEvent.Changes.StartTime );

        oldStart = startDate.getTime();

        startDate = new GregorianCalendar( 2000, Calendar.FEBRUARY, 1, 0, 30 );
        session.setStartTime( startDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        listener.checkChange( oldStart, SessionEvent.Changes.StartTime );

        oldStart = startDate.getTime();

        session.setStartTime( startDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        listener.checkNoEvent();

        try
        {
            session.setStartTime( endDate.getTime() );
            fail( "setStartTime accepted an invalid value" );
        }
        catch ( IllegalArgumentException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            listener.checkNoEvent();
        }

        try
        {
            Calendar badStartDate = (Calendar) endDate.clone();
            badStartDate.add( Calendar.MINUTE, 30 );
            session.setStartTime( badStartDate.getTime() );
            fail( "setStartTime accepted an invalid value" );
        }
        catch ( IllegalArgumentException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            listener.checkNoEvent();
        }

        try
        {
            session.setStartTime( null );
            fail( "setStartTime accepted null" );
        }
        catch ( NullPointerException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            listener.checkNoEvent();
        }

        startDate.add( Calendar.HOUR, -1 );
        listener.setChangeAllowed( false );

        try
        {
            session.setStartTime( startDate.getTime() );
            fail( "setStartTime allowed a prohibited change" );
        }
        catch ( ProhibitedChangeException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of getEndTime method, of class Session.
     */
    @Test
    public void testGetEndTime() throws Exception
    {
        System.out.println( "getEndTime" );

        Calendar startTime1 = new GregorianCalendar( 2000, 01, 20, 23, 30 );
        Calendar endTime1 = new GregorianCalendar( 2000, 01, 21, 1, 0 );
        Session session1 = new Session( startTime1.getTime(), endTime1.getTime() );
        assertEquals( endTime1.getTime(), session1.getEndTime() );

        Calendar startTime2 = new GregorianCalendar( 2002, 02, 06, 16, 30 );
        Calendar endTime2 = new GregorianCalendar( 2002, 02, 06, 17, 30 );
        Session session2 = new Session( UUID.randomUUID(), startTime2.getTime(), endTime2.getTime() );
        assertEquals( endTime2.getTime(), session2.getEndTime() );
    }

    /**
     * Test of setEndTime method, of class Session.
     */
    @Test
    public void testSetEndTime() throws Exception
    {
        System.out.println( "setEndTime" );

        Calendar startDate = new GregorianCalendar(
                2000, Calendar.JUNE, 1, 0, 30 );
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add( Calendar.MINUTE, 90 );

        Session session = new Session( startDate.getTime(), endDate.getTime() );
        TestSessionEventListener listener =
                new TestSessionEventListener( session );
        session.addSessionListener( listener );

        Date oldEnd = endDate.getTime();

        endDate.add( Calendar.MINUTE, 30 );
        session.setEndTime( endDate.getTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkChange( oldEnd, SessionEvent.Changes.EndTime );

        oldEnd = endDate.getTime();

        endDate = new GregorianCalendar( 2000, Calendar.SEPTEMBER, 1, 0, 30 );
        session.setEndTime( endDate.getTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkChange( oldEnd, SessionEvent.Changes.EndTime );

        oldEnd = endDate.getTime();

        session.setEndTime( endDate.getTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkNoEvent();

        try
        {
            session.setEndTime( startDate.getTime() );
            fail( "setEndTime accepted an invalid value" );
        }
        catch ( IllegalArgumentException ex )
        {
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        try
        {
            Calendar badEndDate = (Calendar) startDate.clone();
            badEndDate.add( Calendar.MINUTE, -30 );
            session.setEndTime( badEndDate.getTime() );
            fail( "setEndTime accepted an invalid value" );
        }
        catch ( IllegalArgumentException ex )
        {
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        try
        {
            session.setEndTime( null );
            fail( "setEndTime accepted null" );
        }
        catch ( NullPointerException ex )
        {
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        endDate.add( Calendar.HOUR, 1 );
        listener.setChangeAllowed( false );

        try
        {
            session.setEndTime( endDate.getTime() );
            fail( "setStartTime allowed a prohibited change" );
        }
        catch ( ProhibitedChangeException ex )
        {
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of setStartTime method, of class Session.
     */
    @Test
    public void testSetStartAndEndTime() throws Exception
    {
        System.out.println( "setStartTime" );

        Calendar startDate = new GregorianCalendar(
                2000, Calendar.JUNE, 1, 0, 30 );
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add( Calendar.MINUTE, 90 );

        Session session = new Session( startDate.getTime(), endDate.getTime() );
        TestSessionEventListener listener =
                new TestSessionEventListener( session );
        session.addSessionListener( listener );

        Date oldStart = startDate.getTime();
        Date oldEnd = endDate.getTime();

        startDate.add( Calendar.MINUTE, 15 );
        endDate.add( Calendar.MINUTE, -15 );
        session.setStartAndEndTime( startDate.getTime(),
                                    endDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkChange( oldStart, oldEnd );

        oldStart = startDate.getTime();
        oldEnd = endDate.getTime();

        startDate = new GregorianCalendar( 2000, Calendar.FEBRUARY, 1, 0, 30 );
        session.setStartAndEndTime( startDate.getTime(), endDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkChange( oldStart, SessionEvent.Changes.StartTime );

        oldStart = startDate.getTime();
        oldEnd = endDate.getTime();

        endDate = new GregorianCalendar( 2000, Calendar.FEBRUARY, 1, 2, 0 );
        session.setStartAndEndTime( startDate.getTime(), endDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkChange( oldEnd, SessionEvent.Changes.EndTime );

        oldStart = startDate.getTime();
        oldEnd = endDate.getTime();

        startDate = new GregorianCalendar( 2000, Calendar.NOVEMBER, 1, 0, 30 );
        endDate = new GregorianCalendar( 2000, Calendar.NOVEMBER, 1, 2, 00 );
        session.setStartAndEndTime( startDate.getTime(), endDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkChange( oldStart, oldEnd );

        oldStart = startDate.getTime();
        oldEnd = endDate.getTime();

        session.setStartAndEndTime( startDate.getTime(), endDate.getTime() );
        assertEquals( startDate.getTime(), session.getStartTime() );
        assertEquals( endDate.getTime(), session.getEndTime() );
        listener.checkNoEvent();

        try
        {
            Calendar badStartDate = new GregorianCalendar(
                    1999, Calendar.FEBRUARY, 14, 0, 30 );
            Calendar badEndDate = (Calendar) badStartDate.clone();
            session.setStartAndEndTime( badStartDate.getTime(),
                                        badEndDate.getTime() );
            fail( "setStartAndEndTime accepted invalid values" );
        }
        catch ( IllegalArgumentException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        try
        {
            Calendar badStartDate = new GregorianCalendar(
                    1999, Calendar.FEBRUARY, 14, 0, 30 );
            Calendar badEndDate = (Calendar) badStartDate.clone();
            badEndDate.add( Calendar.MINUTE, -30 );
            session.setStartAndEndTime( badStartDate.getTime(),
                                        badEndDate.getTime() );
            fail( "setStartAndEndTime accepted invalid values" );
        }
        catch ( IllegalArgumentException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        startDate = new GregorianCalendar( 2000, Calendar.NOVEMBER, 1, 23, 30 );
        endDate = new GregorianCalendar( 2000, Calendar.NOVEMBER, 2, 1, 15 );

        try
        {
            session.setStartAndEndTime( startDate.getTime(), null );
            fail( "setStartAndEndTime accepted null" );
        }
        catch ( NullPointerException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        try
        {
            session.setStartAndEndTime( null, endDate.getTime() );
            fail( "setStartAndEndTime accepted null" );
        }
        catch ( NullPointerException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        try
        {
            session.setStartAndEndTime( null, null );
            fail( "setStartAndEndTime accepted null" );
        }
        catch ( NullPointerException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        startDate.add( Calendar.HOUR, -1 );
        endDate.add( Calendar.HOUR, 1 );
        listener.setChangeAllowed( false );

        try
        {
            session.setStartAndEndTime( startDate.getTime(), endDate.getTime() );
            fail( "setStartTime allowed a prohibited change" );
        }
        catch ( ProhibitedChangeException ex )
        {
            assertEquals( oldStart, session.getStartTime() );
            assertEquals( oldEnd, session.getEndTime() );
            listener.checkNoEvent();
        }

        session.removeSessionListener( listener );
        session.setStartAndEndTime( startDate.getTime(), endDate.getTime() );
        listener.checkNoEvent();
    }
}