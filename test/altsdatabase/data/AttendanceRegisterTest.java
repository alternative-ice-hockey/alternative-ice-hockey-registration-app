/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package altsdatabase.data;

import java.util.UUID;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;
import static altsdatabase.data.DataGenerators.*;

/**
 *
 * @author alex
 */
public class AttendanceRegisterTest
{

    public AttendanceRegisterTest() {
    }

    void populateRegister( DataStore store, UUID sessionid )
            throws SQLException
    {
        PreparedStatement pst = store.getConnection().prepareStatement(
                "INSERT INTO Attendance " +
                "(memberid, sessionid) " +
                "VALUES (?, ?)" );
        pst.setString( 2, sessionid.toString() );

        pst.setString( 1, memberData[0].id.toString() );
        pst.executeUpdate();
        pst.setString( 1, memberData[1].id.toString() );
        pst.executeUpdate();
        pst.setString( 1, memberData[2].id.toString() );
        pst.executeUpdate();

        pst.close();
    }

    void populateRegisters( DataStore store )
            throws SQLException
    {
        Statement st = store.getConnection().createStatement();
        ResultSet result = st.executeQuery(
                "SELECT * FROM Sessions" );
        while ( result.next() )
        {
            populateRegister( store, QueryManager.sessionIdFromResult( result, "sessionid" ) );
        }
        st.close();
    }

    void checkAddedTimestamp( DataStore store, Date then, Member member, Session session ) throws SQLException
    {
        Date now = new Date();
        PreparedStatement pst = store.getQueryManager().statementForQuery(
                "SELECT * FROM Attendance where memberid = ? and sessionid = ?"
                );
        pst.setString( 1, member.getId().toString() );
        pst.setString( 2, session.getId().toString() );
        ResultSet result = pst.executeQuery();
        assertTrue (result.next());
        Date addedat = new Date(result.getTimestamp( "added" ).getTime());
        assertFalse( then.after( addedat ) );
        assertFalse( now.before( addedat ) );
        result.close();
    }

	@Test
	public void testEmptyRegister() throws Exception
	{
		System.out.println( "empty register" );
		DataStore store = new DataStore();
		DataGenerators.generateSessionsForYear( 2000, store );
		EventList<Session> sessions = store.sessions();

		AttendanceRegister register = store.getRegister( sessions.get( 0 ) );
		assertNotNull( register );
		assertTrue (register.isEmpty());

		store.discardAndClose();
	}

	@Test
	public void testRegisterSize() throws Exception
	{
		System.out.println( "size (from database)" );
		DataStore store = new DataStore();
		DataGenerators.generateSessionsForYear( 2000, store );
        populateMembersTable( store );
        populateRegisters( store );
		EventList<Session> sessions = store.sessions();

		AttendanceRegister register = store.getRegister( sessions.get( 0 ) );
		assertEquals( 3, register.size() );

		store.discardAndClose();
	}

    /**
     * Test of get method, of class AttendanceRegister.
     */
    @Test
    public void testGet() throws Exception
    {
        System.out.println( "get (from database)" );

        DataStore store = new DataStore();
        generateSessionsForYear( 2000, store);
        populateMembersTable( store );
        populateRegisters( store );
        AttendanceRegister register = store.getRegister( store.sessions().get( 0 ) );

        Member[] members = getMembers( store );
        assertTrue( register.contains( members[0] ) );
        assertTrue( register.contains( members[1] ) );
        assertTrue( register.contains( members[2] ) );
        assertFalse( register.contains( members[3] ) );

        store.discardAndClose();
    }

    /**
     * Test of member changes, of class AttendanceRegister.
     */
    /*@Test
    public void testMemberChangePropogate() throws Exception
    {
        System.out.println( "member change propogate" );

        DataStore store = new DataStore();
        generateSessionsForYear( 2000, store, true );
        populateMembersTable( store );
        populateRegisters( store );
        AttendanceRegister register = store.getRegister( store.sessions().get( 0 ) );

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		register.addListEventListener( listener );

		store.members().get( 0 ).setFirstName( "Foo" );
		assertNotNull( listener.lastEvent );
		assertTrue( listener.lastEvent.hasNext() );
		listener.lastEvent.next();
		assertEquals( ListEvent.UPDATE, listener.lastEvent.getType() );
		assertSame( store.members().get( 0 ), listener.lastEvent.getNewValue() );

        store.discardAndClose();
    }*/

    /**
     * Test of member deletes, of class AttendanceRegister.
     */
    @Test
    public void testMemberDeletePropogate() throws Exception
    {
        System.out.println( "member delete propogate" );

        DataStore store = new DataStore();
        generateSessionsForYear( 2000, store);
        populateMembersTable( store );
        populateRegisters( store );
        AttendanceRegister register = store.getRegister( store.sessions().get( 0 ) );

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		register.addListEventListener( listener );

        Member[] members = getMembers( store );
        Member removedMember = members[0];
		int removedIndex = register.indexOf( removedMember );
		assertTrue( store.members().remove( removedMember ) );
		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.DELETE, listener.lastChangeSet.get(0).changeType );
		assertSame( removedMember, listener.lastChangeSet.get(0).oldValue );
		assertSame( removedIndex, listener.lastChangeSet.get(0).index );

        store.discardAndClose();
    }

    /**
     * Test of method add, of class AttendanceRegister.
     */
    @Test
    public void testAdd() throws Exception
    {
        System.out.println( "add" );

        DataStore store = new DataStore();
        generateSessionsForYear( 2000, store);
        populateMembersTable( store );
        Session session0 = store.sessions().get( 0 );
        AttendanceRegister register = store.getRegister( session0 );
		EventList<Member> members = store.members();

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		register.addListEventListener( listener );

        Date then = new Date();
		register.add( members.get(1) );
		assertEquals( 1, register.size() );
		assertSame( members.get(1), register.get(0) );
		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.INSERT, listener.lastChangeSet.get(0).changeType );
		assertSame( 0, listener.lastChangeSet.get(0).index );
        checkAddedTimestamp( store, then, members.get(1), session0 );

        then = new Date();
		register.add( members.get(3) );
		assertEquals( 2, register.size() );
		assertSame( members.get(3), register.get(1) );
		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.INSERT, listener.lastChangeSet.get(0).changeType );
		assertSame( 1, listener.lastChangeSet.get(0).index );
        checkAddedTimestamp( store, then, members.get(3), session0 );

        then = new Date();
		register.add( 1, members.get(2) );
		assertEquals( 3, register.size() );
		assertSame( members.get(2), register.get(1) );
		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.INSERT, listener.lastChangeSet.get(0).changeType );
		assertSame( 1, listener.lastChangeSet.get(0).index );
        checkAddedTimestamp( store, then, members.get(2), session0 );

		listener.lastChangeSet.clear();
		register.add( 1, members.get(3) );
		assertTrue( listener.lastChangeSet.isEmpty() );
		assertEquals( 3, register.size() );
		assertSame( members.get(2), register.get(1) );

		Member otherMember = new Member( null, "Foo Bar", null, null, Member.Status.Other );
		try
		{
			register.add( otherMember );
			fail("Allowed invalid member");
		}
		catch ( IllegalArgumentException ex )
		{
			assertTrue( listener.lastChangeSet.isEmpty() );
			assertEquals( 3, register.size() );
		}

		try
		{
			register.add( null );
			fail("Allowed null member");
		}
		catch ( NullPointerException ex )
		{
			assertTrue( listener.lastChangeSet.isEmpty() );
			assertEquals( 3, register.size() );
		}

        store.discardAndClose();
    }

    /**
     * Test of method remove, of class AttendanceRegister.
     */
    @Test
    public void testSet() throws Exception
    {
        System.out.println( "set" );

        DataStore store = new DataStore();
        generateSessionsForYear( 2000, store);
        populateMembersTable( store );
        populateRegisters( store );
        AttendanceRegister register = store.getRegister( store.sessions().get( 0 ) );
		EventList<Member> members = store.members();

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		register.addListEventListener( listener );

		register.set( 0, register.get( 0 ) );
		assertTrue( listener.lastChangeSet.isEmpty() );
		assertEquals( 3, register.size() );

		Member origMember = register.get( 1 );
        Member[] membersArr = getMembers( store );
		register.set( 1, membersArr[3] );
		assertEquals( 3, register.size() );
		assertSame( membersArr[3], register.get(1) );
		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.UPDATE, listener.lastChangeSet.get(0).changeType );
		assertSame( origMember, listener.lastChangeSet.get(0).oldValue );
		assertSame( 1, listener.lastChangeSet.get(0).index );

        store.discardAndClose();
    }

    /**
     * Test of method remove, of class AttendanceRegister.
     */
    @Test
    public void testRemove() throws Exception
    {
        System.out.println( "remove" );

        DataStore store = new DataStore();
        generateSessionsForYear( 2000, store);
        populateMembersTable( store );
        populateRegisters( store );
        AttendanceRegister register = store.getRegister( store.sessions().get( 0 ) );

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		register.addListEventListener( listener );

		int removedIndex = 1;
		Member expRemovedMember = register.get( removedIndex );
		Member removedMember = register.remove( removedIndex );
		assertSame( expRemovedMember, removedMember );
		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.DELETE, listener.lastChangeSet.get(0).changeType );
		assertSame( removedMember, listener.lastChangeSet.get(0).oldValue );
		assertSame( removedIndex, listener.lastChangeSet.get(0).index );

        store.discardAndClose();
    }
}