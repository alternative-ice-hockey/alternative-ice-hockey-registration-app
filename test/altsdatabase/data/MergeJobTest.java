/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.io.File;
import java.util.Arrays;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static altsdatabase.data.DataGenerators.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author alex
 */
public class MergeJobTest
{
    static File initFile;
    File srcFile;
    File tgtFile;
    static DataStore initStore;
    DataStore tgtStore;
    DataStore srcStore;

    public MergeJobTest()
    {
    }

    @BeforeClass
    public static void setupInitStore() throws Exception
    {
        initFile = File.createTempFile("test-alts-db", ".alts");

        initStore = new DataStore();
        populateMembersTable(initStore);
        generateSessionsForYear(2010, initStore);
        initStore.save(initFile);
    }

    @AfterClass
    public static void discardInitStore() throws Exception
    {
        initStore.discardAndClose();
        initFile.delete();
        initFile = null;
    }

    public static final MemberData[] memberData1 =
    {
        new MemberData( UUID.randomUUID(), null, "Jimmy Hendrix", "jimmy@famous.com", null, Member.Status.Other ),
        new MemberData( UUID.randomUUID(), "2000123", "Mark Cohen", "mark.cohen@wadh.ox.ac.uk", "Wadham", Member.Status.OUStudent ),
        new MemberData( UUID.randomUUID(), "93248329482", "Oscar Wilde", null, null, Member.Status.OUAlumnus ),
    };

    public static final MemberData[] memberData2 =
    {
        new MemberData( UUID.randomUUID(), null, "James Earl Jones", "jj@earldom.com", null, Member.Status.OUStaff ),
        new MemberData( UUID.randomUUID(), "2444123", "Conan the Barbarian", null, "Merton", Member.Status.OUStudent ),
        new MemberData( UUID.randomUUID(), "92323432333", "Jenny Hill", null, null, Member.Status.Other ),
    };

    public static final MemberData[] memberData3a =
    {
        new MemberData( UUID.randomUUID(), "23849237", "James Earl Jones", "jj@earldom.com", null, Member.Status.OUStaff ),
        new MemberData( UUID.randomUUID(), "73423", "John Smith", null, "Corpus", Member.Status.OUStaff ),
        new MemberData( UUID.randomUUID(), "5346346", "Conan the Barbarian", null, "Merton", Member.Status.OUStudent ),
        new MemberData( UUID.randomUUID(), "74364345", "Jenny Hill", "jenny@hill.com", null, Member.Status.Other ),
    };

    public static final MemberData[] memberData3b =
    {
        new MemberData( UUID.randomUUID(), "23849237", "James Earl Jones", null, "Corpus", Member.Status.OUStaff ),
        new MemberData( UUID.randomUUID(), "73423", "John Smith", "js@earldom.com", null, Member.Status.OUStaff ),
        new MemberData( UUID.randomUUID(), "5346346", "Conan the Barbarian", null, "Wadham", Member.Status.OUStudent ),
        new MemberData( UUID.randomUUID(), "74364345", "Jenny Hill", "hill@jenny.com", null, Member.Status.Other ),
    };

    @Before
    public void createStores() throws Exception
    {
        srcFile = File.createTempFile("test-alts-db", ".alts");
        tgtFile = File.createTempFile("test-alts-db", ".alts");
        tgtStore = DataStore.open(initFile);
        srcStore = DataStore.open(initFile);
    }

    @After
    public void discardStores() throws Exception
    {
        srcStore.discardAndClose();
        srcStore = null;
        tgtStore.discardAndClose();
        tgtStore = null;
        srcFile.delete();
        srcFile = null;
        tgtFile.delete();
        tgtFile = null;
    }

    @Test
    public void testGetters()
    {
        System.out.println("getters");

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        assertEquals(tgtStore, instance.getMergeTarget());
        assertEquals(srcStore, instance.getMergeSource());
        assertTrue(instance.getWarnings().isEmpty());
    }

    @Test
    public void testNoCheckPoints() throws Exception
    {
        System.out.println("no checkpoints");

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
    }

    @Test
    public void testNoChanges() throws Exception
    {
        System.out.println("no changes");

        srcStore.save(srcFile);
        tgtStore.save(tgtFile);
        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
    }

    @Test
    public void testTargetOnlyAdditions() throws Exception
    {
        System.out.println("target additions only (members and attendance)");

        addToMembersTable(tgtStore, Arrays.asList(memberData1));
        assertEquals(memberData.length + memberData1.length, tgtStore.members().size());
        AttendanceRegister r = tgtStore.getRegister(tgtStore.sessions().get(0));
        r.add(findMember(memberData[0], tgtStore));
        r.add(findMember(memberData[1], tgtStore));
        r.add(findMember(memberData1[1], tgtStore));
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
    }

    @Test
    public void testSourceOnlyAdditions() throws Exception
    {
        System.out.println("source additions only (members and attendance)");

        Session s = srcStore.sessions().get(0);
        addToMembersTable(srcStore, Arrays.asList(memberData1));
        assertEquals(memberData.length + memberData1.length, srcStore.members().size());
        AttendanceRegister r = srcStore.getRegister(s);
        r.add(findMember(memberData[0], srcStore));
        r.add(findMember(memberData[1], srcStore));
        r.add(findMember(memberData1[1], srcStore));
        srcStore.save(srcFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        Session ts = findSession(s.getId(), tgtStore);
        assertEquals(3, srcStore.getRegister(ts).size());
    }

    @Test
    public void testSourceAndTargetAdditions() throws Exception
    {
        System.out.println("source and target additions (members and attendance; no overlap)");

        Session ss = srcStore.sessions().get(0);
        addToMembersTable(srcStore, Arrays.asList(memberData1));
        AttendanceRegister sr = srcStore.getRegister(ss);
        sr.add(findMember(memberData[0], srcStore));
        sr.add(findMember(memberData[1], srcStore));
        sr.add(findMember(memberData1[1], srcStore));
        srcStore.save(srcFile);

        addToMembersTable(tgtStore, Arrays.asList(memberData2));
        Session ts = findSession(ss.getId(), tgtStore);
        AttendanceRegister tr = tgtStore.getRegister(ts);
        tr.add(findMember(memberData[2], tgtStore));
        tr.add(findMember(memberData[3], tgtStore));
        tr.add(findMember(memberData2[2], tgtStore));
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(3, srcStore.getRegister(ss).size());
        assertEquals(6, tgtStore.getRegister(ts).size());
    }

    @Test
    public void testSourceAndTargetAdditionsWithOverlap() throws Exception
    {
        System.out.println("source and target additions (members and attendance; overlap)");

        Session ss = srcStore.sessions().get(0);
        addToMembersTable(srcStore, Arrays.asList(memberData1));
        AttendanceRegister sr = srcStore.getRegister(ss);
        sr.add(findMember(memberData[0], srcStore));
        sr.add(findMember(memberData[1], srcStore));
        sr.add(findMember(memberData1[0], srcStore));
        sr.add(findMember(memberData1[1], srcStore));
        srcStore.save(srcFile);

        addToMembersTable(tgtStore, Arrays.asList(memberData1));
        addToMembersTable(tgtStore, Arrays.asList(memberData2));
        Session ts = findSession(ss.getId(), tgtStore);
        AttendanceRegister tr = tgtStore.getRegister(ts);
        tr.add(findMember(memberData[0], tgtStore));
        tr.add(findMember(memberData[3], tgtStore));
        tr.add(findMember(memberData1[1], tgtStore));
        tr.add(findMember(memberData1[2], tgtStore));
        tr.add(findMember(memberData2[0], tgtStore));
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(4, srcStore.getRegister(ss).size());
        assertEquals(7, tgtStore.getRegister(ts).size());
    }

    @Test
    public void mergeIntoEmpty() throws Exception
    {
        System.out.println("merge into empty store");

        Session ss = srcStore.sessions().get(0);
        AttendanceRegister sr = srcStore.getRegister(ss);
        sr.add(findMember(memberData[0], srcStore));
        sr.add(findMember(memberData[1], srcStore));
        srcStore.save(srcFile);

        tgtStore.discardAndClose();
        tgtStore = new DataStore();

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(2, srcStore.getRegister(ss).size());
        assertEquals(2, tgtStore.getRegister(findSession(ss.getId(), tgtStore)).size());
    }

    @Test
    public void testSourceOnlyMemberCodeChanges() throws Exception
    {
        System.out.println("source only member code changes");

        // change from string
        findMember(memberData[0], srcStore).setMembershipCode("4321");
        // change from null
        findMember(memberData[4], srcStore).setMembershipCode("yhn");
        // change to null
        findMember(memberData[2], srcStore).setMembershipCode(null);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("4321", findMember(memberData[0], tgtStore).getMembershipCode());
        assertEquals("yhn", findMember(memberData[4], tgtStore).getMembershipCode());
        assertEquals("", findMember(memberData[2], tgtStore).getMembershipCode());
    }

    @Test
    public void testTargetOnlyMemberCodeChanges() throws Exception
    {
        System.out.println("target only member code changes");

        // change from string
        findMember(memberData[0], tgtStore).setMembershipCode("4321");
        // change from null
        findMember(memberData[4], tgtStore).setMembershipCode("yhn");
        // change to null
        findMember(memberData[2], tgtStore).setMembershipCode(null);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("4321", findMember(memberData[0], tgtStore).getMembershipCode());
        assertEquals("yhn", findMember(memberData[4], tgtStore).getMembershipCode());
        assertEquals("", findMember(memberData[2], tgtStore).getMembershipCode());
    }

    @Test
    public void testSourceOnlyMemberNameChanges() throws Exception
    {
        System.out.println("source only member name changes");

        // change from string
        findMember(memberData[0], srcStore).setName("Mark Brown");
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("Mark Brown", findMember(memberData[0], tgtStore).getName());
    }

    @Test
    public void testTargetOnlyMemberNameChanges() throws Exception
    {
        System.out.println("target only member name changes");

        // change from string
        findMember(memberData[0], tgtStore).setName("Mark Brown");
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("Mark Brown", findMember(memberData[0], tgtStore).getName());
    }

    @Test
    public void testSourceOnlyMemberContactChanges() throws Exception
    {
        System.out.println("source only member contact changes");

        // change from string
        findMember(memberData[0], srcStore).setContactDetails("bloggs@joe.com");
        // change from null
        findMember(memberData[1], srcStore).setContactDetails("james@hudson.com");
        // change to null
        findMember(memberData[3], srcStore).setContactDetails(null);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("bloggs@joe.com", findMember(memberData[0], tgtStore).getContactDetails());
        assertEquals("james@hudson.com", findMember(memberData[1], tgtStore).getContactDetails());
        assertEquals("", findMember(memberData[3], tgtStore).getContactDetails());
    }

    @Test
    public void testTargetOnlyMemberContactChanges() throws Exception
    {
        System.out.println("target only member contact changes");

        // change from string
        findMember(memberData[0], tgtStore).setContactDetails("bloggs@joe.com");
        // change from null
        findMember(memberData[1], tgtStore).setContactDetails("james@hudson.com");
        // change to null
        findMember(memberData[3], srcStore).setContactDetails(null);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("bloggs@joe.com", findMember(memberData[0], tgtStore).getContactDetails());
        assertEquals("james@hudson.com", findMember(memberData[1], tgtStore).getContactDetails());
        assertEquals("", findMember(memberData[3], tgtStore).getContactDetails());
    }

    @Test
    public void testSourceOnlyMemberCollegeChanges() throws Exception
    {
        System.out.println("source only member college changes");

        // change from string
        findMember(memberData[0], srcStore).setCollege("Corpus");
        // change from null
        findMember(memberData[1], srcStore).setCollege("Keble");
        // change to null
        findMember(memberData[2], srcStore).setCollege(null);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("Corpus", findMember(memberData[0], tgtStore).getCollege());
        assertEquals("Keble", findMember(memberData[1], tgtStore).getCollege());
        assertEquals("", findMember(memberData[2], tgtStore).getCollege());
    }

    @Test
    public void testTargetOnlyMemberCollegeChanges() throws Exception
    {
        System.out.println("target only member college changes");

        // change from string
        findMember(memberData[0], tgtStore).setCollege("Corpus");
        // change from null
        findMember(memberData[1], tgtStore).setCollege("Keble");
        // change to null
        findMember(memberData[2], srcStore).setCollege(null);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals("Corpus", findMember(memberData[0], tgtStore).getCollege());
        assertEquals("Keble", findMember(memberData[1], tgtStore).getCollege());
        assertEquals("", findMember(memberData[2], tgtStore).getCollege());
    }

    @Test
    public void testSourceOnlyMemberStatusChanges() throws Exception
    {
        System.out.println("source only member name changes");

        // change from string
        findMember(memberData[0], srcStore).setStatus(Member.Status.OUAlumnus);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(Member.Status.OUAlumnus, findMember(memberData[0], tgtStore).getStatus());
    }

    @Test
    public void testTargetOnlyMemberStatusChanges() throws Exception
    {
        System.out.println("target only member name changes");

        // change from string
        findMember(memberData[0], tgtStore).setStatus(Member.Status.OUAlumnus);
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(Member.Status.OUAlumnus, findMember(memberData[0], tgtStore).getStatus());
    }

    @Test
    public void testIndependentIdenticalMemberAdditions() throws Exception
    {
        System.out.println("independent identical member additions");

        addToMembersTable(tgtStore, Arrays.asList(memberData1));
        addToMembersTable(srcStore, Arrays.asList(memberData1));
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(memberData.length + memberData1.length, tgtStore.members().size());
    }

    @Test
    public void testIndependentNonIdenticalMemberAdditions() throws Exception
    {
        System.out.println("independent non-identical member additions");

        addToMembersTable(tgtStore, Arrays.asList(memberData1));
        addToMembersTable(srcStore, Arrays.asList(memberData2));
        addToMembersTable(tgtStore, Arrays.asList(memberData3a));
        addToMembersTable(srcStore, Arrays.asList(memberData3b));
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(memberData.length +
                     memberData1.length +
                     memberData2.length +
                     memberData3a.length, tgtStore.members().size());
        assertEquals(memberData3a[0].contact, findMember(memberData3a[0], tgtStore).getContactDetails());
        assertEquals(memberData3b[0].college, findMember(memberData3a[0], tgtStore).getCollege());
        assertEquals(memberData3b[1].contact, findMember(memberData3a[1], tgtStore).getContactDetails());
        assertEquals(memberData3a[1].college, findMember(memberData3a[1], tgtStore).getCollege());
        assertEquals(memberData3a[2].college, findMember(memberData3a[2], tgtStore).getCollege());
        assertEquals(memberData3a[3].contact, findMember(memberData3a[3], tgtStore).getContactDetails());
    }
    
    public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static Date parseDate(String str)
    {
        try {
            return df.parse(str);
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static final SessionData[] sessionData1a = {
        new SessionData(UUID.randomUUID(),
                        parseDate("2012-01-11 23:30"),
                        parseDate("2012-01-12 01:15")),
        new SessionData(UUID.randomUUID(),
                        parseDate("2012-01-13 23:45"),
                        parseDate("2012-01-14 01:00"))
    };

    @Test
    public void testTargetOnlySessionAdditions() throws Exception
    {
        System.out.println("target session additions only");

        addToSessionsTable(tgtStore, Arrays.asList(sessionData1a));
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(SESSION_COUNT + sessionData1a.length, tgtStore.sessions().size());
    }

    @Test
    public void testSourceOnlySessionAdditions() throws Exception
    {
        System.out.println("source session additions only");

        addToSessionsTable(srcStore, Arrays.asList(sessionData1a));
        srcStore.save(srcFile);
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(SESSION_COUNT + sessionData1a.length, tgtStore.sessions().size());
    }

    @Test
    public void testIndependentIdenticalSessionAdditions() throws Exception
    {
        System.out.println("independent identical session additions");

        addToSessionsTable(srcStore, Arrays.asList(sessionData1a));
        srcStore.save(srcFile);
        addToSessionsTable(tgtStore, Arrays.asList(sessionData1a));
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(SESSION_COUNT + sessionData1a.length, tgtStore.sessions().size());
    }

    @Test
    public void testIndependentIdenticalSessionAdditionsWithAttendance() throws Exception
    {
        System.out.println("independent identical session additions with attendance");

        addToSessionsTable(srcStore, Arrays.asList(sessionData1a));
        Session ss = findSession(sessionData1a[0], srcStore);
        AttendanceRegister sr = srcStore.getRegister(ss);
        sr.add(findMember(memberData[0], srcStore));
        srcStore.save(srcFile);

        addToSessionsTable(tgtStore, Arrays.asList(sessionData1a));
        Session ts = findSession(sessionData1a[0], tgtStore);
        AttendanceRegister tr = tgtStore.getRegister(ts);
        tr.add(findMember(memberData[1], tgtStore));
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(2, tgtStore.getRegister(ts).size());
    }

    @Test
    public void testIndependentIdenticalSessionAndMemberAdditions() throws Exception
    {
        System.out.println("independent identical session and member additions with attendance");

        addToMembersTable(srcStore, Arrays.asList(memberData3a));
        addToSessionsTable(srcStore, Arrays.asList(sessionData1a));
        Session ss = findSession(sessionData1a[0], srcStore);
        AttendanceRegister sr = srcStore.getRegister(ss);
        sr.add(findMember(memberData3a[0], srcStore));
        sr.add(findMember(memberData3a[2], srcStore));
        srcStore.save(srcFile);

        addToMembersTable(tgtStore, Arrays.asList(memberData3b));
        addToSessionsTable(tgtStore, Arrays.asList(sessionData1a));
        Session ts = findSession(sessionData1a[0], tgtStore);
        AttendanceRegister tr = tgtStore.getRegister(ts);
        tr.add(findMember(memberData3b[1], tgtStore));
        tr.add(findMember(memberData3b[2], tgtStore));
        tgtStore.save(tgtFile);

        MergeJob instance = new MergeJob(tgtStore, srcStore);
        instance.start();
        assertTrue(instance.getWarnings().isEmpty());
        assertEquals(3, tgtStore.getRegister(ts).size());
    }
}
