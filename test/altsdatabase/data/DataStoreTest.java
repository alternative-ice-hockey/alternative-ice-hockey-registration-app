/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.net.URL;
import ca.odell.glazedlists.EventList;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.junit.Test;
import static org.junit.Assert.*;
import static altsdatabase.data.DataGenerators.*;

/**
 *
 * @author alex
 */
public class DataStoreTest
{
	public DataStoreTest()
	{
	}

	/**
	 * Test of DataStore creation
	 */
	@Test
	public void testCreate() throws Exception
	{
		System.out.println( "create new data store" );
		DataStore store = new DataStore();
		assertNotNull( store );

		Statement st = store.getConnection().createStatement();
		ResultSet results = st.executeQuery(
                "SELECT int_value FROM Admin "
				+ "WHERE key = 'db_version'" );
		assertTrue ( results.next() );
        assertEquals( DataStore.DB_VERSION, results.getInt( 1 ) );
        st.close();

		store.discardAndClose();
	}

	/**
	 * Test of discardAndClose method, of class DataStore.
	 */
	@Test
	public void testSaveAndClose() throws Exception
	{
		System.out.println( "saveAndClose" );
		DataStore instance = new DataStore();
		File tempDir = instance.getDataDir();

		File tempSaveDir = File.createTempFile( "dataTest", null );
		tempSaveDir.delete();
		tempSaveDir.mkdir();
		File file = File.createTempFile( "dataTest", ".alts", tempSaveDir );

		instance.saveAndClose( file );

		assertFalse( "Failed to cleanup properly", tempDir.exists() );
		assertTrue( "Failed to create save file", file.exists() );
		File[] files = tempSaveDir.listFiles();
		assertEquals( "Failed to cleanup properly", 1, files.length );

		file.delete();
		tempSaveDir.delete();
	}

	/**
	 * Test of discardAndClose method, of class DataStore.
	 */
	@Test
	public void testDiscardAndClose() throws Exception
	{
		System.out.println( "discardAndClose" );
		DataStore instance = new DataStore();
		File tempDir = instance.getDataDir();
		instance.discardAndClose();
		assertFalse( "Failed to cleanup properly", tempDir.exists() );
	}

	/**
	 * Test of DataStore creation/saving/opening
	 */
	@Test
	public void testCreateSaveOpen() throws Exception
	{
		System.out.println( "create/save/open data store" );
		DataStore first = new DataStore();

		// test data
		Statement st = first.getConnection().createStatement();

		st.executeUpdate( "INSERT INTO Admin (key, int_value) "
				+ "VALUES ( 'test data', 7 )" );

        ResultSet results = st.executeQuery( "SELECT * FROM Checkpoints" );
        assertFalse( results.next() );

		st.close();
		st = null;

		File file = File.createTempFile( "dataTest", ".alts" );
		first.save( file );
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore second = DataStore.open( file );

		// test data
		st = second.getConnection().createStatement();
		results = st.executeQuery( "SELECT int_value FROM Admin "
				+ "WHERE key = 'test data'" );
		assertTrue( results.next() );
		assertEquals( 7, results.getInt( 1 ) );

        results = st.executeQuery( "SELECT * FROM Checkpoints" );
        assertTrue( results.next() );

		st.close();
		st = null;

		first.discardAndClose();
		second.discardAndClose();
		file.delete();
	}

	/**
	 * Test of DataStore creation/saving/opening
	 */
	@Test
	public void testCreateSaveOpenWithData() throws Exception
	{
		System.out.println( "create/populate/save/open data store" );
		DataStore first = new DataStore();
        populateMembersTable( first );
        generateSessionsForYear( 2010, first );

		File file = File.createTempFile( "dataTest", ".alts" );
		first.save( file );
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore second = DataStore.open( file );

        for ( MemberData data : memberData )
        {
            boolean found = false;
            for ( Member m : second.members() )
            {
                if ( m.getId().equals( data.id ) )
                {
                    found = true;
                    break;
                }
            }
            assertTrue( found );
        }
        assertEquals( SESSION_COUNT, second.sessions().size() );

		first.discardAndClose();
		second.discardAndClose();
		file.delete();
	}

	/**
	 * Test of DataStore creation/saving-and-closing/opening
	 */
	@Test
	public void testCreateSaveCloseOpen() throws Exception
	{
		System.out.println( "create/save-and-close/open data store" );
		DataStore first = new DataStore();

		// test data
		Statement st = first.getConnection().createStatement();
		st.executeUpdate( "INSERT INTO Admin (key, int_value) "
				+ "VALUES ( 'test data', 9 )" );

        ResultSet results = st.executeQuery( "SELECT * FROM Checkpoints" );
        assertFalse( results.next() );

		st.close();
		st = null;

		File file = File.createTempFile( "dataTest", ".alts" );
		first.saveAndClose( file );
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore second = DataStore.open( file );

		// test data
		st = second.getConnection().createStatement();
		results = st.executeQuery( "SELECT int_value FROM Admin "
				+ "WHERE key = 'test data'" );
		assertTrue( results.next() );
		assertEquals( 9, results.getInt( 1 ) );

        results = st.executeQuery( "SELECT * FROM Checkpoints" );
        assertTrue( results.next() );

		st.close();
		st = null;

		second.discardAndClose();
		file.delete();
	}

	/**
	 * Test of DataStore creation/saving-and-closing/opening
	 */
	@Test
	public void testCreateSaveCloseOpenWithData() throws Exception
	{
		System.out.println( "create/populate/save-and-close/open data store" );
		DataStore first = new DataStore();
        populateMembersTable( first );
        generateSessionsForYear( 2010, first );

		File file = File.createTempFile( "dataTest", ".alts" );
		first.saveAndClose( file );
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore second = DataStore.open( file );

        for ( MemberData data : memberData )
        {
            boolean found = false;
            for ( Member m : second.members() )
            {
                if ( m.getId().equals( data.id ) )
                {
                    found = true;
                    break;
                }
            }
            assertTrue( found );
        }
        assertEquals( SESSION_COUNT, second.sessions().size() );

		second.discardAndClose();
		file.delete();
	}

	/**
	 * Test of save method, of class DataStore.
	 */
	@Test
	public void testSave_0args() throws Exception
	{
		System.out.println( "save" );
		File file = File.createTempFile( "dataTest", ".alts" );
		DataStore instance = new DataStore();
		instance.save( file );
		file.delete();
		instance.save();
		if ( !file.exists() )
		{
			fail( "The file was not created." );
		}
		file.delete();
		instance.discardAndClose();
	}

	/**
	 * Test of DataStore creation/saving/opening
	 */
	@Test
	public void testCreateSaveCloseOpenSave_0args() throws Exception
	{
		System.out.println( "create/save-and-close/open/save data store" );
		DataStore first = new DataStore();

		// test data
		Statement st = first.getConnection().createStatement();
		st.executeUpdate( "INSERT INTO Admin (key, int_value) "
				+ "VALUES ( 'test data', 7 )" );
		st.close();
		st = null;

		File file = File.createTempFile( "dataTest", ".alts" );
		first.saveAndClose( file );
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore second = DataStore.open( file );

		// test data
		st = second.getConnection().createStatement();
		ResultSet results = st.executeQuery( "SELECT int_value FROM Admin "
				+ "WHERE key = 'test data'" );
		results.next();
		assertEquals( 7, results.getInt( 1 ) );
		st.executeUpdate( "UPDATE Admin SET int_value = 15 "
				+ "WHERE key = 'test data'" );
		st.close();
		st = null;

		second.save();
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore third = DataStore.open( file );

		// test data
		st = third.getConnection().createStatement();
		results = st.executeQuery( "SELECT int_value FROM Admin "
				+ "WHERE key = 'test data'" );
		results.next();
		assertEquals( 15, results.getInt( 1 ) );
		st.close();
		st = null;

		second.discardAndClose();
		third.discardAndClose();
		file.delete();
	}

	/**
	 * Test of DataStore creation/saving/opening
	 */
	@Test
	public void testCreateSaveCloseOpenSaveClose_0args() throws Exception
	{
		System.out.println( "create/save-and-close/open/save-and-close data store" );
		DataStore first = new DataStore();

		// test data
		Statement st = first.getConnection().createStatement();
		st.executeUpdate( "INSERT INTO Admin (key, int_value) "
				+ "VALUES ( 'test data', 7 )" );
		st.close();
		st = null;

		File file = File.createTempFile( "dataTest", ".alts" );
		first.saveAndClose( file );
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore second = DataStore.open( file );

		// test data
		st = second.getConnection().createStatement();
		ResultSet results = st.executeQuery( "SELECT int_value FROM Admin "
				+ "WHERE key = 'test data'" );
		results.next();
		assertEquals( 7, results.getInt( 1 ) );
		st.executeUpdate( "UPDATE Admin SET int_value = 15 "
				+ "WHERE key = 'test data'" );
		st.close();
		st = null;

		second.saveAndClose();
		if ( !file.exists() )
		{
			fail( "The file was never created." );
		}

		DataStore third = DataStore.open( file );

		// test data
		st = third.getConnection().createStatement();
		results = st.executeQuery( "SELECT int_value FROM Admin "
				+ "WHERE key = 'test data'" );
		results.next();
		assertEquals( 15, results.getInt( 1 ) );
		st.close();
		st = null;

		third.discardAndClose();
		file.delete();
	}

	/**
	 * Test of DataStore method membershipRoster
	 */
	@Test
	public void testLoadMembers() throws Exception
	{
		System.out.println( "members" );
		DataStore store = new DataStore();

		EventList<Member> members = store.members();
		assertNotNull( members );
		assertSame( members, store.members() );

		store.discardAndClose();
	}

	/**
	 * Test of DataStore method sessions
	 */
	@Test
	public void testLoadSessions() throws Exception
	{
		System.out.println( "sessions" );
		DataStore store = new DataStore();

		EventList<Session> sessions = store.sessions();
		assertNotNull( sessions );
		assertSame( sessions, store.sessions() );

		store.discardAndClose();
	}

	/**
	 * Test of getConnection method, of class DataStore.
	 */
	@Test
	public void testGetConnection() throws Exception
	{
		System.out.println( "getConnection" );
		DataStore instance = new DataStore();
		Connection result = instance.getConnection();
		assertNotNull( result );

		instance.discardAndClose();
	}

	/**
	 * Test of cleanScannedCode method, of class DataStore.
	 */
	@Test
	public void testCleanScannedCode() throws Exception
	{
		System.out.println( "cleanScannedCode" );
		DataStore instance = new DataStore();
		PreparedStatement pst = instance.getConnection().prepareStatement(
				"SELECT * FROM BodCardLog WHERE checksum = ? AND checkdigit = ?" );

		String cleaned = "2345678";
		int checksum = 35;
		String checkDigit = "z";
		String correctCheckDigit = "2345678z";
		String wrongCheckDigit = "2345678@";

		pst.setInt( 1, checksum );
		pst.setString( 2, checkDigit );

		assertEquals( cleaned, instance.cleanScannedCode( cleaned ) );
		ResultSet results = pst.executeQuery();
		assertFalse( results.next() );

		assertEquals( cleaned, instance.cleanScannedCode( correctCheckDigit ) );
		pst.setString( 2, checkDigit );
		results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( 1, results.getInt( "seencount" ) );
		results.close();

		assertEquals( cleaned, instance.cleanScannedCode( correctCheckDigit ) );
		pst.setString( 2, checkDigit );
		results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( 2, results.getInt( "seencount" ) );
		results.close();

		assertEquals( cleaned, instance.cleanScannedCode( wrongCheckDigit ) );
		pst.setString( 2, "@" );
		results = pst.executeQuery();
		assertTrue (results.next());
		assertEquals( 1, results.getInt( "seencount" ) );
		results.close();

		pst.close();
		instance.discardAndClose();
	}

    @Test
    public void testUpgradeV4() throws Exception
    {
		System.out.println( "upgrade (from v4)" );

        URL dbv4Url = getClass().getResource( "resources/db_version_4.alts" );
        File dbv4File = new File(dbv4Url.toURI());
        DataStore instance = DataStore.open( dbv4File );

		Statement st = instance.getConnection().createStatement();
		ResultSet results = st.executeQuery(
                "SELECT int_value FROM Admin "
				+ "WHERE key = 'db_version'" );
		assertTrue ( results.next() );
        assertEquals( DataStore.DB_VERSION, results.getInt( 1 ) );
        st.close();


        instance.members();
        instance.sessions();

		instance.discardAndClose();
    }

    @Test
    public void testUpgradeV5() throws Exception
    {
		System.out.println( "upgrade (from v5)" );

        URL dbv4Url = getClass().getResource( "resources/db_version_5.alts" );
        File dbv4File = new File(dbv4Url.toURI());
        DataStore instance = DataStore.open( dbv4File );

		Statement st = instance.getConnection().createStatement();
		ResultSet results = st.executeQuery(
                "SELECT int_value FROM Admin "
				+ "WHERE key = 'db_version'" );
		assertTrue ( results.next() );
        assertEquals( DataStore.DB_VERSION, results.getInt( 1 ) );
        st.close();


        instance.members();
        instance.sessions();

		instance.discardAndClose();
    }
}
