package altsdatabase.data;

import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.event.ListEventListener;
import java.util.ArrayList;

public class ListEventChecker<E> implements ListEventListener<E>
{
	public static class Change<E>
	{
		public int changeType;
		public int index;
		public E oldValue;
		public E newValue;
	}
	public ArrayList<Change<E>> lastChangeSet = new ArrayList<Change<E>>();
	public boolean wasReordering = false;
	public int[] reorderMap = null;

	public void listChanged( ListEvent<E> le )
	{
		lastChangeSet.clear();
		wasReordering = false;
		reorderMap = null;
		if ( le.isReordering() )
		{
			wasReordering = true;
			reorderMap = le.getReorderMap();
		}
		else
		{
			while ( le.next() )
			{
				Change<E> ch = new Change<E>();
				ch.changeType = le.getType();
				ch.index = le.getIndex();
				ch.oldValue = le.getOldValue();
				ch.newValue = le.getNewValue();
				lastChangeSet.add( ch );
			}
		}
	}
}
