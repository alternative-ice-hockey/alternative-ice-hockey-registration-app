/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.EventList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alex
 */
public class MembershipRosterTest
{
	public MembershipRosterTest()
	{
	}

	@Test
	public void testEmptyStore() throws Exception
	{
		System.out.println( "empty store" );
		DataStore store = new DataStore();
		EventList<Member> members = store.members();

		assertTrue (members.isEmpty());

		store.discardAndClose();
	}

	private static String emptyIfNull( String input )
	{
		if ( input == null )
		{
			return "";
		}
		else
		{
			return input;
		}
	}

	private void assertMemberMatchesResultSet( Member member, ResultSet results )
		throws SQLException
	{
		assertEquals( member.getId(), QueryManager.memberIdFromResult( results, "memberid" ) );
		assertEquals( member.getMembershipCode(), emptyIfNull( results.getString( "membershipcode" ) ) );
		assertEquals( member.getName(), results.getString( "name" ) );
		assertEquals( member.getContactDetails(), emptyIfNull( results.getString( "contact" ) ) );
		assertEquals( member.getCollege(), emptyIfNull( results.getString( "college" ) ) );
		assertEquals( member.getStatus().name(), results.getString( "status" ) );
	}

	@Test
	public void testLoad() throws Exception
	{
		System.out.println( "load existing members" );
		DataStore store = new DataStore();
		PreparedStatement st = store.getConnection().prepareStatement( 
                "SELECT * FROM MembersView WHERE memberid = ?" );
		DataGenerators.populateMembersTable( store );
		EventList<Member> members = store.members();

		assertEquals( DataGenerators.memberData.length, members.size() );

		Set<UUID> seenIds = new HashSet<UUID>();
		Set<Member> seenMembers = new HashSet<Member>();
		for ( Member member : members )
		{
			assertFalse( seenMembers.contains( member ) );
			seenMembers.add( member );

			UUID memberId = member.getId();
			assertNotNull (memberId);
			assertFalse( seenIds.contains( memberId ) );
			seenIds.add( memberId );

            st.setString( 1, memberId.toString() );
			ResultSet results = st.executeQuery();
			assertTrue (results.next());
			assertMemberMatchesResultSet( member, results );
			results.close();
		}

		st.close();
		store.discardAndClose();
	}

	@Test
	public void testRemove() throws Exception
	{
		System.out.println( "remove existing member" );
		DataStore store = new DataStore();
		PreparedStatement st = store.getConnection().prepareStatement( 
                "SELECT * FROM MembersView WHERE memberid = ?" );
		DataGenerators.populateMembersTable( store );
		EventList<Member> members = store.members();

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		members.addListEventListener( listener );

		Member member0 = members.get( 0 );
		Member member1 = members.get( 1 );
		UUID member0Id = member0.getId();

		Member removed = members.remove( 0 );
		assertSame( member0, removed );
		assertTrue (removed.getId() == null);
        st.setString( 1, member0Id.toString() );
		ResultSet results = st.executeQuery();
		assertFalse( results.next() );
		results.close();
		assertFalse( members.contains( member0 ) );

		assertEquals( 1, listener.lastChangeSet.size() );
		assertEquals( ListEvent.DELETE, listener.lastChangeSet.get( 0 ).changeType );
		assertSame( member0, listener.lastChangeSet.get( 0 ).oldValue );

		// just making sure we don't get an exception here
		member0.setFields( member1.getMembershipCode(), member1.getName(),
                           member1.getContactDetails(),
                           member1.getCollege(),
                           member1.getStatus() );

		st.close();
		store.discardAndClose();
	}

	@Test
	public void testAlter() throws Exception
	{
		System.out.println( "altering members updates database" );
		DataStore store = new DataStore();
		PreparedStatement st = store.getConnection().prepareStatement( 
                "SELECT * FROM MembersView WHERE memberid = ?" );
		DataGenerators.populateMembersTable( store );
		EventList<Member> members = store.members();

		Member member0 = members.get( 0 );
		UUID member0Id = member0.getId();
        st.setString( 1, member0Id.toString() );

        Date after = new Date();
        Date then = new Date();
		member0.setMembershipCode( "342398" );
		ResultSet results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        Date now = new Date();
        // we construct a new Date object, as the Timestamp object does
        // not behave properly (it does not set its internal fastTime
        // variable correctly, so Date.before() and Date.after() return
        // incorrect results)
        Date lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        Date created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setMembershipCode( null );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();


        then = new Date();
		member0.setName( "Bryn Markov" );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setContactDetails( "Blah blah blah" );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setContactDetails( null );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setCollege( "St. Johns" );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setCollege( null );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setStatus(
                  member0.getStatus() == Member.Status.OUAlumnus
                ? Member.Status.OUStaff
                : Member.Status.OUAlumnus );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setStatus( Member.Status.Other );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

        then = new Date();
		member0.setFields( "0101010", "James Edison", "james.edison@magd.ox.ac.uk", "Magdalen", Member.Status.OUStudent );
		results = st.executeQuery();
		assertTrue (results.next());
		assertMemberMatchesResultSet( member0, results );
        now = new Date();
        lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
        assertFalse( then.after( lastmodified ) );
        assertFalse( now.before( lastmodified ) );
        created = new Date(results.getTimestamp( "created" ).getTime());
        assertFalse( after.before( created ) );
		results.close();

		Member member1 = members.get( 1 );
		member0.setMembershipCode( null );
		member1.setMembershipCode( null );
		member0.setMembershipCode( "" );
		member1.setMembershipCode( "" );

		st.close();
		store.discardAndClose();
	}

	@Test
	public void testBadAlter() throws Exception
	{
		System.out.println( "altering members cannot create duplicates" );
		DataStore store = new DataStore();
		PreparedStatement st = store.getConnection().prepareStatement( 
                "SELECT * FROM MembersView WHERE memberid = ?" );
		DataGenerators.populateMembersTable( store );
		EventList<Member> members = store.members();

		Member member0 = members.get( 0 );
		Member member1 = members.get( 1 );
		UUID member0Id = member0.getId();
        st.setString( 1, member0Id.toString() );
		String oldMembershipCode = member0.getMembershipCode();

		try
		{
			member0.setMembershipCode( member1.getMembershipCode() );
			fail( "Duplicate membership code allowed" );
		}
		catch ( ProhibitedChangeException ex )
		{
			assertEquals( oldMembershipCode, member0.getMembershipCode() );
			ResultSet results = st.executeQuery();
			assertTrue (results.next());
			assertMemberMatchesResultSet( member0, results );
			results.close();
		}

		try
		{
			member0.setFields( member1.getMembershipCode(), "James Edison", "james.edison@magd.ox.ac.uk", "Magdalen", Member.Status.OUStudent );
			fail( "Duplicate membership code allowed" );
		}
		catch ( ProhibitedChangeException ex )
		{
			assertEquals( oldMembershipCode, member0.getMembershipCode() );
			ResultSet results = st.executeQuery();
			assertTrue (results.next());
			assertMemberMatchesResultSet( member0, results );
			results.close();
		}

		st.close();
		store.discardAndClose();
	}

	/**
	 * Test of create method, of class MembershipRoster.
	 */
	@Test
	public void testCreate() throws Exception
	{
		System.out.println( "create" );

		DataStore store = new DataStore();
		PreparedStatement pst = store.getConnection().prepareStatement( 
                "SELECT * FROM MembersView WHERE memberid = ?" );
		EventList<Member> members = store.members();

		ListEventChecker<Member> listener = new ListEventChecker<Member>();
		members.addListEventListener( listener );

		for ( DataGenerators.MemberData data : DataGenerators.memberData )
		{
            Date then = new Date();
			Member member = new Member( data.membershipCode,
                                        data.name,
                                        data.contact,
                                        data.college,
                                        data.status );
			members.add( member );
			assertTrue (member.getId() != null);
            pst.setString( 1, member.getId().toString() );
			ResultSet results = pst.executeQuery();
			assertTrue (results.next());
			assertMemberMatchesResultSet( member, results );
            Date now = new Date();
            // we construct a new Date object, as the Timestamp object does
            // not behave properly (it does not set its internal fastTime
            // variable correctly, so Date.before() and Date.after() return
            // incorrect results)
            Date lastmodified = new Date(results.getTimestamp( "lastmodified" ).getTime());
            assertFalse( then.after( lastmodified ) );
            assertFalse( now.before( lastmodified ) );
            Date created = new Date(results.getTimestamp( "created" ).getTime());
            assertFalse( then.after( created ) );
			results.close();
			members.contains( member );

			assertEquals( 1, listener.lastChangeSet.size() );
			assertEquals( ListEvent.INSERT, listener.lastChangeSet.get( 0 ).changeType );
			assertSame( member, members.get( listener.lastChangeSet.get( 0 ).index ) );
		}

		DataGenerators.MemberData[] data = DataGenerators.memberData;
		Member member = new Member( data[0].membershipCode,
                                    data[1].name,
                                    data[2].contact,
                                    data[1].college,
                                    data[0].status );
		try
		{
			members.add( member );
			fail( "Member with duplicate code allowed" );
		}
		catch ( DuplicateMembershipCodeException ex )
		{
			assertNull( member.getId() );
            Statement st = store.getConnection().createStatement();
			ResultSet results = st.executeQuery( "SELECT COUNT(*) FROM MembersView" );
			results.next();
			assertEquals( data.length, results.getInt( 1 ) );
			results.close();
            st.close();
		}

		pst.close();
		store.discardAndClose();
	}
}
