/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.util.EnumSet;
import altsdatabase.data.Member.Status;
import altsdatabase.data.event.MemberEvent;
import altsdatabase.data.event.MemberListener;
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alex
 */
public class MemberTest
{
    public MemberTest()
    {
    }

    private class TestData
    {
        public TestData( int id, String membershipCode, String name, String contact, String college, Member.Status status )
        {
            this.id = id;
            this.status = status;
            this.membershipCode = membershipCode;
            this.name = name;
            this.contact = contact;
            this.college = college;
        }
        public int id;
        public Member.Status status;
        public String membershipCode;
        public String name;
        public String contact;
        public String college;

        @Override
        public TestData clone()
        {
            return new TestData( id, membershipCode, name, contact, college, status );
        }

        public void assertMatches( Member member )
        {
            assertEquals( membershipCode, member.getMembershipCode() );
            assertEquals( name, member.getName() );
            assertEquals( contact, member.getContactDetails() );
            assertEquals( college, member.getCollege() );
            assertEquals( status, member.getStatus() );
        }
    }

    private boolean relaxedEquals( String s1, String s2 )
    {
        if ( s1 == null )
        {
            s1 = "";
        }
        if ( s2 == null )
        {
            s2 = "";
        }
        return s1.equals( s2 );
    }

    private class TestMemberEventListener implements MemberListener
    {
        Member member;
        MemberEvent lastEvent;
        boolean changeAllowed = true;

        public TestMemberEventListener( Member session )
        {
            this.member = session;
        }

        public boolean isChangeAllowed()
        {
            return changeAllowed;
        }

        public void setChangeAllowed( boolean changeAllowed )
        {
            this.changeAllowed = changeAllowed;
        }

        public void checkChange( TestData oldData, TestData newData )
        {
            newData.assertMatches( member );
            EnumSet<MemberEvent.Changes> expectedChanges = EnumSet.noneOf( MemberEvent.Changes.class );
            if ( !relaxedEquals( oldData.membershipCode, newData.membershipCode ) )
            {
                expectedChanges.add( MemberEvent.Changes.MembershipCode );
            }
            if ( !relaxedEquals( oldData.name, newData.name ) )
            {
                expectedChanges.add( MemberEvent.Changes.Name );
            }
            if ( !relaxedEquals( oldData.contact, newData.contact ) )
            {
                expectedChanges.add( MemberEvent.Changes.Contact );
            }
            if ( !relaxedEquals( oldData.college, newData.college ) )
            {
                expectedChanges.add( MemberEvent.Changes.College );
            }
            if ( oldData.status != newData.status )
            {
                expectedChanges.add( MemberEvent.Changes.Status );
            }
            checkChange( oldData.membershipCode, oldData.name,
                         oldData.contact, oldData.college,
                         oldData.status, expectedChanges );
        }

        public void checkChange( String origValue, MemberEvent.Changes change )
        {
            switch ( change )
            {
                case MembershipCode:
                    checkChange( origValue, member.getName(),
                                 member.getContactDetails(),
                                 member.getCollege(),
                                 member.getStatus(), EnumSet.of( change ) );
                    break;
                case Name:
                    checkChange( member.getMembershipCode(), origValue,
                                 member.getContactDetails(),
                                 member.getCollege(),
                                 member.getStatus(), EnumSet.of( change ) );
                    break;
                case Contact:
                    checkChange( member.getMembershipCode(), member.getName(),
                                 origValue,
                                 member.getCollege(),
                                 member.getStatus(), EnumSet.of( change ) );
                    break;
                case College:
                    checkChange( member.getMembershipCode(), member.getName(),
                                 member.getContactDetails(),
                                 origValue,
                                 member.getStatus(), EnumSet.of( change ) );
                    break;
            }
        }

        public void checkChange( Member.Status status )
        {
            checkChange( member.getMembershipCode(), member.getName(),
                         member.getContactDetails(),
                                 member.getCollege(),
                         status, EnumSet.of( MemberEvent.Changes.Status ) );
        }

        public void checkChange( String oldMembershipCode,
                                 String oldName,
                                 String oldContact,
                                 String oldCollege,
                                 Member.Status oldStatus,
                                 EnumSet<MemberEvent.Changes> changes )
        {
            assertSame( member, lastEvent.getSource() );
            assertSame( member, lastEvent.getMember() );
            assertEquals( lastEvent.getChangeMask(), changes );
            assertEquals( oldMembershipCode, lastEvent.getOldMembershipCode() );
            assertEquals( oldName, lastEvent.getOldName() );
            assertEquals( oldContact, lastEvent.getOldContactDetails() );
            assertEquals( oldCollege, lastEvent.getOldCollege() );
            assertEquals( oldStatus, lastEvent.getOldStatus() );
            clearEvent();
        }

        public void checkNoEvent()
        {
            assertNull( lastEvent );
        }

        public void clearEvent()
        {
            lastEvent = null;
        }

        public void memberChanged( MemberEvent event )
        {
            lastEvent = event;
        }

        public boolean aboutToChangeMember( Member member, String newMembershipCode, String newName, String newContact, java.lang.String newCollege, Status newStatus )
        {
            return changeAllowed;
        }
    }

    /**
     * Test of addMemberListener method, of class Member.
     */
    @Test
    public void testAddMemberListener()
    {
        System.out.println( "addMemberListener" );
        Member instance = new Member( null, "Jim Carey", null, null, Status.Other );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );
    }

    /**
     * Test of removeMemberListener method, of class Member.
     */
    @Test
    public void testRemoveMemberListener()
    {
        System.out.println( "removeMemberListener" );
        Member instance = new Member( null, "Jim Carey", null, null, Status.Other );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.removeMemberListener( listener );
        instance.addMemberListener( listener );
        instance.removeMemberListener( listener );
        instance.removeMemberListener( listener );
    }

    /**
     * Test of fireMemberChanged method, of class Member.
     */
    @Test
    public void testFireMemberChanged()
    {
        System.out.println( "fireMemberChanged" );
        Member instance = new Member( null, "Jim Carey", null, null, Status.Other );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );
        String oldMembershipCode = "2345678";
        String oldName = "Mark Hamil";
        String oldContact = "a@b";
        String oldCollege = "Wadham";
        Status oldStatus = Member.Status.OUAlumnus;
        instance.fireMemberChanged( oldMembershipCode, oldName, oldContact, oldCollege, oldStatus );
        listener.checkChange( oldMembershipCode, oldName,
                              oldContact, oldCollege, oldStatus,
                              EnumSet.allOf( MemberEvent.Changes.class ) );
    }

    /**
     * Test of isMemberChangeAllowed method, of class Member.
     */
    @Test
    public void testIsMemberChangeAllowed()
    {
        System.out.println( "isMemberChangeAllowed" );
        Member instance = new Member( null, "Jim Carey", null, null, Status.Other );
        TestMemberEventListener listener1 = new TestMemberEventListener( instance );
        instance.addMemberListener( listener1 );
        TestMemberEventListener listener2 = new TestMemberEventListener( instance );
        instance.addMemberListener( listener2 );

        String newMembershipCode = "";
        String newName = "";
        String newContact = "";
        String newCollege = "";
        Status newStatus = null;

        boolean result = instance.isMemberChangeAllowed( newMembershipCode, newName, newContact, newCollege, newStatus );
        assertTrue( result );
        listener1.setChangeAllowed( false );
        result = instance.isMemberChangeAllowed( newMembershipCode, newName, newContact, newCollege, newStatus );
        assertFalse( result );
        listener2.setChangeAllowed( false );
        result = instance.isMemberChangeAllowed( newMembershipCode, newName, newContact, newCollege, newStatus );
        assertFalse( result );
        listener1.setChangeAllowed( true );
        result = instance.isMemberChangeAllowed( newMembershipCode, newName, newContact, newCollege, newStatus );
        assertFalse( result );
    }

    /**
     * Test of getStatus method, of class Member.
     */
    @Test
    public void testGetStatus()
    {
        System.out.println( "getStatus" );
        Member instance1 = new Member( null, "Jim Carey", null, null, Status.Other );
        assertEquals( Status.Other, instance1.getStatus() );
        Member instance2 = new Member( null, "Jim Carey", null, null, Status.OUAlumnus );
        assertEquals( Status.OUAlumnus, instance2.getStatus() );
        Member instance3 = new Member( null, "Jim Carey", null, null, Status.OUStaff );
        assertEquals( Status.OUStaff, instance3.getStatus() );
        Member instance4 = new Member( null, "Jim Carey", null, null, Status.OUStudent );
        assertEquals( Status.OUStudent, instance4.getStatus() );
        Member instance5 = new Member( UUID.randomUUID(), null, "Jim Carey", null, null, Status.OUStudent );
        assertEquals( Status.OUStudent, instance5.getStatus() );
    }

    /**
     * Test of getName method, of class Member.
     */
    @Test
    public void testGetName()
    {
        System.out.println( "getName" );
        Member instance1 = new Member( null, "Jim Carey", null, null, Status.Other );
        assertEquals( "Jim Carey", instance1.getName() );
        Member instance2 = new Member( UUID.randomUUID(), null, "Henrietta Jones", null, null, Status.Other );
        assertEquals( "Henrietta Jones", instance2.getName() );
    }

    /**
     * Test of getMembershipCode method, of class Member.
     */
    @Test
    public void testGetMembershipCode()
    {
        System.out.println( "getMembershipCode" );
        Member instance1 = new Member( null, "Jim Carey", null, null, Status.Other );
        assertEquals( "", instance1.getMembershipCode() );
        Member instance2 = new Member( "", "Henrietta Jones", null, null, Status.Other );
        assertEquals( "", instance2.getMembershipCode() );
        Member instance3 = new Member( "1234567", "Henrietta Jones", null, null, Status.Other );
        assertEquals( "1234567", instance3.getMembershipCode() );
    }

    /**
     * Test of getContactDetails method, of class Member.
     */
    @Test
    public void testGetContactDetails()
    {
        System.out.println( "getContactDetails" );
        Member instance1 = new Member( null, "Jim Carey", null, null, Status.Other );
        assertEquals( "", instance1.getContactDetails() );
        Member instance2 = new Member( "", "Henrietta Jones", "", null, Status.Other );
        assertEquals( "", instance2.getContactDetails() );
        Member instance3 = new Member( "1234567", "Henrietta Jones", "alfred@batman.com", "Christ Church", Status.Other );
        assertEquals( "alfred@batman.com", instance3.getContactDetails() );
    }

    /**
     * Test of setFields method, of class Member.
     */
    @Test
    public void testSetFields() throws Exception
    {
        System.out.println( "setFields" );

        TestData oldData = new TestData( -1, "7634", "Jim Carey", "carey@jim.com", "St. Peters", Status.Other );

        Member instance = new Member( oldData.membershipCode, oldData.name,
                                      oldData.contact,
                                      oldData.college,
                                      oldData.status );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );

        TestData data = new TestData( -1, "123", "Mark Hamil", "mark@hamil.com", "Pembroke", Status.OUStaff );
        instance.setFields( data.membershipCode, data.name,
                            data.contact, data.college, data.status );
        listener.checkChange( oldData, data );

        oldData = data;
        data = new TestData( -1, "", "Jack Jones", "", "", Status.OUAlumnus );
        instance.setFields( data.membershipCode, data.name,
                            data.contact, data.college, data.status );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.membershipCode = "24680";
        instance.setFields( data.membershipCode, data.name,
                            data.contact, data.college, data.status );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.name = "Jane Jungle";
        instance.setFields( data.membershipCode, data.name,
                            data.contact, data.college, data.status );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.contact = "jane@jungle.com";
        instance.setFields( data.membershipCode, data.name,
                            data.contact, data.college, data.status );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.status = Member.Status.OUStudent;
        instance.setFields( data.membershipCode, data.name,
                            data.contact, data.college, data.status );
        listener.checkChange( oldData, data );

        oldData = data;
        data = new TestData( -1, "", "Jack Jones", "", "", Status.OUAlumnus );
        instance.setFields( null, data.name,
                            null, null, data.status );
        listener.checkChange( oldData, data );

        oldData = data;
        data = new TestData( -1, "238492", "Stacey's Mom",
                             "stacey@fountains-of-wayne.com",
                             "St. Hugh's",
                             Status.Other );

        listener.setChangeAllowed( false );

        try
        {
            instance.setFields( data.membershipCode, data.name,
                                data.contact, data.college, data.status );
            fail( "Prohibited change allowed" );
        }
        catch ( ProhibitedChangeException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }

        listener.setChangeAllowed( true );

        try
        {
            instance.setFields( data.membershipCode, "",
                                data.contact, data.college, data.status );
            fail( "Empty name allowed" );
        }
        catch ( IllegalArgumentException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }

        try
        {
            instance.setFields( data.membershipCode, null,
                                data.contact, data.college, data.status );
            fail( "Null name allowed" );
        }
        catch ( IllegalArgumentException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
        catch ( NullPointerException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }

        try
        {
            instance.setFields( data.membershipCode, data.name,
                                data.contact, data.college, null );
            fail( "Null status allowed" );
        }
        catch ( IllegalArgumentException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
        catch ( NullPointerException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of setMembershipCode method, of class Member.
     */
    @Test
    public void testSetMembershipCode() throws Exception
    {
        System.out.println( "setMembershipCode" );

        TestData data = new TestData( -1, "7634", "Jim Carey",
                                      "carey@jim.com", "Magdalen", Status.Other );

        Member instance = new Member( data.membershipCode, data.name,
                                      data.contact, data.college,
                                      data.status );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );

        TestData oldData = data.clone();
        data.membershipCode = "83728";
        instance.setMembershipCode( data.membershipCode );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.membershipCode = "";
        instance.setMembershipCode( data.membershipCode );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.membershipCode = "83728";
        instance.setMembershipCode( data.membershipCode );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.membershipCode = "";
        instance.setMembershipCode( null );
        listener.checkChange( oldData, data );

        listener.setChangeAllowed( false );
        oldData = data.clone();
        data.membershipCode = "232434";

        try
        {
            instance.setMembershipCode( data.membershipCode );
            fail( "Prohibited change allowed" );
        }
        catch ( ProhibitedChangeException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of setName method, of class Member.
     */
    @Test
    public void testSetName() throws Exception
    {
        System.out.println( "setName" );

        TestData data = new TestData( -1, "7634", "Jim Carey",
                                      "carey@jim.com", "Wadham", Status.Other );

        Member instance = new Member( data.membershipCode, data.name,
                                      data.contact, data.college,
                                      data.status );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );

        TestData oldData = data.clone();
        data.name = "Jane of the Jungle";
        instance.setName( data.name );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.name = "Sir Marcus Browning, MP";
        instance.setName( data.name );
        listener.checkChange( oldData, data );

        listener.setChangeAllowed( false );
        oldData = data.clone();
        data.name = "Mark O'Brien";

        try
        {
            instance.setName( data.name );
            fail( "Prohibited change allowed" );
        }
        catch ( ProhibitedChangeException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }

        listener.setChangeAllowed( true );
        data.name = "";

        try
        {
            instance.setName( data.name );
            fail( "Empty first name allowed" );
        }
        catch ( IllegalArgumentException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }

        try
        {
            instance.setName( null );
            fail( "Null first name allowed" );
        }
        catch ( IllegalArgumentException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
        catch ( NullPointerException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of setContactDetails method, of class Member.
     */
    @Test
    public void testSetContactDetails() throws Exception
    {
        System.out.println( "setContactDetails" );

        TestData data = new TestData( -1, "7634", "Jim Carey",
                                      "carey@jim.com", "Wadham", Status.Other );

        Member instance = new Member( data.membershipCode, data.name,
                                      data.contact, data.college,
                                      data.status );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );

        TestData oldData = data.clone();
        data.contact = "j@foo.com";
        instance.setContactDetails( data.contact );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.contact = "";
        instance.setContactDetails( data.contact );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.contact = "07777 777777";
        instance.setContactDetails( data.contact );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.contact = "";
        instance.setContactDetails( null );
        listener.checkChange( oldData, data );

        listener.setChangeAllowed( false );
        oldData = data.clone();
        data.contact = "sdfjsdk@sdfjsakldfjsal.com";

        try
        {
            instance.setContactDetails( data.contact );
            fail( "Prohibited change allowed" );
        }
        catch ( ProhibitedChangeException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of setCollege method, of class Member.
     */
    @Test
    public void testSetCollege() throws Exception
    {
        System.out.println( "setCollege" );

        TestData data = new TestData( -1, "7634", "Jim Carey",
                                      "carey@jim.com", "Wadham", Status.Other );

        Member instance = new Member( data.membershipCode, data.name,
                                      data.contact, data.college,
                                      data.status );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );

        TestData oldData = data.clone();
        data.college = "Merton";
        instance.setCollege( data.college );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.college = "";
        instance.setCollege( null );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.college = "A really really really long college name";
        instance.setCollege( data.college );
        listener.checkChange( oldData, data );

        listener.setChangeAllowed( false );
        oldData = data.clone();
        data.college = "Wadham";

        try
        {
            instance.setCollege( data.college );
            fail( "Prohibited change allowed" );
        }
        catch ( ProhibitedChangeException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
    }

    /**
     * Test of setStatus method, of class Member.
     */
    @Test
    public void testSetStatus() throws Exception
    {
        System.out.println( "setStatus" );

        TestData data = new TestData( -1, "7634", "Jim Carey",
                                      "carey@jim.com", "Wadham", Status.Other );

        Member instance = new Member( data.membershipCode, data.name,
                                      data.contact, data.college,
                                      data.status );
        TestMemberEventListener listener = new TestMemberEventListener( instance );
        instance.addMemberListener( listener );

        TestData oldData = data.clone();
        data.status = Member.Status.OUStaff;
        instance.setStatus( data.status );
        listener.checkChange( oldData, data );

        oldData = data.clone();
        data.status = Member.Status.OUAlumnus;
        instance.setStatus( data.status );
        listener.checkChange( oldData, data );

        listener.setChangeAllowed( false );
        oldData = data.clone();
        data.status = Member.Status.OUStudent;

        try
        {
            instance.setStatus( data.status );
            fail( "Prohibited change allowed" );
        }
        catch ( ProhibitedChangeException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }

        listener.setChangeAllowed( true );

        try
        {
            instance.setStatus( null );
            fail( "Null status allowed" );
        }
        catch ( IllegalArgumentException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
        catch ( NullPointerException ex )
        {
            oldData.assertMatches( instance );
            listener.checkNoEvent();
        }
    }
}