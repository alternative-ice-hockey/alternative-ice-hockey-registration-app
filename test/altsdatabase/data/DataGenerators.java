/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package altsdatabase.data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 *
 * @author alex
 */
public class DataGenerators
{
    public static final int GROUP_COUNT = 5;
    public static final int SESSIONS_PER_GROUP = 8;
    public static final int SESSION_COUNT = GROUP_COUNT * SESSIONS_PER_GROUP;

    private DataGenerators()
    {
    }

    public static void generateSessionsForYear( int year, DataStore store )
            throws SQLException
    {
        PreparedStatement pst = store.getConnection().prepareStatement(
                "INSERT INTO Sessions (sessionid, starttime, endtime) "
                + "VALUES (gen_uuid(), ?, ?)" );

        int week = 2;
        for ( int block = 1; block <= GROUP_COUNT; block++ )
        {
            Calendar startDate = new GregorianCalendar( year, Calendar.JANUARY, 1 );
            startDate.set( Calendar.WEEK_OF_YEAR, week );
            startDate.set( Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY );
            startDate.set( Calendar.HOUR_OF_DAY, 23 );
            startDate.set( Calendar.MINUTE, 30 );
            startDate.set( Calendar.SECOND, 0 );
            startDate.set( Calendar.MILLISECOND, 0 );
            Calendar endDate = (Calendar) startDate.clone();
            endDate.add( Calendar.MINUTE, 90 );

            for ( int i = 0; i < SESSIONS_PER_GROUP; i++ )
            {
                pst.setTimestamp( 1, new java.sql.Timestamp( startDate.getTime().getTime() ) );
                pst.setTimestamp( 2, new java.sql.Timestamp( endDate.getTime().getTime() ) );
                pst.executeUpdate();
                startDate.add( Calendar.WEEK_OF_YEAR, 1 );
                endDate.add( Calendar.WEEK_OF_YEAR, 1 );
                ++week;
            }
            ++week;
        }

        pst.close();
    }

    public static class MemberData
    {
        public MemberData( UUID id, String membershipCode, String name,
                           String contact, String college,
                           Member.Status status )
        {
            this.id = id;
            this.status = status;
            this.membershipCode = membershipCode;
            this.name = name;
            this.contact = contact;
            this.college = college;
        }
        public final UUID id;
        public final String membershipCode;
        public final String name;
        public final String contact;
        public final String college;
        public final Member.Status status;
    }
    public static final MemberData[] memberData =
    {
        new MemberData( UUID.randomUUID(), "1234", "Joe Bloggs", "joe@bloggs.com", "Wadham", Member.Status.Other ),
        new MemberData( UUID.randomUUID(), "abcd", "James Hudson", null, null, Member.Status.OUAlumnus ),
        new MemberData( UUID.randomUUID(), "2756434", "Charles Darwin", null, "St. Hildas", Member.Status.OUStudent ),
        new MemberData( UUID.randomUUID(), "4004", "James Ussher", "james@bishop.org.uk", null, Member.Status.OUStaff ),
        new MemberData( UUID.randomUUID(), null, "James Ussher", "james@bishop.org.u", "Corpus Christi", Member.Status.OUStaff )
    };

    public static Member[] getMembers(DataStore store)
    {
        Member[] result = new Member[memberData.length];
        for ( Member testMember : store.members() ) {
            for ( int i = 0; i < memberData.length; ++i ) {
                if ( testMember.getId().equals( memberData[i].id ) ) {
                    result[i] = testMember;
                    break;
                }
            }
        }
        return result;
    }

    public static MemberData[] getMemberData()
    {
        return memberData;
    }

    public static void addToMembersTable( DataStore store, Collection<MemberData> data )
            throws SQLException
    {
        PreparedStatement pst = store.getConnection().prepareStatement(
                "INSERT INTO Members "
                + "(memberid, membershipcode, name, contact, college, statusid) "
                + "VALUES (?, ?, ?, ?, ?, (SELECT statusid FROM MemberStatuses WHERE status = ?))" );

        for ( MemberData datum : data )
        {
            pst.setString( 1, datum.id.toString() );
            pst.setString( 2, datum.membershipCode );
            pst.setString( 3, datum.name );
            pst.setString( 4, datum.contact );
            pst.setString( 5, datum.college );
            pst.setString( 6, datum.status.name() );
            pst.executeUpdate();
        }

        pst.close();
    }

    public static void populateMembersTable( DataStore store, Collection<MemberData> data )
            throws SQLException
    {
        Statement st = store.getConnection().createStatement();
        ResultSet results = st.executeQuery( "SELECT COUNT(*) FROM Members" );
        results.next();
        if ( results.getInt( 1 ) == 0 )
        {
            addToMembersTable(store, data);
        }
        results.close();
        st.close();
    }

    public static void populateMembersTable( DataStore store )
            throws SQLException
    {
        populateMembersTable( store, java.util.Arrays.asList( memberData ) );
    }

    public static Member findMember(MemberData data, DataStore store)
    {
        return findMember(data.id, store);
    }

    public static Member findMember(UUID memberid, DataStore store)
    {
        for (Member m : store.members())
        {
            if (m.getId().equals(memberid))
                return m;
        }
        return null;
    }

    public static class SessionData
    {
        public SessionData( UUID id, Date startTime, Date endTime )
        {
            this.id = id;
            this.startTime = startTime;
            this.endTime = endTime;
        }
        public final UUID id;
        public final Date startTime;
        public final Date endTime;
    }

    public static void addToSessionsTable( DataStore store, Collection<SessionData> data )
            throws SQLException
    {
        PreparedStatement pst = store.getConnection().prepareStatement(
                "INSERT INTO Sessions (sessionid, starttime, endtime) "
                + "VALUES (?, ?, ?)" );

        for ( SessionData datum : data )
        {
            pst.setString( 1, datum.id.toString() );
            pst.setTimestamp( 2, new java.sql.Timestamp( datum.startTime.getTime() ) );
            pst.setTimestamp( 3, new java.sql.Timestamp( datum.endTime.getTime() ) );
            pst.executeUpdate();
        }

        pst.close();
    }

    public static Session findSession(SessionData session, DataStore store)
    {
        return findSession (session.id, store);
    }

    public static Session findSession(UUID sessionid, DataStore store)
    {
        for (Session s : store.sessions())
        {
            if (s.getId().equals(sessionid))
                return s;
        }
        return null;
    }
}
